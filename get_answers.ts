import {appendFile} from "node:fs";

const regex = /Your puzzle answer was <code>(.*?)<\/code>/g;


const downloadCorrect = async (year: string|number, day: string|number): Promise<(string|number)[]> => {
  const res = await fetch(`https://adventofcode.com/${year}/day/${day}`, {
    headers: {
    cookie: `session=${Bun.env.TOKEN}`,
  },
  });
  const data = await res.text();
  const matches = data.matchAll(regex);
  if (!matches) throw new Error("No matches");
  const correctOutputs = [...matches].map(m => (
    // If contains any non-digit chars
    /([^\d])/.test(m[1])
      ? m[1]
      : parseInt(m[1])
  ));
  console.log("Downloaded", correctOutputs);
  return correctOutputs;
}

(async () => {

  if (Bun.argv.length < 4) {
    console.error("Provide year and day");
    return 1;
  }
  const year = Bun.argv[2];
  const day = Bun.argv[3];

  const basePath = `./src/${year}/day_${day.padStart(2, '0')}`;

  const getStored = (task: number) => {
    try {
      const {correctOutput } = require(`${basePath}/task_0${task}.ts`);
      return correctOutput;
    } catch(e) {
      return null;
    }
  }

  const day1 = await getStored(1);
  const day2 = await getStored(2);

  console.log("Stored:", {day1, day2});

  // Null means the file doesn't exist
  // We still want to continue if it does
  if (day1 && day2) {
    console.log([day1, day2]);
    return;
  } if (day1 && day2 === null) {
    console.log([day1, null]);
    return;
  } if (day1 === null && day2) {
    console.log([null, day2]);
    return;
  }

  const correctOutputs = await downloadCorrect(year, day);
  console.log(correctOutputs);



  /** Save a correct value to the solution file */
  const putStored = (task: number) => {
    // Undefined means the file exists but doesn't have a correct answer
    if (getStored(task) !== undefined) return;
    console.log("Saving task", `${task}:`, correctOutputs[task-1]);
    appendFile(
      `${basePath}/task_0${task}.ts`,
      `\nexport const correctOutput = ${JSON.stringify(correctOutputs[task-1])};\n`,
        (err) => {
        if (!err) return;
        console.error(err);
        throw err;
      }
    );
  }
  putStored(1);
  putStored(2);
})();
