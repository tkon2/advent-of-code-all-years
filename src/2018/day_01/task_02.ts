import { splitTN } from '../../lib/numbers';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  let seen = new Set<number>();

  let maxLoops = 500;
  while (maxLoops-- > 0) {
    for (let line of lines) {
      const numbers = splitTN(line, true);
      for (let num of numbers) {
        answer += num;
        if (seen.has(answer)) return answer;
        seen.add(answer);
      }
    }
  }

  throw new Error('No duplicates found');
};

export const correctOutput = 76787;
