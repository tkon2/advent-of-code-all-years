import { splitTN } from '../../lib/numbers';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;

  lines.forEach((line) => {
    const numbers = splitTN(line, true);
    numbers.forEach((num) => {
      answer += num;
    });
  });

  return answer;
};

export const correctOutput = 531;
