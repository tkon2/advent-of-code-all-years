import { Solution } from '../../types/solution';

export const solve: Solution = async ([line], { log }) => {
  const availablePolymers = [
    ...new Set<string>(line.split('').map((l) => l.toLowerCase())),
  ].sort((a, b) => a.localeCompare(b));

  const reactPolymer = (polymer: string) => {
    let maxTries = polymer.length / 2;
    while (maxTries-- > 0) {
      const lineLength = polymer.length;

      for (let check of availablePolymers) {
        polymer = polymer.replaceAll(`${check}${check.toUpperCase()}`, '');
        polymer = polymer.replaceAll(`${check.toUpperCase()}${check}`, '');
      }

      if (lineLength === polymer.length) break;
    }

    return polymer.length;
  };

  let lowest = Infinity;

  for (let i = 0; i < availablePolymers.length; i++) {
    const polymer = availablePolymers[i];
    const str = line
      .split('')
      .filter((c) => c.toLowerCase() !== polymer)
      .join('');
    const length = reactPolymer(str);
    log(
      polymer.toUpperCase(),
      'reacts down to',
      length,
      'elements, and originally occured',
      line.split('').filter((c) => c.toLowerCase() === polymer).length,
      'times',
    );
    lowest = Math.min(lowest, length);
  }

  return lowest;
};

export const correctOutput = 4052;
