import { Solution } from '../../types/solution';

export const solve: Solution = async ([line], { log }) => {
  log('Original length:', line.length);
  let maxTries = line.length / 2;
  while (maxTries-- > 0) {
    const lineLength = line.length;
    for (let i = 1; i < line.length; i++) {
      const a = line[i - 1];
      const b = line[i];

      if (a.toLowerCase() === b.toLowerCase() && a !== b) {
        line = line.slice(0, i - 1) + line.slice(i + 1);
        i -= 1;
      }
    }
    if (lineLength === line.length) break;
  }

  return line.length;
};

export const correctOutput = 9238;
