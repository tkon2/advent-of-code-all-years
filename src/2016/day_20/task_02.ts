import { Solution } from '../../types/solution';
import { splitTN } from '../../lib/numbers';

export const solve: Solution = async (lines, { log }) => {
  let total = 0;
  let pos = 0;

  let ranges = lines.map((l) => splitTN(l)).sort((a, b) => a[0] - b[0]);

  while (ranges.length) {
    const range = ranges.find((r) => r[0] <= pos && r[1] >= pos);
    if (!range) {
      const nextRange = ranges.find((r) => r[0] >= pos);
      if (!nextRange) {
        total++;
        pos++;
        continue;
      }
      total += nextRange[0] - pos;
      pos = nextRange[0];
      continue;
    }

    pos = Math.max(pos, range[1] + 1);
    ranges = ranges.filter((r) => r[1] >= pos);
    log(pos);
  }

  log('');

  return total;
};

export const correctOutput = 146;
