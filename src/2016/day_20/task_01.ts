import { Solution } from '../../types/solution';
import {splitTN} from '../../lib/numbers';


export const solve: Solution = async (lines, {log}) => {
  let answer = 0;

  let ranges = lines.map(l => splitTN(l));

  while(ranges.some(l => l[0] <= answer && l[1] >= answer)) {
    const range = ranges.find(r => r[0] <= answer && r[1] >= answer);
    if (!range) {
      answer++;
      continue;
    }

    answer = Math.max(answer, range[1]+1);
    ranges = ranges.filter(r => r[1] >= answer);
    log(answer);
  }

  return answer;
};

export const correctOutput = 4793564;
