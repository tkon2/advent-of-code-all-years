import { log } from 'console';
import { Solution } from '../../types/solution';
import { tn } from '../../lib/functions';

const toNum = ([x, y]: [number, number]) => {
  const conversion = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
  ];
  return conversion?.[x]?.[y];
};

export const solve: Solution = async (data) => {
  let code = '';
  let pos: [number, number] = [1, 1];
  data.forEach((line) => {
    for (let move of line.split('')) {
      if (move === 'R' && pos[1] < 2) pos[1] += 1;
      if (move === 'L' && pos[1] > 0) pos[1] -= 1;
      if (move === 'U' && pos[0] > 0) pos[0] -= 1;
      if (move === 'D' && pos[0] < 2) pos[0] += 1;
    }
    code = code + toNum(pos);
  });

  return tn(code);
};

export const correctOutput = 61529;
