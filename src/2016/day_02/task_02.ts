import { log } from 'console';
import { Solution } from '../../types/solution';

const toNum = ([x, y]: [number, number]) => {
  const conversion = [
    '       ',
    '   1   ',
    '  234  ',
    ' 56789 ',
    '  ABC  ',
    '   D   ',
    '       ',
  ];
  return conversion?.[x]?.[y] ?? ' ';
};

export const solve: Solution = async (data) => {
  let code = '';
  let pos: [number, number] = [3, 1];
  data.forEach((line) => {
    for (let move of line.split('')) {
      if (move === 'R' && toNum([pos[0], pos[1] + 1]) !== ' ') pos[1] += 1;
      if (move === 'L' && toNum([pos[0], pos[1] - 1]) !== ' ') pos[1] -= 1;
      if (move === 'U' && toNum([pos[0] - 1, pos[1]]) !== ' ') pos[0] -= 1;
      if (move === 'D' && toNum([pos[0] + 1, pos[1]]) !== ' ') pos[0] += 1;
    }
    code = code + toNum(pos);
  });

  return code;
};

export const correctOutput = 'C2C28';
