import { log } from 'console';
import { Solution } from '../../types/solution';

export const solve: Solution = async ([line]) => {
  let facing = 0;
  let pos = [0, 0];
  for (let move of line.split(', ')) {
    if (move.includes('R')) {
      facing = (facing + 1) % 4;
    } else {
      facing = (facing - 1 + 4) % 4;
    }
    const num = parseInt(move.replace(/\w/, ''));
    if (facing === 0) pos[0] += num;
    if (facing === 1) pos[1] += num;
    if (facing === 2) pos[0] -= num;
    if (facing === 3) pos[1] -= num;
  }

  return Math.abs(pos[0]) + Math.abs(pos[1]);
};

export const correctOutput = 241;
