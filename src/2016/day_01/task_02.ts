import { log } from 'console';
import { Solution } from '../../types/solution';

export const solve: Solution = async ([line]) => {
  let facing = 0;
  let pos: [number, number] = [0, 0];
  let positions: [number, number][] = [];
  for (let move of line.split(', ')) {
    if (move.includes('R')) {
      facing = (facing + 1) % 4;
    } else {
      facing = (facing - 1 + 4) % 4;
    }
    const num = parseInt(move.replace(/\w/, ''));
    for (let i = 0; i < num; i++) {
      if (facing === 0) pos[0] += 1;
      if (facing === 1) pos[1] += 1;
      if (facing === 2) pos[0] -= 1;
      if (facing === 3) pos[1] -= 1;

      if (positions.some((p) => p[0] === pos[0] && p[1] === pos[1])) {
        return Math.abs(pos[0]) + Math.abs(pos[1]);
      }

      positions.push([...pos]);
    }
  }

  return Math.abs(pos[0]) + Math.abs(pos[1]);
};

export const correctOutput = 116;
