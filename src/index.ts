import { exists } from './helpers/promises';
import { Solution } from './types/solution';
import { readClipboard, readStrings } from './helpers/readInput';
import { makePath } from './helpers/makePath';
import { styleText } from 'node:util';

type TextColor = Parameters<typeof styleText>[0];

const formatTime = (t: number, short: boolean = false, highlight = false) => {
  let color: TextColor = 'yellow';
  if (highlight && t >= 10000) color = ['bgRed', 'white'];
  else if (highlight && t >= 2000) color = 'redBright';
  else if (highlight && t >= 10) color = 'yellowBright';
  else if (highlight) color = 'greenBright';
  if (t > 2000) {
    return short
      ? `${styleText(color, `${Math.round(t) / 1000}`)} s`
      : `${styleText(color, `${Math.round(t) / 1000}`)} seconds`;
  } else {
    return short
      ? `${styleText(color, `${+t.toFixed(3)}`)} ms`
      : `${styleText(color, `${+t.toFixed(3)}`)} milliseconds`;
  }
};

type Config = {
  clip: boolean;
  time: boolean;
};

const solve = async (
  year: number,
  day: number,
  task: number,
  clip: boolean,
  logging: boolean = false,
) => {
  try {
    const {
      solve: solveTask,
      correctOutput,
    }: {
      solve: Solution;
      correctOutput: string | number;
    } = require(makePath(year, day, task));
    try {
      const data = await (clip ? readClipboard() : readStrings(year, day));

      const log = logging ? console.log : () => null;

      const answer = await solveTask(data, {
        logging,
        log,
        clip,
      });

      // If we are logging we ignore correct
      if (!logging && !correctOutput) return answer + ' ?';
      if (!logging && answer !== correctOutput) return answer + ' !';
      return answer;
    } catch (e) {
      console.error(e);
      return null;
    }
  } catch (e) {
    console.error(e);
    return null;
  }
};

const solveTask = async (
  year: number,
  day: number,
  task: number,
  { clip }: Config,
) => {
  const ans = await solve(year, day, task, clip, true);
  console.log(ans);
};

const solveDay = async (
  year: number,
  day: number,
  { clip, time }: Config,
  prefix: string = '',
) => {
  const initialTime = performance.now();
  Bun.write(Bun.stdout, `${prefix}Task 1: `);
  let ans1 = (await solve(year, day, 1, clip)) ?? 'Not solved';
  const betweenTime = performance.now();
  Bun.write(Bun.stdout, `${styleText('yellow', `${ans1}`)} `);
  if (day < 25) {
    const spacing = ' '.repeat(Math.max(0, 18 - `${ans1}`.length));
    Bun.write(Bun.stdout, `${spacing}Task 2: `);
    let ans2 = (await solve(year, day, 2, clip)) ?? 'Not solved';
    console.log(ans2);
  } else {
    // Add the newline at the end
    console.log('');
  }
  if (time) {
    Bun.write(Bun.stdout, `${prefix}`);
    const firstTime = formatTime(betweenTime - initialTime, true, true);
    Bun.write(Bun.stdout, firstTime);
    if (day < 25) {
      const spacing = ' '.repeat(Math.max(0, 37 - `${firstTime}`.length));
      Bun.write(Bun.stdout, spacing);
      const secondTime = formatTime(
        performance.now() - betweenTime,
        true,
        true,
      );
      Bun.write(Bun.stdout, secondTime);
    }
    console.log('');
  }
};

const solveYear = async (year: number, conf: Config, prefix: string = '') => {
  for (let day = 1; day <= 25; day += 1) {
    const dayExists = await exists(makePath(year, day));
    if (!dayExists) continue;
    console.log(`${prefix}Day ${day}:`);
    await solveDay(year, day, conf, `    ${prefix}`);
  }
};

const solveAll = async (conf: Config) => {
  for (let year = 2010; year <= new Date().getFullYear(); year += 1) {
    const yearExists = await exists(`./src/${year}`);
    if (!yearExists) continue;
    console.log(`Year ${year}:`);
    await solveYear(year, conf, '    ');
  }
};

(async () => {
  let args = process.argv.slice(2);

  if (args.includes('-h')) {
    const helpText: string[] = [];
    helpText.push('');
    helpText.push('Usage:');
    helpText.push('');
    helpText.push('[command] [-h] [-c] [year [day [task]]]');
    helpText.push('');
    helpText.push('-h     Show this help text');
    helpText.push('');
    helpText.push('-c     Use clipboard contents instead of stored data');
    helpText.push('');
    helpText.push('-t     Measure time taken on individual tasks');
    helpText.push('');
    helpText.push('year   Only solve this year');
    helpText.push('       Valid values are 2015 - current year');
    helpText.push('');
    helpText.push('day    Only solve this day in that year');
    helpText.push('       Valid values are 1 - 25');
    helpText.push('');
    helpText.push('task   Only solve this task on that day');
    helpText.push('       Valid values are 1 and 2');
    helpText.push('');
    console.log(helpText.join('\n    '));
    return;
  }

  let clip = false;
  let time = false;
  if (args.includes('-c')) {
    clip = true;
    args = args.filter((a) => a !== '-c');
  }
  if (args.includes('-t')) {
    time = true;
    args = args.filter((a) => a !== '-t');
  }

  const year = parseInt(args[0], 10) || null;
  const day = parseInt(args[1], 10) || null;
  const task = parseInt(args[2], 10) || null;

  const startTime = performance.now();

  if (year && day && task) {
    await solveTask(year, day, task, { clip, time });
  } else if (year && day) {
    await solveDay(year, day, { clip, time });
  } else if (year) {
    await solveYear(year, { clip, time });
  } else {
    await solveAll({ clip, time });
  }
  const timeTaken = +(performance.now() - startTime).toFixed(3);
  console.log();
  console.log('Solved in', formatTime(timeTaken));
})();
