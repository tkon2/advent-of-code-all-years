import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let answer = 0;

  outer: for (let a = 0; a < lines.length; a++) {
    const A = tn(lines[a]);
    for (let b = 0; b < lines.length; b++) {
      const B = tn(lines[b]);
      for (let c = 0; c < lines.length; c++) {
        const C = tn(lines[c]);
        if (A + B + C !== 2020) continue;
        answer = A * B * C;
        break outer;
      }
    }
  }

  return answer;
};

export const correctOutput = 9199664;
