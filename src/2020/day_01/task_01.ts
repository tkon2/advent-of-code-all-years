import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let answer = 0;

  outer: for (let a = 0; a < lines.length; a++) {
    const A = tn(lines[a]);
    for (let b = 0; b < lines.length; b++) {
      const B = tn(lines[b]);
      if (A + B !== 2020) continue;
      answer = A * B;
      break outer;
    }
  }

  return answer;
};

export const correctOutput = 842016;
