import { splitTN } from '../../lib/numbers';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    const [range, letter, str] = line.split(/:? /);
    const [from, to] = splitTN(range);
    const charCount = str.split('').filter((s) => s === letter).length;
    if (charCount < from) continue;
    if (charCount > to) continue;
    log(from, to, letter, str);
    answer++;
  }

  return answer;
};

export const correctOutput = 620;
