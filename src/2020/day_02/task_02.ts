import { splitTN } from '../../lib/numbers';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    const [range, letter, str] = line.split(/:? /);
    const [c1, c2] = splitTN(range);
    const h1 = str[c1 - 1] === letter;
    const h2 = str[c2 - 1] === letter;
    if (h1 && h2) continue;
    if (!h1 && !h2) continue;
    log(range, letter, str, h1, h2);
    answer++;
  }

  return answer;
};

export const correctOutput = 727;
