import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 1;
  const slopes: [number, number][] = [
    [1, 1],
    [3, 1],
    [5, 1],
    [7, 1],
    [1, 2],
  ];

  const runSlope = ([x, y]: [number, number]) => {
    let trees = 0;
    let c = 0;
    for (let l = 0; l < lines.length; l += y) {
      const line = lines[l];
      if (line[c] === '#') trees++;
      c += x;
      c %= lines[0].length;
    }
    log([x, y], trees);
    answer *= trees;
  };

  for (const slope of slopes) runSlope(slope);

  return answer;
};

export const correctOutput = 1115775000;
