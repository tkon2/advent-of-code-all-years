import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let answer = 0;

  for (let l = 0, c = 0; l < lines.length; l++) {
    const line = lines[l];
    if (line[c] === '#') answer++;
    c = (c + 3) % lines[0].length;
  }

  return answer;
};

export const correctOutput = 225;
