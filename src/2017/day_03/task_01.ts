import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

/*
 * square root of number, then the centers can be worked out
 * 37 36 35 34 33 32 31
 * 38 17 16 15 14 13 30
 * 39 18  5  4  3 12 29
 * 40 19  6  1  2 11 28
 * 41 20  7  8  9 10 27  <--  9 = 3 * 3
 * 42 21 22 23 24 25 26  <-- 25 = 5 * 5
 * 43 44 45 46 47 48 49  <-- 49 = 7 * 7
 * ...                   <-- 81 = 9 * 9
 * ...                   ....
 */

export const solve: Solution = async ([line], { log }) => {
  const pos = tn(line);

  let sideLength = Math.ceil(Math.sqrt(pos));
  if (sideLength % 2 === 0) sideLength++;

  /** The highest value in the current layer */
  let maxLayer = Math.pow(sideLength, 2);

  log({ maxLayer, sideLength });

  const firstSide = maxLayer - Math.floor(sideLength / 2);
  const secondSide = firstSide - (sideLength - 1);
  const thirdSide = secondSide - (sideLength - 1);
  const fourthSide = thirdSide - (sideLength - 1);

  log({ firstSide, secondSide, thirdSide, fourthSide });

  const sideDistance = Math.min(
    Math.abs(pos - firstSide),
    Math.abs(pos - secondSide),
    Math.abs(pos - thirdSide),
    Math.abs(pos - fourthSide),
  );

  const layer = Math.floor(sideLength / 2);

  log({ layer, sideDistance });

  return layer + sideDistance;
};

export const correctOutput = 438;
