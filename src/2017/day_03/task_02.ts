import { tn } from '../../lib/functions';
import { get8N, get8NGeneric } from '../../lib/grids';
import { Solution } from '../../types/solution';

/*
 * 37 36 35 34 33 32 31
 * 38 17 16 15 14 13 30
 * 39 18  5  4  3 12 29
 * 40 19  6  1  2 11 28
 * 41 20  7  8  9 10 27
 * 42 21 22 23 24 25 26
 * 43 44 45 46 47 48 49
 */

export const solve: Solution = async ([line], { log }) => {
  const target = tn(line);
  let answer: number | null = null;

  let grid = new Map<string, number>();
  grid.set('0,0', 1);

  const getVal = (x: number, y: number) => grid.get(`${x},${y}`);

  const addValue = (x: number, y: number) => {
    const ns = get8NGeneric(x, y, getVal);

    const val = ns.reduce((p, v) => p + (v.v ?? 0), 0);

    grid.set(`${x},${y}`, val);

    if (val > target) {
      answer = val;
      return true;
    }
    return false;
  };

  mainLoop: for (let outer = 1; outer < 10; outer++) {
    // Right
    for (let y = outer - 1; y >= -outer; y--) {
      if (addValue(outer, y)) break mainLoop;
    }
    // Top
    for (let x = outer - 1; x >= -outer; x--) {
      if (addValue(x, -outer)) break mainLoop;
    }
    // Left
    for (let y = 1 - outer; y <= outer; y++) {
      if (addValue(-outer, y)) break mainLoop;
    }
    // Bottom
    for (let x = 1 - outer; x <= outer; x++) {
      if (addValue(x, outer)) break mainLoop;
    }
  }

  for (let i = -5; i < 5; i++) {
    let l = '';
    for (let j = -5; j < 5; j++) {
      l += ' ';
      if (i === 0 && j === 0) {
        l += `[1]`.padStart(`${target}`.length);
      } else {
        l += `${grid.get(`${j},${i}`) ?? '.'}`.padStart(`${target}`.length);
      }
    }
    log(l);
  }

  return answer;
};

export const correctOutput = 266330;
