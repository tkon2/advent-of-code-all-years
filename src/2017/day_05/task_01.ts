import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  const instructions = lines.map(tn);
  let current = 0;
  let answer = 0;

  for (; current < instructions.length; answer++) {
    current += instructions[current]++;
  }

  log(instructions);

  return answer;
};

export const correctOutput = 318883;
