import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let line = '';

  let ignore = false;
  let garbage = false;
  for (let char of lines[0]) {
    if (ignore) ignore = false;
    else if (char === '!') ignore = true;
    else if (char === '>' && garbage) garbage = false;
    else if (char === '<' && !garbage) garbage = true;
    else if (garbage) line += char;
  }

  log(line);

  return line.length;
};

export const correctOutput = 5288;
