import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  let line = '';

  let ignore = false;
  let garbage = false;
  for (let char of lines[0]) {
    if (ignore) ignore = false;
    else if (char === '!') ignore = true;
    else if (char === '>' && garbage) garbage = false;
    else if (char === '<') garbage = true;
    else if (garbage) continue;
    else if (char === ',') continue;
    else line += char;
  }

  log(line);

  while (line.length) {
    const index = line.indexOf('{}');
    if (index < 0) throw new Error('No brackets found');
    let depth = 0;
    for (let i = index; i > 0; i--) {
      depth += line[i] === '{' ? 1 : -1;
    }
    answer += 1 + depth;
    line = line.slice(0, index) + line.slice(index + 2);
  }

  return answer;
};

export const correctOutput = 11089;
