import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

const getOther = (current: [number, number], val: number) => {
  const i = current.indexOf(val);
  return i === 0 ? current[1] : current[0];
};

export const solve: Solution = async (lines, { log }) => {
  const components = lines.map((l) => l.split('/').map(tn) as [number, number]);

  log(components);

  const getAvailableFrom = (
    path: [number, number][],
    next: number,
    available: [number, number][],
  ): [number, number][][] => {
    let paths: [number, number][][] = [path];
    for (let i = 0; i < available.length; i++) {
      if (available[i].includes(next)) {
        paths = [
          ...paths,
          ...getAvailableFrom(
            [...path, available[i]],
            getOther(available[i], next),
            available.filter((_, j) => j !== i),
          ),
        ];
      }
    }
    return paths;
  };

  const paths = getAvailableFrom([], 0, components);

  let strongest = 0;
  let longest = 0;
  paths.forEach((p) => {
    const strength = p.reduce((p, c) => p + c[0] + c[1], 0);
    const length = p.length;
    if (length === longest) {
      log(p);
      log(length, 'Equal,', strength);
      strongest = Math.max(strongest, strength);
    } else if (length > longest) {
      log(p);
      log(length, 'Greater,', strength);
      strongest = strength;
      longest = length;
    }
  });

  return strongest;
};

export const correctOutput = 1824;
