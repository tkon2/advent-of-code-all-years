import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let count = 0;

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    const words = line.split(' ').map((v) =>
      v
        .split('')
        .sort((a, b) => a.localeCompare(b))
        .join(''),
    );
    if (words.length === new Set(words).size) count++;
  }

  return count;
};

export const correctOutput = 223;
