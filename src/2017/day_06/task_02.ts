import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

export const solve: Solution = async ([line], { log }) => {
  const banks = line.split(/\t| /).map(tn);
  const seen = new Set<string>();
  const getKey = () => banks.join(',');
  let answer = 0;

  const iterate = () => {
    const index = banks.indexOf(Math.max(...banks));
    const distribute = banks[index];
    banks[index] = 0;
    for (let i = 1; i <= distribute; i++) {
      banks[(index + i) % banks.length]++;
    }
  };

  while (!seen.has(getKey())) {
    seen.add(getKey());
    iterate();
  }

  const target = getKey();

  for (; answer === 0 || getKey() !== target; answer++) {
    log(banks.map((b) => `${b}`.padStart(2)).join(' '));
    iterate();
  }

  return answer;
};

export const correctOutput = 2793;
