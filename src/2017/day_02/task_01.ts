import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let total = 0;

  for (let i = 0; i < lines.length; i++) {
    const line = lines[i];
    const numbers = line.split('\t').map(tn);
    log(numbers, Math.max(...numbers) - Math.min(...numbers));
    total += Math.max(...numbers) - Math.min(...numbers);
  }

  return total;
};

export const correctOutput = 46402;
