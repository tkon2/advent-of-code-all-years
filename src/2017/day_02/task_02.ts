import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let total = 0;

  for (let i = 0; i < lines.length; i++) {
    const line = lines[i];
    const numbers = line.split(/\t| /).map(tn);
    log(numbers);
    outer: for (let j = 0; j < numbers.length; j++) {
      for (let jj = j + 1; jj < numbers.length; jj++) {
        const a = numbers[j];
        const b = numbers[jj];
        if (a % b === 0) {
          total += a / b;
          log(a, '/', b, '=', a / b);
          break outer;
        }
        if (b % a === 0) {
          total += b / a;
          log(b, '/', a, '=', b / a);
          break outer;
        }
      }
    }
  }

  return total;
};

export const correctOutput = 265;
