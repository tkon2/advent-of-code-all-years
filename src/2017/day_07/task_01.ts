import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  const programs = lines.map((l) => {
    const [id, weight, _, ...children] = l
      .split(' ')
      .map((v) => v.replaceAll(/[^0-9a-z]/g, ''));

    return {
      id,
      weight,
      children,
    };
  });

  let current = programs[0];

  while (programs.some((p) => p.children.includes(current.id))) {
    current = programs.find((p) => p.children.includes(current.id))!;
  }

  log(current);

  return current.id;
};

export const correctOutput = 'eqgvf';
