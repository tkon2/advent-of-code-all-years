import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

type Program = {
  id: string;
  weight: number;
  totalWeight: number | undefined;
  children: Program[];
};

const findOdd = (numbers: number[]) => {
  return numbers.map((n) => numbers.filter((v) => v === n).length < 2);
};

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  let programs = lines.map((l): Program => {
    const [id, weight, _, ...children] = l
      .split(' ')
      .map((v) => v.replaceAll(/[^0-9a-z]/g, ''));
    return {
      id,
      weight: tn(weight),
      totalWeight: undefined,
      children: children.map((c) => ({
        id: c,
        weight: 0,
        children: [],
        totalWeight: undefined,
      })),
    };
  });

  // Attach the objects
  programs.forEach((program, _i, progs) => {
    program.children = program.children.map(
      (c) => progs.find((p) => p.id === c.id)!,
    );
  });

  const calculateWeight = (n: Program | undefined) => {
    if (n === undefined) return 0;
    if (n.totalWeight) return n.totalWeight;
    let total = n.weight;
    n.children.forEach((c) => (total += calculateWeight(c)));
    n.totalWeight = total;
    return total;
  };

  let root = programs[0];

  while (programs.some((p) => p.children.some((c) => c.id === root.id))) {
    root = programs.find((p) => p.children.some((c) => c.id === root.id))!;
  }

  const rootWeights = root.children.map(calculateWeight);
  log('Root weights:', rootWeights);
  const rootOdds = findOdd(rootWeights);
  const weightDiff =
    rootWeights[rootOdds.indexOf(false)] - rootWeights[rootOdds.indexOf(true)];
  log('Total difference in weight:', weightDiff);

  let maxTries = 50;
  while (--maxTries > 0) {
    answer = root.weight + weightDiff;
    const kids = root.children;
    const kidWeights = kids.map(calculateWeight);
    const odd = findOdd(kidWeights);

    log(
      `${root.id}:`,
      kids.map((w, i) => (odd[i] ? `[${w.id}]` : ` ${w.id} `)).join(''),
      '|',
      kidWeights.map((w, i) => (odd[i] ? `[${w}]` : ` ${w} `)).join(''),
    );

    if (odd.includes(true)) {
      root = kids[odd.indexOf(true)]!;
    } else {
      break;
    }
  }

  log({ ...root, children: root.children.map((v) => v.id) });
  log(root.children.map(calculateWeight));

  return answer;
};

export const correctOutput = 757;
