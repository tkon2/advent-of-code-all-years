import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

const performOp = (a: number, b: number, op: string) => {
  switch (op) {
    case '>':
      return a > b;
    case '<':
      return a < b;
    case '>=':
      return a >= b;
    case '<=':
      return a <= b;
    case '==':
      return a === b;
    case '!=':
      return a !== b;
    default:
      throw new Error(`Operator ${op} not supported`);
  }
};

export const solve: Solution = async (lines, { log }) => {
  const register = new Map<string, number>();

  for (let line of lines) {
    log(line);
    const [reg, ins, val, , checkReg, op, checkVal] = line.split(' ');
    log({ reg, ins, val, checkReg, op, checkVal });

    if (!performOp(register.get(checkReg) ?? 0, tn(checkVal), op)) continue;

    if (ins === 'inc') {
      register.set(reg, (register.get(reg) ?? 0) + tn(val));
    }
    if (ins === 'dec') {
      register.set(reg, (register.get(reg) ?? 0) - tn(val));
    }
  }

  log(register);
  return [...register.values()].reduce((c, v) => Math.max(c, v), 0);
};

export const correctOutput = 5752;
