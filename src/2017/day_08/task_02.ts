import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

const performOp = (a: number, b: number, op: string) => {
  switch (op) {
    case '>':
      return a > b;
    case '<':
      return a < b;
    case '>=':
      return a >= b;
    case '<=':
      return a <= b;
    case '==':
      return a === b;
    case '!=':
      return a !== b;
    default:
      throw new Error(`Operator ${op} not supported`);
  }
};

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  const register = new Map<string, number>();

  for (let line of lines) {
    log(line);
    const [reg, ins, val, , checkReg, op, checkVal] = line.split(' ');
    log({ reg, ins, val, checkReg, op, checkVal });

    if (!performOp(register.get(checkReg) ?? 0, tn(checkVal), op)) continue;

    let value = 0;
    if (ins === 'inc') {
      value = (register.get(reg) ?? 0) + tn(val);
    }
    if (ins === 'dec') {
      value = (register.get(reg) ?? 0) - tn(val);
    }

    answer = Math.max(answer, value);
    register.set(reg, value);
  }

  log(register);
  return answer;
};

export const correctOutput = 6366;
