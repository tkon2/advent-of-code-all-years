import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

export const solve: Solution = async ([line], { log }) => {
  let total = 0;

  log(line);

  /** Length */
  const l = line.length;
  /** HalfWay */
  const hw = l / 2;

  for (let i = 0; i < line.length; i++) {
    if (line[i] !== line[(i + hw) % l]) continue;
    total += tn(line[i]);
  }

  return total;
};

export const correctOutput = 982;
