import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

export const solve: Solution = async ([line], { log }) => {
  let total = 0;

  while (line[line.length - 1] === line[0]) {
    const char = line[0];
    line = line.substring(1);
    line += char;
  }

  log(line);

  if (line[0] === line[1]) total += tn(line[0]);

  log(total);

  for (let i = 1; i < line.length; i++) {
    if (line[i - 1] !== line[i]) continue;
    total += tn(line[i]);
  }

  return total;
};

export const correctOutput = 1047;
