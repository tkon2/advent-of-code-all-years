import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  let ll = lines
    .join('\n')
    .split('\n\n')
    .map((v) => v.split('\n'));

  const W = lines[0].length;
  const dots = '.'.repeat(W);
  const hashes = '#'.repeat(W);

  const locks = ll.filter((v) => v[0] === dots);
  const keys = ll.filter((v) => v[0] === hashes);

  log({ locks, keys });

  const getCols = (v: string[]) => {
    const cols: number[] = Array.from(Array(v[0].length)).fill(0);
    for (let i = 0; i < v.length; i++) {
      for (let j = 0; j < v[i].length; j++) {
        cols[j] += v[i][j] === '#' ? 1 : 0;
      }
    }
    return cols;
  };

  for (let lock of locks) {
    keyLoop: for (let key of keys) {
      const lockCols = getCols(lock);
      const keyCols = getCols(key);
      for (let i = 0; i < lockCols.length; i++) {
        if (lockCols[i] + keyCols[i] > lock.length) continue keyLoop;
      }
      answer++;
    }
  }

  return answer;
};

export const correctOutput = 3021;
