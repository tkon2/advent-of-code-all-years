import { tn } from '../../lib/functions';
import { splitTN } from '../../lib/numbers';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  let numbers: number[] = [];

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    numbers = splitTN(line);
  }

  log(numbers);

  const memo = new Map<string, number>();

  const blinkNum = (num: number, remaining: number = 75): number => {
    if (memo.has(`${num},${remaining}`)) {
      return memo.get(`${num},${remaining}`)!;
    }
    let didBlink = 0;
    if (remaining <= 0) return 1;
    const numLen = `${num}`.length;
    if (num === 0) {
      didBlink = blinkNum(1, remaining - 1);
    } else if (numLen % 2 === 0 && numLen > 1) {
      const first = `${num}`.slice(0, numLen / 2);
      const second = `${num}`.slice(numLen / 2);
      didBlink =
        blinkNum(tn(first), remaining - 1) +
        blinkNum(tn(second), remaining - 1);
    } else {
      didBlink = blinkNum(num * 2024, remaining - 1);
    }
    memo.set(`${num},${remaining}`, didBlink);
    return didBlink;
  };

  for (let num of numbers) {
    answer += blinkNum(num);
    log(num, answer);
  }

  return answer;
};

export const correctOutput = 264350935776416;
