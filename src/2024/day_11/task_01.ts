import { tn } from '../../lib/functions';
import { splitTN } from '../../lib/numbers';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let arr: number[] = [];

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    arr = splitTN(line);
  }

  log(arr);

  const blink = () => {
    const newStones: number[] = [];
    for (let stone of arr) {
      const numLen = `${stone}`.length;
      if (stone === 0) {
        newStones.push(1);
      } else if (numLen % 2 === 0 && numLen > 1) {
        const first = `${stone}`.slice(0, numLen / 2);
        const second = `${stone}`.slice(numLen / 2);
        newStones.push(tn(first));
        newStones.push(tn(second));
      } else {
        newStones.push(stone * 2024);
      }
    }
    return newStones;
  };

  for (let i = 0; i < 25; i++) {
    log(i, arr.length);
    arr = blink();
  }

  return arr.length;
};

export const correctOutput = 222461;
