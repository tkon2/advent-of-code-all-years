import { splitTN } from '../../lib/numbers';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];

    const numbers = splitTN(line);

    let good = true;
    log(numbers);
    let dir: number | undefined = undefined;
    for (let i = 1; good === true && i < numbers.length; i++) {
      if (dir === undefined) dir = Math.sign(numbers[i] - numbers[i - 1]);
      if (Math.sign(numbers[i] - numbers[i - 1]) !== dir) {
        log('Not all increasing or decreasing');
        good = false;
      }
      const diff = Math.abs(numbers[i] - numbers[i - 1]);
      if (diff === 0) {
        log('No increase or decrease');
        good = false;
      }
      if (diff > 3) {
        good = false;
        log('Diff too high:', diff);
      }
    }

    if (good) {
      answer += 1;
      log('-----------GOOD-----------');
    }
  }

  return answer;
};

export const correctOutput = 432;
