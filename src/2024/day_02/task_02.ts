import { splitTN } from '../../lib/numbers';
import { Solution } from '../../types/solution';

const checkNumbers = (numbers: number[]) => {
  let good = true;
  let dir: number | undefined = undefined;
  for (let i = 1; good === true && i < numbers.length; i++) {
    if (dir === undefined) dir = Math.sign(numbers[i] - numbers[i - 1]);
    if (Math.sign(numbers[i] - numbers[i - 1]) !== dir) {
      good = false;
    }
    const diff = Math.abs(numbers[i] - numbers[i - 1]);
    if (diff === 0 || diff > 3) {
      good = false;
    }
  }
  return good;
};

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];

    const numbers = splitTN(line);

    log(numbers);

    if (checkNumbers(numbers)) {
      answer++;
      log('Worked!');
      continue;
    }

    for (let i = 0; i < numbers.length; i++) {
      const newArr = [...numbers.slice(0, i), ...numbers.slice(i + 1)];
      log('Checking fix', newArr);
      if (checkNumbers(newArr)) {
        answer++;
        log('Fix worked!');
        break;
      }
    }
  }

  return answer;
};

export const correctOutput = 488;
