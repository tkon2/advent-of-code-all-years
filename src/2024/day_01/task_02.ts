import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

const countNumber = (list: number[], find: number) => {
  return list.reduce((p, v) => p + (v === find ? 1 : 0), 0);
};

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  let left: number[] = [];
  let right: number[] = [];
  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    const [ll, rr] = line.split(/ +/).map(tn);
    left.push(ll);
    right.push(rr);
  }

  for (let i = 0; i < left.length; i++) {
    log(i, left[i], countNumber(right, left[i]));
    answer += left[i] * countNumber(right, left[i]);
  }

  return answer;
};

export const correctOutput = 22588371;
