import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  const left: number[] = [];
  const right: number[] = [];
  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    const [ll, rr] = line.split(/ +/).map(tn);
    left.push(ll);
    right.push(rr);
  }

  left.sort((a, b) => a - b);
  right.sort((a, b) => a - b);

  for (let i = 0; i < left.length; i++) {
    log(i, left[i], right[i]);
    answer += Math.abs(left[i] - right[i]);
  }

  return answer;
};

export const correctOutput = 1590491;
