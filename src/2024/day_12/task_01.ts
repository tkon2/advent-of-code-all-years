import { Queue } from 'data-structure-typed';
import { Position } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  const taken = new PositionSet<Position>();
  const groups: Position[][] = [];

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    for (let c = 0; c < line.length; c++) {
      const pos: Position = [l, c];
      if (taken.has(pos)) continue;
      const char = line[c];
      let group: Position[] = [];
      const candidates = new Queue([pos]);
      while (!candidates.isEmpty()) {
        const candidate = candidates.shift();
        if (!candidate) throw new Error('No items left');
        if (lines[candidate[0]][candidate[1]] !== char) continue;
        if (taken.has(candidate)) continue;

        group.push(candidate);
        taken.add(candidate);

        const [cl, cc] = candidate;
        if (cl > 0) candidates.push([cl - 1, cc]);
        if (cc > 0) candidates.push([cl, cc - 1]);
        if (cl < lines.length - 1) candidates.push([cl + 1, cc]);
        if (cc < line.length - 1) candidates.push([cl, cc + 1]);
      }
      groups.push(group);
    }
  }

  log(groups.map((g) => g.map((p) => `(${p.join(',')})`).join(',')).join('\n'));

  for (let group of groups) {
    let perimiter = 0;
    const char = lines[group[0][0]][group[0][1]];
    for (let [l, c] of group) {
      if (lines[l - 1]?.[c] !== char) perimiter++;
      if (lines[l]?.[c - 1] !== char) perimiter++;
      if (lines[l + 1]?.[c] !== char) perimiter++;
      if (lines[l]?.[c + 1] !== char) perimiter++;
    }
    log(
      'Group',
      char,
      'at',
      group[0],
      'has area',
      group.length,
      'and perimiter',
      perimiter,
    );
    answer += perimiter * group.length;
  }

  return answer;
};

export const correctOutput = 1396298;
