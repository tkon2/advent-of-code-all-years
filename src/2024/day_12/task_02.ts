import { Queue } from 'data-structure-typed';
import { tn } from '../../lib/functions';
import { splitTN } from '../../lib/numbers';
import { Position } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  const taken = new PositionSet<Position>();
  const groups: Position[][] = [];

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    for (let c = 0; c < line.length; c++) {
      const pos: Position = [l, c];
      if (taken.has(pos)) continue;
      const char = line[c];
      let group: Position[] = [];
      const candidates = new Queue([pos]);
      while (!candidates.isEmpty()) {
        log(candidates.size);
        const candidate = candidates.shift();
        if (!candidate) throw new Error('No items left');
        const [cl, cc] = candidate;
        log(candidate, lines[candidate[0]][candidate[1]]);
        if (lines[candidate[0]][candidate[1]] !== char) {
          log("Candidate doesn't match", candidate);
          continue;
        }
        if (taken.has(candidate)) {
          log('Already seen', candidate);
          continue;
        }
        group.push(candidate);
        taken.add(candidate);
        log('Added!');
        log('Before', candidates.size);
        if (cl > 0) candidates.push([cl - 1, cc]);
        if (cc > 0) candidates.push([cl, cc - 1]);
        if (cl < lines.length - 1) candidates.push([cl + 1, cc]);
        if (cc < line.length - 1) candidates.push([cl, cc + 1]);
        log('After', candidates.size);
        log(candidates.toArray());
      }
      log('Final group:', group);
      groups.push(group);
    }
  }

  for (let group of groups) {
    let perimiter = 0;
    const char = lines[group[0][0]][group[0][1]];
    for (let [ll, cc] of group) {
      const u = lines[ll - 1]?.[cc] === char;
      const r = lines[ll]?.[cc + 1] === char;
      const d = lines[ll + 1]?.[cc] === char;
      const l = lines[ll]?.[cc - 1] === char;
      const ul = lines[ll - 1]?.[cc - 1] === char;
      const ur = lines[ll - 1]?.[cc + 1] === char;
      const dl = lines[ll + 1]?.[cc - 1] === char;
      const dr = lines[ll + 1]?.[cc + 1] === char;

      if (!u && !r) perimiter++;
      if (!r && !d) perimiter++;
      if (!d && !l) perimiter++;
      if (!l && !u) perimiter++;
      if (!ul && u && l) perimiter++;
      if (!ur && u && r) perimiter++;
      if (!dl && d && l) perimiter++;
      if (!dr && d && r) perimiter++;
    }
    log(char, perimiter);
    answer += perimiter * group.length;
  }

  log(groups);

  // NOT 458492

  return answer;
};

export const correctOutput = 853588;
