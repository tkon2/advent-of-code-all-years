import { splitTN } from '../../lib/numbers';
import { Solution } from '../../types/solution';

const funcs = [
  (a: number, b: number) => a + b,
  (a: number, b: number) => a * b,
];

const doOp = (target: number, val: number, [next, ...rest]: number[]) => {
  if (next === undefined) return val === target;
  for (let func of funcs) {
    if (doOp(target, func(val, next), rest)) return true;
  }
  return false;
};

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    const [check, ...nums] = splitTN(line);

    log('Checking', check, nums);
    if (doOp(check, nums[0], nums.slice(1))) {
      answer += check;
      log('Correct!');
    } else {
      log('No solution found');
    }
  }

  return answer;
};

export const correctOutput = 4555081946288;
