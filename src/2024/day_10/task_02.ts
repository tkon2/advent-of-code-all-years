import { UndirectedGraph } from 'data-structure-typed';
import { Position } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';
import { tn } from '../../lib/functions';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  let trailheads = new PositionSet<Position>();
  const G = new UndirectedGraph<[number, number, number]>();

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    for (let c = 0; c < line.length; c++) {
      const char = line[c];
      if (char === '0') trailheads.add([l, c]);
      const id = [l, c].join(',');
      G.addVertex(id, [l, c, tn(char)]);
      if (l > 0) G.addEdge(id, [l - 1, c].join(','));
      if (c > 0) G.addEdge(id, [l, c - 1].join(','));
    }
  }

  const findPath = (cl: number, cr: number, v: number) => {
    if (lines[cl][cr] === '9') return 1;
    let count = 0;
    for (let n of G.getNeighbors([cl, cr].join(','))) {
      const [l, c, h] = n.value!;
      if (h === v) count += findPath(l, c, v + 1);
    }
    return count;
  };

  for (let trailhead of trailheads.toArray()) {
    const [r, c] = trailhead;
    const score = findPath(r, c, 1);
    log('(%d,%d) =', r, c, score);
    answer += score;
  }

  return answer;
};

export const correctOutput = 1120;
