import { UndirectedGraph } from 'data-structure-typed';
import { tn } from '../../lib/functions';
import { Position } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  let trailheads = new PositionSet<Position>();
  const G = new UndirectedGraph<[number, number, number]>();

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    for (let c = 0; c < line.length; c++) {
      const char = line[c];
      if (char === '0') trailheads.add([l, c]);
      const id = [l, c].join(',');
      G.addVertex(id, [l, c, tn(char)]);
      if (l > 0) G.addEdge(id, [l - 1, c].join(','));
      if (c > 0) G.addEdge(id, [l, c - 1].join(','));
    }
  }

  const getNextSet = (candidates: string[], nextStep: number) => {
    const newCandidates = new PositionSet<Position>();
    for (let candidate of candidates) {
      const N = G.getNeighbors(candidate);
      for (let n of N) {
        const [l, c, h] = n.value!;
        if (h !== nextStep) continue;
        newCandidates.add([l, c]);
      }
    }
    return [...newCandidates.raw];
  };

  for (let trailhead of trailheads.toArray()) {
    let candidates = [trailhead.join(',')];
    for (let current = 1; current <= 9; current++) {
      candidates = getNextSet([...candidates], current);
    }
    log('(%d,%d) =', trailhead[0], trailhead[1], candidates.length);
    answer += candidates.length;
  }

  return answer;
};

export const correctOutput = 496;
