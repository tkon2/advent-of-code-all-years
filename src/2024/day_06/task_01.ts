import { Position } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let guardPos: Position = [0, 0];
  let guardFace = 0;
  let blocks = new PositionSet<Position>();
  let visited = new PositionSet<Position>();
  let H = lines.length;
  let W = lines[0].length;

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    for (let i = 0; i < line.length; i++) {
      if (line[i] === '#') {
        blocks.add([l, i]);
      }
      if (line[i] === '^') {
        guardPos = [l, i];
      }
    }
  }

  let maxTries = 500000;
  while (
    guardPos[0] >= 0 &&
    guardPos[1] >= 0 &&
    guardPos[0] < H &&
    guardPos[1] < W &&
    maxTries-- > 0
  ) {
    visited.add(guardPos);
    let newSquare: Position = [...guardPos];
    switch (guardFace) {
      case 0:
        newSquare[0]--;
        break;
      case 1:
        newSquare[1]++;
        break;
      case 2:
        newSquare[0]++;
        break;
      case 3:
        newSquare[1]--;
        break;
    }
    if (blocks.has(newSquare)) {
      guardFace += 1;
      guardFace %= 4;
      continue;
    } else {
      guardPos = newSquare;
    }
  }

  for (let l = 0; l < lines.length; l++) {
    let line = '';
    for (let i = 0; i < lines[l].length; i++) {
      if (visited.has([l, i])) {
        line += '*';
      } else if (blocks.has([l, i])) {
        line += '#';
      } else {
        line += ' ';
      }
    }
    log(line);
  }

  return visited.size;
};

export const correctOutput = 5095;
