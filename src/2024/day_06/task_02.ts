import { Position } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;

  let startingPos: Position = [0, 0];
  let blocks = new PositionSet<Position>();
  let H = lines.length;
  let W = lines[0].length;

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    for (let i = 0; i < line.length; i++) {
      if (line[i] === '#') {
        blocks.add([l, i]);
      }
      if (line[i] === '^') {
        startingPos = [l, i];
      }
    }
  }

  const walkPath = (extraObsticle: Position, needPositions = false) => {
    let guardPos: Position = [...startingPos];
    let guardFace = 0;
    let states = new Set<string>();
    let positions = new PositionSet();
    let hitBlock = false;

    let maxTries = 50000;
    while (
      guardPos[0] >= 0 &&
      guardPos[1] >= 0 &&
      guardPos[0] < H &&
      guardPos[1] < W &&
      maxTries-- > 0
    ) {
      if (needPositions) positions.add(guardPos);

      if (hitBlock && states.has(`${guardPos},${guardFace}`)) {
        return { loop: true, positions };
      }
      states.add(`${guardPos},${guardFace}`);

      let newSquare: Position = [...guardPos];
      switch (guardFace) {
        case 0:
          newSquare[0]--;
          break;
        case 1:
          newSquare[1]++;
          break;
        case 2:
          newSquare[0]++;
          break;
        case 3:
          newSquare[1]--;
          break;
      }
      if (
        !hitBlock &&
        extraObsticle[0] === newSquare[0] &&
        extraObsticle[1] === newSquare[1]
      ) {
        hitBlock = true;
      }
      if (
        (extraObsticle[0] === newSquare[0] &&
          extraObsticle[1] === newSquare[1]) ||
        blocks.has(newSquare)
      ) {
        guardFace += 1;
        guardFace %= 4;
      } else {
        guardPos = newSquare;
      }
    }
    return { loop: false, positions };
  };

  const originalPath = walkPath([-1, -1], true).positions;

  log(originalPath.size);

  originalPath.forEach(([l, i]) => {
    if (lines[l][i] === '^') return;
    if (walkPath([l, i]).loop) {
      answer++;
    }
  });

  return answer;
};

export const correctOutput = 1933;
