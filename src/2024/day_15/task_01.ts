import { addDelta, equal, Position } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

const getDelta = (d: string): Position => {
  switch (d) {
    case '^':
      return [-1, 0];
    case '>':
      return [0, +1];
    case 'v':
      return [+1, 0];
    case '<':
      return [0, -1];
    default:
      throw new Error(`Not a valid dir: ${d}`);
  }
};

export const solve: Solution = async (lines, { log, clip }) => {
  let answer = 0;
  let blocks = new PositionSet<Position>();
  let boxes = new PositionSet<Position>();
  let position: Position = [0, 0];
  let instructions: string[] = [];
  let W = 0;
  let H = 0;

  let readingMoves = false;
  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    if (line === '') {
      readingMoves = true;
    } else if (readingMoves) {
      instructions = [...instructions, ...line.split('')];
    } else {
      for (let c = 0; c < line.length; c++) {
        const char = line[c];
        const pos: Position = [l, c];

        W = Math.max(W, pos[1]);
        H = Math.max(H, pos[0]);

        if (char === '#') blocks.add(pos);
        else if (char === 'O') boxes.add(pos);
        else if (char === '@') position = pos;
      }
    }
  }

  const generateMapStrings = (): string[] => {
    const lines: string[] = [];
    for (let l = 0, line = ''; l <= H; l++, line = '') {
      for (let c = 0; c <= W; c++) {
        const pos: Position = [l, c];
        if (equal(position, pos)) line += '@';
        else if (blocks.has(pos)) line += '#';
        else if (boxes.has(pos)) line += 'O';
        else line += ' ';
      }
      lines.push(line);
    }
    return lines;
  };

  const beforeMap = generateMapStrings();
  for (let instruction of instructions) {
    const delta = getDelta(instruction);

    const initialMove = addDelta(position, delta);

    if (blocks.has(initialMove)) continue;
    if (!boxes.has(initialMove)) {
      position = initialMove;
      continue;
    }

    let newPos = addDelta(position, delta);
    while (!blocks.has(newPos) && boxes.has(newPos)) {
      newPos = newPos.map((v, i) => v + delta[i]) as Position;
    }

    if (blocks.has(newPos)) continue;
    if (boxes.has(newPos)) continue;

    boxes.delete(initialMove);
    boxes.add(newPos);
    position = initialMove;
  }

  const afterMap = generateMapStrings();

  for (let i = 0; i < afterMap.length; i++) {
    log(beforeMap[i], ' | ', afterMap[i]);
  }

  for (let [y, x] of boxes.toArray()) {
    answer += y * 100 + x;
  }

  return answer;
};

export const correctOutput = 1490942;
