import { addDelta, equal, Position } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

const getDelta = (d: string): Position => {
  switch (d) {
    case '^':
      return [-1, 0];
    case '>':
      return [0, +1];
    case 'v':
      return [+1, 0];
    case '<':
      return [0, -1];
    default:
      throw new Error(`Not a valid dir: ${d}`);
  }
};

export const solve: Solution = async (lines, { log, clip }) => {
  let answer = 0;
  let blocks = new PositionSet<Position>();
  let boxes = new PositionSet<Position>();
  let position: Position = [0, 0];
  let instructions: string[] = [];
  let W = 0;
  let H = 0;

  let readingMoves = false;
  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    if (line === '') {
      readingMoves = true;
    } else if (readingMoves) {
      instructions = [...instructions, ...line.split('')];
    } else {
      for (let c = 0; c < line.length; c++) {
        const char = line[c];
        const pos: Position = [l, c * 2];

        W = Math.max(W, pos[1]);
        H = Math.max(H, pos[0]);

        if (char === '#') blocks.add(pos);
        else if (char === 'O') boxes.add(pos);
        else if (char === '@') position = pos;
      }
    }
  }

  const showMap = () => {
    for (let l = 0, line = ''; l <= H; l++, line = '') {
      for (let c = 0; c <= W; c += 1) {
        const pos: Position = [l, c];
        if (equal(position, pos)) line += '@';
        else if (hasBlock(pos)) line += '#';
        else if (hasBox(pos)) line += boxes.has(pos) ? '[' : ']';
        else line += ' ';
      }
      log(line);
    }
  };

  const hasBlock = (p: Position) => {
    if (blocks.has(p)) return true;
    if (blocks.has([p[0], p[1] - 1])) return true;
    return false;
  };

  const hasBox = (p: Position) => {
    if (boxes.has(p)) return true;
    if (boxes.has([p[0], p[1] - 1])) return true;
    return false;
  };

  /**
   * Get the two positions the block occupies, always returns the actual block
   * position at [0]. Returns null if the position is not a block
   */
  const getBoxPoses = (p: Position): Position[] | null => {
    if (!hasBox(p)) return null;
    return boxes.has(p)
      ? [
          [p[0], p[1]],
          [p[0], p[1] + 1],
        ]
      : [
          [p[0], p[1] - 1],
          [p[0], p[1]],
        ];
  };

  /** Return true if the box at `p` can be moved in `dir` */
  const iterateBoxes = (
    /** Position */
    p: Position,
    /** Direction */
    d: Position,
    /** Move */
    m: boolean = false,
  ): boolean => {
    if (hasBlock(p)) return false;
    const bp = getBoxPoses(p);
    if (bp === null) return true;

    /** CanMove */
    let cm = false;
    if (d[1] < 0) cm = iterateBoxes(addDelta(bp[0], d), d, m);
    else if (d[1] > 0) cm = iterateBoxes(addDelta(bp[1], d), d, m);
    else cm = bp.every((p) => iterateBoxes(addDelta(p, d), d, m));

    if (m) {
      boxes.delete(bp[0]);
      boxes.add(addDelta(bp[0], d));
    }

    return cm;
  };

  // Before
  showMap();

  for (let instruction of instructions) {
    const delta = getDelta(instruction);
    const initialMove = addDelta(position, delta);

    if (iterateBoxes(initialMove, delta)) {
      iterateBoxes(initialMove, delta, true);
      position = initialMove;
    }
  }

  // After
  log();
  showMap();

  for (let [y, x] of boxes.toArray()) {
    answer += y * 100 + x;
  }

  return answer;
};

export const correctOutput = 1519202;
