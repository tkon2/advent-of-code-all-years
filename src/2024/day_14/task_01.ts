import { splitTN } from '../../lib/numbers';
import { Position } from '../../lib/position';
import { Solution } from '../../types/solution';

type Robot = {
  pos: Position;
  vel: Position;
};

export const solve: Solution = async (lines, { log, clip }) => {
  let W = clip ? 11 : 101;
  let H = clip ? 7 : 103;
  let Wmid = Math.floor(W / 2);
  let Hmid = Math.floor(H / 2);

  let robots: Robot[] = [];

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    const [p, v] = line.split(' ').map((v) => splitTN(v, true));
    let robot = { pos: p as Position, vel: v as Position };
    // Fix velocities so they aren't negative (pre-wrap)
    robot.vel[0] = (W + robot.vel[0]) % W;
    robot.vel[1] = (H + robot.vel[1]) % H;
    robots.push(robot);
  }

  let before = robots.map((r) => [...r.pos]);

  for (let t = 0; t < 100; t++) {
    for (let robot of robots) {
      robot.pos = [
        (robot.pos[0] + robot.vel[0]) % W,
        (robot.pos[1] + robot.vel[1]) % H,
      ];
    }
  }

  let after = robots.map((r) => [...r.pos]);
  for (let i = 0; i < after.length; i++) {
    const padding = ' '.repeat(6 - `${before[i]}`.length);
    log(padding, before[i], '=>', after[i]);
  }

  let totals = [0, 0, 0, 0];

  for (let robot of robots) {
    if (robot.pos[0] === Wmid) continue;
    if (robot.pos[1] === Hmid) continue;

    let index = 0;
    if (robot.pos[0] > Wmid) {
      index += 2;
    }
    if (robot.pos[1] > Hmid) {
      index += 1;
    }
    totals[index]++;
  }

  log('Totals', totals);

  return totals.reduce((c, v) => c * v, 1);
};

export const correctOutput = 229980828;
