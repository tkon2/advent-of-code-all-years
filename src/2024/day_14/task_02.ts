import { splitTN } from '../../lib/numbers';
import { Position } from '../../lib/position';
import { Solution } from '../../types/solution';

type Robot = {
  pos: Position;
  vel: Position;
};

export const solve: Solution = async (lines, { log, clip }) => {
  let W = clip ? 11 : 101;
  let H = clip ? 7 : 103;

  let robots: Robot[] = [];

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    const [p, v] = line.split(' ').map((v) => splitTN(v, true));
    let robot = { pos: p as Position, vel: v as Position };
    // Fix velocities so they aren't negative (pre-wrap)
    robot.vel[0] = (W + robot.vel[0]) % W;
    robot.vel[1] = (H + robot.vel[1]) % H;

    robots.push(robot);
  }

  let t = 1;
  for (; t < W * H; t++) {
    for (let robot of robots) {
      robot.pos = [
        (robot.pos[0] + robot.vel[0]) % W,
        (robot.pos[1] + robot.vel[1]) % H,
      ];
    }

    const uniquePositions = new Set(robots.map((r) => r.pos.join(','))).size;
    if (uniquePositions !== robots.length) continue;
    log('\n\n');
    for (let y = 0; y < H; y++) {
      let line = '';
      for (let x = 0; x < W; x++) {
        line +=
          robots.filter((r) => r.pos[0] === x && r.pos[1] === y).length || ' ';
        line += ' ';
      }
      log(line);
    }
    break;
  }

  return t;
};

export const correctOutput = 7132;
