import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log, clip }) => {
  let answer = 0;
  let available = lines[0].split(', ').map((v) => v.trim());
  let patterns: string[] = [];

  for (let l = 2; l < lines.length; l++) {
    const line = lines[l];
    patterns.push(line);
  }

  const memo = new Map<string, number>();
  const checkPattern = (remaing: string) => {
    if (memo.has(remaing)) return memo.get(remaing) ?? 0;
    if (remaing.length === 0) {
      return 1;
    }
    let waysToDoIt = 0;
    for (let check of available) {
      if (remaing.startsWith(check)) {
        const newPattern = remaing.replace(check, '');
        waysToDoIt += checkPattern(newPattern);
      }
    }
    memo.set(remaing, waysToDoIt);
    return waysToDoIt;
  };

  log(available, patterns);

  patterns.forEach((p) => {
    const ans = checkPattern(p);
    log(p, ans);
    answer += ans;
  });

  return answer;
};

export const correctOutput = 1100663950563322;
