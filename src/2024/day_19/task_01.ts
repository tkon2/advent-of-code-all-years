import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log, clip }) => {
  let answer = 0;
  let available = lines[0].split(', ');
  let patterns: string[] = [];

  for (let l = 2; l < lines.length; l++) {
    const line = lines[l];
    patterns.push(line);
  }

  const checkPattern = (remaing: string) => {
    if (remaing.length === 0) return true;
    for (let check of available) {
      if (remaing.startsWith(check)) {
        const newPattern = remaing.replace(check, '');
        if (checkPattern(newPattern)) return true;
      }
    }
    return false;
  };

  log(available, patterns);

  patterns.forEach((p) => {
    log(p, checkPattern(p));
    if (checkPattern(p)) answer++;
  });

  return answer;
};

export const correctOutput = 374;
