import { Solution } from '../../types/solution';
import { tn } from '../../lib/functions';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  let nums: bigint[] = [];

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    nums.push(BigInt(tn(line)));
  }

  const mixnprune = (n: bigint, v: bigint) => {
    const xored = n ^ v;
    return xored % 16777216n;
  };

  for (const num of nums) {
    let v = num;
    for (let i = 0; i < 2000; i++) {
      v = mixnprune(v, v * 64n);
      v = mixnprune(v, v / 32n);
      v = mixnprune(v, v * 2048n);
    }
    answer += Number(v);
    log(Number(num), '=>', Number(v));
  }

  return answer;
};

export const correctOutput = 17724064040;
