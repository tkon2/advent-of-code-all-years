import { Solution } from '../../types/solution';
import { tn } from '../../lib/functions';

export const solve: Solution = async (lines, { log, logging }) => {
  let answer = 0;
  let nums: bigint[] = [];

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    nums.push(BigInt(tn(line)));
  }

  const ld = (n: bigint) => {
    return Number(n % 10n);
  };

  const mixnprune = (n: bigint, v: bigint) => {
    const xored = n ^ v;
    return xored % 16777216n;
  };

  const totals = new Map<string, number>();

  for (let n = 0; n < nums.length; n++) {
    let num = nums[n];
    let history = [];
    let oldv = ld(num);
    const seen = new Set<string>();

    for (let i = 0; i < 2000; i++) {
      num = mixnprune(num, num * 64n);
      num = mixnprune(num, num / 32n);
      num = mixnprune(num, num * 2048n);

      const v = ld(num);

      history.push(v - oldv);
      if (history.length > 4) history.shift();
      oldv = v;

      if (history.length >= 4) {
        const k = history.join(',');
        if (seen.has(k)) continue;
        seen.add(k);
        totals.set(k, (totals.get(k) ?? 0) + v);
      }
    }
  }

  if (logging) {
    log(
      'Top 20',
      new Map([...totals.entries()].sort(([, a], [, b]) => b - a).slice(0, 20)),
      'of',
      totals.size,
      'total entries',
    );
  }
  answer = [...totals.values()].reduce((v, c) => Math.max(v, c), 0);

  return answer;
};

export const correctOutput = 1998;
