import { tn } from '../../lib/functions';
import { asc } from '../../lib/sorting';
import { Solution } from '../../types/solution';

type Op = {
  a: string;
  b: string;
  op: string;
  to: string;
};

/** MakeKey */
const mk = (str: string, num: number) => str + `${num}`.padStart(2, '0');

const getVals = (l: string) => {
  const [a, op, b, _, to] = l.split(' ');
  return { a, op, b, to };
};

/** GetTo */
const gt = (l: string) => l.split(' ').at(-1)!;

const lineFilter = (vars: Partial<Op>) => (l: string) => {
  if (!l.includes('->')) return false;
  const { a, b, op, to } = getVals(l);
  if (vars.a && vars.a !== a && vars.a !== b) return false;
  if (vars.b && vars.b !== a && vars.b !== b) return false;
  if (vars.op && vars.op !== op) return false;
  if (vars.to && vars.to !== to) return false;
  return true;
};

export const solve: Solution = async (lines, { log, ...funcs }) => {
  const maxZ = lines.reduce(
    (p, v) => (v.includes('z') ? Math.max(tn(gt(v)), p) : p),
    0,
  );

  const getLines = (vars: Partial<Op>) => lines.filter(lineFilter(vars));
  const getLine = (vars: Partial<Op>) => lines.find(lineFilter(vars));

  const swap = (l1i: number, l2i: number) => {
    const l1 = gt(lines[l1i]);
    const l2 = gt(lines[l2i]);
    if (l1 === undefined || l2 === undefined) throw new Error('No string');
    lines[l1i] = lines[l1i].replace(new RegExp(`${l1}$`), l2);
    lines[l2i] = lines[l2i].replace(new RegExp(`${l2}$`), l1);
  };

  const check = (v: number) => {
    const xor = getLine({ a: mk('x', v), b: mk('y', v), op: 'XOR' });
    if (!xor) return false;
    const and = getLine({ a: mk('x', v), b: mk('y', v), op: 'AND' });
    if (!and) return false;
    const xorOut = getLine({ a: gt(xor), op: 'XOR', to: mk('z', v) });
    if (!xorOut) return false;
    const xorNext = getLine({ a: gt(xor), op: 'AND' });
    if (!xorNext) return false;
    const carry = getLine({ a: gt(xorNext), b: gt(and), op: 'OR' });
    if (!carry) return false;
    return true;
  };

  const fix = (v: number) => {
    const firstN = getLines({ a: mk('x', v), b: mk('y', v) });
    const secondN = firstN.flatMap((n) => getLines({ a: getVals(n).to }));
    const fixSet = [...firstN, ...secondN];

    for (let i = 0; i < fixSet.length; i++) {
      const ii = lines.indexOf(fixSet[i]);
      for (let j = i + 1; j < fixSet.length; j++) {
        const ji = lines.indexOf(fixSet[j]);
        swap(ii, ji);
        if (check(v)) return [ii, ji];
        swap(ii, ji);
      }
    }

    return null;
  };

  const swappedWires: string[] = [];
  for (let i = 1; i < maxZ; i++) {
    if (check(i)) continue;
    log('Found an error at', i);
    const swapped = fix(i);
    if (!swapped) continue;
    log(
      'Swapped',
      swapped.map((s) => lines[s]),
    );
    swapped.forEach((s) => {
      swappedWires.push(gt(lines[s]));
    });
  }

  return swappedWires.sort(asc).join(',');
};

export const correctOutput = 'cqr,ncd,nfj,qnw,vkg,z15,z20,z37';
