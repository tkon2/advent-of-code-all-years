import { tn } from '../../lib/functions';
import { desc } from '../../lib/sorting';
import { Solution } from '../../types/solution';

type Op = {
  a: string;
  b: string;
  op: string;
  to: string;
};

export const solve: Solution = async (lines, { log }) => {
  let gates = new Map<string, number>();
  let ops: Op[] = [];

  let readingGates = true;
  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    if (line === '') {
      readingGates = false;
      continue;
    }
    if (readingGates) {
      const [gate, val] = line.split(': ');
      gates.set(gate, tn(val));
    } else {
      const [a, op, b, _, to] = line.split(' ');
      ops.push({ a, op, b, to });
    }
  }

  const performOp = (op: string, a: string, b: string) => {
    if (gates.get(a) === undefined) return null;
    if (gates.get(b) === undefined) return null;
    const A = gates.get(a) === 1;
    const B = gates.get(b) === 1;
    if (op === 'AND') {
      return A && B ? 1 : 0;
    }
    if (op === 'XOR') {
      return (A && !B) || (!A && B) ? 1 : 0;
    }
    if (op === 'OR') {
      return A || B ? 1 : 0;
    }

    throw new Error(`Operation not supported, ${op}`);
  };

  let shouldContinue = true;
  let maxTries = 5000;
  while (shouldContinue && maxTries-- > 0) {
    shouldContinue = false;
    for (let { a, b, op, to } of ops) {
      const result = performOp(op, a, b);
      if (result === null) {
        shouldContinue = true;
        continue;
      }
      gates.set(to, result);
    }
  }

  for (let g of gates.entries()) {
    if (!g[0].startsWith('z')) gates.delete(g[0]);
  }

  log(
    [...gates.entries()]
      .sort((a, b) => desc(a[0], b[0]))
      .map((v) => v[1])
      .join(''),
  );

  return parseInt(
    [...gates.entries()]
      .sort((a, b) => desc(a[0], b[0]))
      .map((v) => v[1])
      .join(''),
    2,
  );
};

export const correctOutput = 48508229772400;
