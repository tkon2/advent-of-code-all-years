import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;

  const grid: string[][] = [];

  const checkXmas = (y: number, x: number) => {
    log('Checking', x, y);
    const checks = [
      // +x
      [grid[y]?.[x], grid[y]?.[x + 1], grid[y]?.[x + 2], grid[y]?.[x + 3]].join(
        '',
      ),
      // -x
      [grid[y]?.[x], grid[y]?.[x - 1], grid[y]?.[x - 2], grid[y]?.[x - 3]].join(
        '',
      ),
      // +y
      [grid[y]?.[x], grid[y + 1]?.[x], grid[y + 2]?.[x], grid[y + 3]?.[x]].join(
        '',
      ),
      // -y
      [grid[y]?.[x], grid[y - 1]?.[x], grid[y - 2]?.[x], grid[y - 3]?.[x]].join(
        '',
      ),
      // +x +y
      [
        grid[y]?.[x],
        grid[y + 1]?.[x + 1],
        grid[y + 2]?.[x + 2],
        grid[y + 3]?.[x + 3],
      ].join(''),
      // +y -x
      [
        grid[y]?.[x],
        grid[y + 1]?.[x - 1],
        grid[y + 2]?.[x - 2],
        grid[y + 3]?.[x - 3],
      ].join(''),
      // -y +x
      [
        grid[y]?.[x],
        grid[y - 1]?.[x + 1],
        grid[y - 2]?.[x + 2],
        grid[y - 3]?.[x + 3],
      ].join(''),
      // -y -x
      [
        grid[y]?.[x],
        grid[y - 1]?.[x - 1],
        grid[y - 2]?.[x - 2],
        grid[y - 3]?.[x - 3],
      ].join(''),
    ];
    return checks.filter((c) => c === 'XMAS').length;
  };

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    grid.push(line.split(''));
  }

  for (let i = 0; i < grid.length; i++) {
    for (let j = 0; j < grid[i].length; j++) {
      if (grid[i][j] === 'X') {
        answer += checkXmas(i, j);
      }
    }
  }

  return answer;
};

export const correctOutput = 2496;
