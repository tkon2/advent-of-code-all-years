import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;

  const grid: string[][] = [];

  const checkXmas = (y: number, x: number) => {
    const tl = grid[y - 1]?.[x - 1];
    const br = grid[y + 1]?.[x + 1];

    const tr = grid[y - 1]?.[x + 1];
    const bl = grid[y + 1]?.[x - 1];

    log('\nChecking', x, y);
    log({ tl, br, tr, bl });

    if (tl !== 'M' && tl !== 'S') return false;
    if (br !== 'M' && br !== 'S') return false;
    if (tr !== 'M' && tr !== 'S') return false;
    if (bl !== 'M' && bl !== 'S') return false;

    log('Valid:', tl !== br, tr !== bl);

    if (tl === br) return false;
    if (tr === bl) return false;

    log('Found an X-MAS');

    return true;
  };

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    grid.push(line.split(''));
  }

  for (let i = 1; i < grid.length - 1; i++) {
    for (let j = 1; j < grid[i].length - 1; j++) {
      if (grid[i][j] === 'A') {
        answer += checkXmas(i, j) ? 1 : 0;
      }
    }
  }

  return answer;
};

export const correctOutput = 1967;
