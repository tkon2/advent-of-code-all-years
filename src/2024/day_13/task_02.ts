import { splitTN } from '../../lib/numbers';
import { Position } from '../../lib/position';
import { Solution } from '../../types/solution';

type Machine = {
  A: Position;
  B: Position;
  G: Position;
};

const error = 10000000000000;

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;

  const machines: Machine[] = [];

  let currentMachine: Partial<Machine> = {};
  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    if (line.includes('Button A')) currentMachine.A = splitTN(line) as Position;
    if (line.includes('Button B')) currentMachine.B = splitTN(line) as Position;
    if (line.includes('Prize')) {
      currentMachine.G = splitTN(line).map((v) => v + error) as Position;
      machines.push(currentMachine as Machine);
      currentMachine = {};
    }
  }

  for (let m of machines) {
    // https://en.wikipedia.org/wiki/Cramer's_rule#Applications
    let [a1, a2, b1, b2, c1, c2] = [...m.A, ...m.B, ...m.G];
    const x = (c1 * b2 - b1 * c2) / (a1 * b2 - b1 * a2);
    const y = (a1 * c2 - c1 * a2) / (a1 * b2 - b1 * a2);

    log();
    log(m);
    if (x % 1 !== 0 || y % 1 !== 0) {
      log('No solution');
    } else {
      log('Solution at', [x, y]);
      answer += x * 3 + y;
    }
  }

  return answer;
};

export const correctOutput = 94955433618919;
