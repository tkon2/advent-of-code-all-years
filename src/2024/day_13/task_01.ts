import { splitTN } from '../../lib/numbers';
import { Position } from '../../lib/position';
import { Solution } from '../../types/solution';

type Machine = {
  A: Position;
  B: Position;
  G: Position;
};

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;

  const machines: Machine[] = [];

  let currentMachine: Partial<Machine> = {};
  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    if (line.includes('Button A')) currentMachine.A = splitTN(line) as Position;
    if (line.includes('Button B')) currentMachine.B = splitTN(line) as Position;
    if (line.includes('Prize')) {
      currentMachine.G = splitTN(line) as Position;
      machines.push(currentMachine as Machine);
      currentMachine = {};
    }
  }

  for (let m = 0; m < machines.length; m++) {
    const current = machines[m];
    log();
    log(current);
    let pos = [0, 0];
    let a = 0;
    for (; pos[0] < current.G[0] && pos[1] < current.G[1]; a++) {
      const xDiff = current.G[0] - pos[0];
      const yDiff = current.G[1] - pos[1];
      if (xDiff % current.B[0] === 0) {
        if (yDiff % current.B[1] === 0) {
          if (xDiff / current.B[0] === yDiff / current.B[1]) {
            break;
          }
        }
      }
      pos[0] += current.A[0];
      pos[1] += current.A[1];
    }

    let b = 0;
    if ((current.G[0] - pos[0]) % current.B[0] === 0) {
      if ((current.G[1] - pos[1]) % current.B[1] === 0) {
        b = (current.G[0] - pos[0]) / current.B[0];
      }
    }

    log(m, a, b);
    if (b === 0) continue;
    if (a > 100) continue;
    if (b > 100) continue;
    answer += a * 3 + b;
  }

  return answer;
};

export const correctOutput = 29711;
