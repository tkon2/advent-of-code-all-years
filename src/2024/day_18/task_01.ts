import { PriorityQueue } from 'data-structure-typed';
import { splitTN } from '../../lib/numbers';
import { equal, Position } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

type Path = {
  pos: Position;
  cost: number;
};

export const solve: Solution = async (lines, { log, clip }) => {
  let answer = 0;
  let start: Position = [0, 0];
  let end: Position = clip ? [6, 6] : [70, 70];
  let byteCount = clip ? 12 : 1024;
  let bytes = new PositionSet<Position>();

  for (let l = 0; l < lines.length && bytes.size < byteCount; l++) {
    const line = lines[l];
    const pos = splitTN(line);
    bytes.add(pos as Position);
  }

  const Q = new PriorityQueue<Path>([], {
    comparator: (a, b) => a.cost - b.cost,
  });

  Q.add({ pos: start, cost: 0 });

  log(bytes);

  const visited = new PositionSet<Position>();

  while (!Q.isEmpty()) {
    const item = Q.poll();
    if (!item) throw new Error('Item missing');
    const { pos, cost } = item;
    if (bytes.has(pos)) continue;
    if (pos[0] < 0 || pos[0] > end[0]) continue;
    if (pos[1] < 0 || pos[1] > end[1]) continue;
    if (visited.has(pos)) continue;

    if (equal(pos, end)) {
      return cost;
    }

    visited.add(pos);

    Q.add({ pos: [pos[0] + 1, pos[1]], cost: cost + 1 });
    Q.add({ pos: [pos[0] - 1, pos[1]], cost: cost + 1 });
    Q.add({ pos: [pos[0], pos[1] + 1], cost: cost + 1 });
    Q.add({ pos: [pos[0], pos[1] - 1], cost: cost + 1 });
  }

  return answer;
};

export const correctOutput = 250;
