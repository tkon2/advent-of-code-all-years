import { PriorityQueue } from 'data-structure-typed';
import { splitTN } from '../../lib/numbers';
import { equal, Position } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

type Path = {
  pos: Position;
  cost: number;
};

export const solve: Solution = async (lines, { log, clip }) => {
  let start: Position = [0, 0];
  let end: Position = clip ? [6, 6] : [70, 70];
  let byteCount = clip ? 12 : 1024;
  let byteBytes: PositionSet<Position>[] = [];

  const bytes = new PositionSet<Position>();
  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    const pos = splitTN(line);
    bytes.add(pos as Position);
    if (l >= byteCount) {
      byteBytes.push(bytes.copy());
    }
  }

  const checkArr = (bytes: PositionSet<Position>) => {
    const Q = new PriorityQueue<Path>([], {
      comparator: (a, b) => a.cost - b.cost,
    });

    Q.add({ pos: start, cost: 0 });

    const visited = new PositionSet<Position>();

    while (!Q.isEmpty()) {
      const item = Q.poll();
      if (!item) throw new Error('Item missing');
      const { pos, cost } = item;
      if (bytes.has(pos)) continue;
      if (pos[0] < 0 || pos[0] > end[0]) continue;
      if (pos[1] < 0 || pos[1] > end[1]) continue;
      if (visited.has(pos)) continue;

      if (equal(pos, end)) {
        return cost;
      }

      visited.add(pos);

      Q.add({ pos: [pos[0] + 1, pos[1]], cost: cost + 1 });
      Q.add({ pos: [pos[0] - 1, pos[1]], cost: cost + 1 });
      Q.add({ pos: [pos[0], pos[1] + 1], cost: cost + 1 });
      Q.add({ pos: [pos[0], pos[1] - 1], cost: cost + 1 });
    }

    return null;
  };

  let searchStart = 0;
  let searchEnd = byteBytes.length;
  let maxTries = byteBytes.length / 2;
  while (searchEnd - searchStart > 1 && maxTries-- > 0) {
    const index = searchStart + Math.floor((searchEnd - searchStart) / 2);
    let check = checkArr(byteBytes[index]);
    log(searchStart, searchEnd, index, check);
    if (check !== null) {
      searchStart = Math.min(index, searchEnd);
    } else {
      searchEnd = Math.max(index, searchStart);
    }
  }
  log(searchStart, searchEnd);

  return byteBytes[searchEnd].toArray().at(-1)?.join(',') ?? null;
};

export const correctOutput = '56,8';
