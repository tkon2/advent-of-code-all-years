import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  let blocks: (number | undefined)[] = [];

  let isSection = false;
  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    for (let c = 0; c < line.length; c++) {
      isSection = !isSection;
      const char = line[c];
      const num = tn(char);
      for (let i = 0; i < num; i++) {
        if (isSection) {
          blocks.push(Math.floor(c / 2));
        } else {
          blocks.push(undefined);
        }
      }
    }
  }

  log(blocks.length);
  log('BLOCKS', blocks.map((b) => b ?? '.').join(''));

  let firstEmpty = blocks.indexOf(undefined);
  let lastFilled = blocks.findLastIndex((v) => v !== undefined);
  while (firstEmpty + 1 < lastFilled) {
    const val = blocks[lastFilled];
    blocks[lastFilled] = undefined;
    blocks[firstEmpty] = val;
    firstEmpty = blocks.indexOf(undefined);
    lastFilled = blocks.findLastIndex((v) => v !== undefined);
  }

  log(blocks);

  blocks.forEach((b, i) => (answer += (b ?? 0) * i));

  return answer;
};

export const correctOutput = 6320029754031;
