import { tn } from '../../lib/functions';
import { makeUniqueLogger } from '../../lib/logging';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log, logging }) => {
  const uniqueLog = makeUniqueLogger(log);
  let answer = 0;
  let blocks: (number | null)[] = [];
  let hasMoved = new Set<number>();

  let isSection = false;
  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    for (let c = 0; c < line.length; c++) {
      isSection = !isSection;
      const char = line[c];
      const num = tn(char);
      for (let i = 0; i < num; i++) {
        if (isSection) {
          blocks.push(Math.floor(c / 2));
        } else {
          blocks.push(null);
        }
      }
    }
  }

  const showBlocks = (message: string) => {
    if (!logging) return;
    let blocksString = blocks
      .slice(0, 80)
      .map((b) => `${b ?? '.'}`.padStart(5, ' '))
      .join('');

    blocksString = `${message} ${blocksString}`;
    uniqueLog(blocksString);
  };

  log(blocks.length);
  showBlocks('BLOCKS BEFORE');

  const uniqueBlocks = new Set<number | null>(blocks);
  let maxTries = 500000;
  while (hasMoved.size < uniqueBlocks.size - 1 && maxTries-- > 0) {
    const indexEnd = blocks.findLastIndex(
      (b) => b !== null && !hasMoved.has(b),
    );
    if (indexEnd < 0) continue;
    const valToMove = blocks[indexEnd];
    if (valToMove === null) continue;
    if (valToMove === undefined) continue;
    hasMoved.add(valToMove);
    const indexStart = blocks.indexOf(valToMove);
    if (indexStart < 0) continue;
    const size = indexEnd - indexStart + 1;
    const bigEnoughSpot = blocks.findIndex((_v, i) => {
      if (i >= indexStart) return false;
      for (let j = 0; j < size; j++) {
        if (blocks[i + j] !== null) return false;
      }
      return true;
    });
    if (bigEnoughSpot < 0) continue;
    for (let i = 0; i < size; i++) {
      blocks[indexStart + i] = null;
      blocks[bigEnoughSpot + i] = valToMove;
    }
    showBlocks('BLOCKS DURING');
  }
  showBlocks('BLOCKS AFTER ');

  blocks.forEach((b, i) => (answer += (b ?? 0) * i));

  return answer;
};

export const correctOutput = 6347435485773;
