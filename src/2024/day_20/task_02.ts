import { PriorityQueue } from 'data-structure-typed';
import { equal, Position } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

type Item = {
  pos: Position;
  cost: number;
  path: Position[];
};

export const solve: Solution = async (lines, { log, logging }) => {
  let answer = 0;
  const blocks = new PositionSet<Position>();
  let start: Position = [0, 0];
  let end: Position = [0, 0];
  const H = lines.length;
  const W = lines[0].length;

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    for (let c = 0; c < line.length; c++) {
      const char = line[c];
      const pos = [l, c] as Position;
      if (char === '#') blocks.add(pos);
      if (char === 'S') start = pos;
      if (char === 'E') end = pos;
    }
  }

  log(start, '->', end);

  let mainPath: Position[] = [];

  let normalScore = 0;

  const Q = new PriorityQueue<Item>([{ pos: start, cost: 0, path: [] }], {
    comparator: (a, b) => a.cost - b.cost,
  });

  const seen = new PositionSet<Position>();

  while (!Q.isEmpty()) {
    const item = Q.poll();
    if (item === undefined) throw new Error('No item');
    const { pos, cost } = item;
    if (equal(pos, end)) {
      log('Normal run costs', cost);
      mainPath = [...item.path, end];
      break;
    }
    if (seen.has(pos)) continue;
    seen.add(pos);

    const path = [...item.path, pos];

    if (!blocks.has([pos[0] - 1, pos[1]]))
      Q.add({ ...item, pos: [pos[0] - 1, pos[1]], cost: cost + 1, path });
    if (!blocks.has([pos[0] + 1, pos[1]]))
      Q.add({ ...item, pos: [pos[0] + 1, pos[1]], cost: cost + 1, path });
    if (!blocks.has([pos[0], pos[1] - 1]))
      Q.add({ ...item, pos: [pos[0], pos[1] - 1], cost: cost + 1, path });
    if (!blocks.has([pos[0], pos[1] + 1]))
      Q.add({ ...item, pos: [pos[0], pos[1] + 1], cost: cost + 1, path });
  }

  normalScore = mainPath.length;
  log(normalScore);
  if (logging) {
    for (let l = 0; l < lines.length; l++) {
      const line = lines[l];
      let str = '';
      for (let c = 0; c < line.length; c++) {
        const pos = [l, c] as Position;
        if (mainPath.some((p) => equal(p, pos))) str += '.';
        else if (blocks.has(pos)) str += '#';
        else str += ' ';
      }
      log(str);
    }
  }

  const runMap = new Map<string, number>(
    mainPath.map((v, i) => [v.join(','), i]),
  );

  let saved: number[] = [];

  for (let firstR = 0; firstR < H; firstR++) {
    for (let firstC = 0; firstC < W; firstC++) {
      const first: Position = [firstR, firstC];
      if (blocks.has(first)) continue;
      const firstPos = runMap.get(first.join(','));
      if (firstPos === undefined) continue;

      for (let secondR = 0; secondR < H; secondR++) {
        const yDist = Math.abs(firstR - secondR);
        if (yDist > 20) continue;

        for (let secondC = 0; secondC < W; secondC++) {
          const xDist = Math.abs(secondC - firstC);
          if (xDist > 20) continue;
          const distance = xDist + yDist;
          if (distance > 20) continue;
          if (distance <= 1) continue;

          const second: Position = [secondR, secondC];
          if (blocks.has(second)) continue;

          const secondPos = runMap.get(second.join(','));
          if (secondPos === undefined) continue;
          const scoreSaved = secondPos - firstPos - distance;
          if (scoreSaved <= 0) continue;
          if (scoreSaved >= 100) answer++;
          if (logging) saved.push(scoreSaved);
        }
      }
    }
  }

  if (logging) {
    log(
      new Map(
        [...new Set(saved)]
          .sort((a, b) => a - b)
          .map((v) => [v, saved.filter((s) => s === v).length]),
      ),
    );
  }

  return answer;
};

export const correctOutput = 979014;
