import { splitTN } from '../../lib/numbers';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  const rules: [number, number][] = [];
  const updates: number[][] = [];

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    if (line.includes('|')) {
      rules.push(splitTN(line) as [number, number]);
    } else if (line.includes(',')) {
      updates.push(splitTN(line));
    }
  }

  outer: for (let update of updates) {
    log('Checking', update);
    for (let rule of rules) {
      if (update.includes(rule[0]) && update.includes(rule[1])) {
        if (update.indexOf(rule[0]) > update.indexOf(rule[1])) {
          log('Found wrong rule', rule);
          continue outer;
        }
      }
    }
    const middle = update[Math.floor(update.length / 2)];
    log('Correct! adding', middle);
    answer += middle;
  }

  return answer;
};

export const correctOutput = 5762;
