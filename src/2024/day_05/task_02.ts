import { splitTN } from '../../lib/numbers';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  const rules: [number, number][] = [];
  const updates: number[][] = [];

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    if (line.includes('|')) {
      rules.push(splitTN(line) as [number, number]);
    } else if (line.includes(',')) {
      updates.push(splitTN(line));
    }
  }

  for (let update of updates) {
    let hasErrors = false;
    log('Checking', update);
    for (let r = 0; r < rules.length; r++) {
      const rule = rules[r];
      if (update.includes(rule[0]) && update.includes(rule[1])) {
        if (update.indexOf(rule[0]) > update.indexOf(rule[1])) {
          hasErrors = true;
          log('Fixing wrong rule', rule);
          const a = update.indexOf(rule[0]);
          const b = update.indexOf(rule[1]);
          [update[a], update[b]] = [update[b], update[a]];
          r = 0;
        }
      }
    }
    if (!hasErrors) {
      log('No errors, boring...');
      continue;
    }
    const middle = update[Math.floor(update.length / 2)];
    log(`Adding ${middle}, correct order was`, update);
    answer += middle;
  }

  return answer;
};

export const correctOutput = 4130;
