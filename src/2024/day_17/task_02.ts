import { tn } from '../../lib/functions';
import { splitTN } from '../../lib/numbers';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log, logging }) => {
  let registers: { A: number; B: number; C: number } = { A: 0, B: 0, C: 0 };
  const program: number[] = [];

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    if (line.includes('Register')) {
      const [_, name, value] = line.split(/:? /);
      registers[name as 'A' | 'B' | 'C'] = tn(value);
    }
    if (line.includes('Program')) {
      const prog = splitTN(line.replace('Program: ', ''));
      prog.forEach((p) => program.push(p));
    }
  }

  log(registers, program);

  const check = (checkVal: number) => {
    const regs = { A: checkVal, B: registers.B, C: registers.C };
    let output: number[] = [];

    const comboOp = (n: number) => {
      return [0, 1, 2, 3, regs.A, regs.B, regs.C][n];
    };

    for (let ptr = 0; ptr < program.length; ptr += 2) {
      const inst = program[ptr];
      const arg = program[ptr + 1];
      [
        // ADV
        () => (regs.A = Math.floor(regs.A / Math.pow(2, comboOp(arg)))),
        // BXL
        () => (regs.B = Number(BigInt(regs.B) ^ BigInt(arg))),
        // BST
        () => (regs.B = comboOp(arg) % 8),
        // JNZ
        () => (ptr = regs.A === 0 ? ptr : arg - 2),
        // BXC
        () => (regs.B = Number(BigInt(regs.B) ^ BigInt(regs.C))),
        // OUT
        () => output.push(comboOp(arg) % 8),
        // BDV
        () => (regs.B = Math.floor(regs.A / Math.pow(2, comboOp(arg)))),
        // CDV
        () => (regs.C = Math.floor(regs.A / Math.pow(2, comboOp(arg)))),
      ][inst]();
    }
    if (logging) {
      let l2 = '';
      let printList = output.map((v) => `${v}`);
      while (printList.length < program.length) printList = ['?', ...printList];
      for (let i = 0; i < printList.length; i++) {
        if (printList[i] === '?') l2 += '  ';
        else if (printList[i] === `${program[i]}`) l2 += '  ';
        else l2 += '! ';
      }
      log(program.join(','));
      log(printList.join(','));
      log(l2);
    }
    return output;
  };

  const getNext = (nextVal = 0, i = program.length - 1): number | null => {
    if (i < 0) return nextVal;
    for (let aVal = nextVal * 8; aVal < nextVal * 8 + 8; aVal++) {
      log(aVal);
      const result = check(aVal);
      if (result[0] === program[i]) {
        const finalVal = getNext(aVal, i - 1);
        if (finalVal !== null) return finalVal;
      }
    }
    return null;
  };

  const answer = getNext();
  if (!answer) return null;

  return answer;
};

export const correctOutput = 236580836040301;
