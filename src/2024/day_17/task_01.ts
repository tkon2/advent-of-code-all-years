import { tn } from '../../lib/functions';
import { splitTN } from '../../lib/numbers';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log, clip }) => {
  let regs: { A: number; B: number; C: number } = { A: 0, B: 0, C: 0 };
  let program: number[] = [];
  let output: number[] = [];

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    if (line.includes('Register')) {
      const [_, name, value] = line.split(/:? /);
      regs[name as 'A' | 'B' | 'C'] = tn(value);
    }
    if (line.includes('Program')) {
      const prog = splitTN(line.replace('Program: ', ''));
      program = prog;
    }
  }

  log(regs, program);

  const comboOp = (n: number) => {
    return [0, 1, 2, 3, regs.A, regs.B, regs.C][n];
  };

  for (let ptr = 0; ptr < program.length; ptr += 2) {
    const inst = program[ptr];
    const arg = program[ptr + 1];
    [
      // ADV
      () => (regs.A = Math.floor(regs.A / Math.pow(2, comboOp(arg)))),
      // BXL
      () => (regs.B = Number(BigInt(regs.B) ^ BigInt(arg))),
      // BST
      () => (regs.B = comboOp(arg) % 8),
      // JNZ
      () => (ptr = regs.A === 0 ? ptr : arg - 2),
      // BXC
      () => (regs.B = Number(BigInt(regs.B) ^ BigInt(regs.C))),
      // OUT
      () => output.push(comboOp(arg) % 8),
      // BDV
      () => (regs.B = Math.floor(regs.A / Math.pow(2, comboOp(arg)))),
      // CDV
      () => (regs.C = Math.floor(regs.A / Math.pow(2, comboOp(arg)))),
    ][inst]();
  }

  return output.join(',');
};

export const correctOutput = '2,0,1,3,4,0,2,1,7';
