import { splitTN } from '../../lib/numbers';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  const line = lines.join('');
  const instructions = [...line.matchAll(/mul\(\d+,\d+\)/g)].map((v) => v[0]);
  instructions.forEach((i) => {
    log(i);
    const [a, b] = splitTN(i);
    log(a, '*', b, '=', a * b);
    answer += a * b;
  });

  return answer;
};

export const correctOutput = 173785482;
