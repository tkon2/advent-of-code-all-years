import { splitTN } from '../../lib/numbers';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  const line = lines.join('');
  const instructions = [
    ...line.matchAll(/(mul\(\d+,\d+\)|do\(\)|don't\(\))/g),
  ].map((v) => v[0]);
  let shouldDo = true;
  instructions.forEach((i) => {
    log(i);
    if (i.includes('don')) {
      log('DISABLING');
      shouldDo = false;
      return;
    }
    if (i.includes('do')) {
      log('ENABLING');
      shouldDo = true;
      return;
    }

    log(shouldDo ? 'ENABLED' : 'DISABLED');
    if (!shouldDo) return;

    const [a, b] = splitTN(i);

    log(a, '*', b, '=', a * b);
    answer += a * b;
  });

  return answer;
};

export const correctOutput = 83158140;
