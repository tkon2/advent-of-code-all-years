import { Position } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  const antennas: { l: number; c: number; type: string }[] = [];
  const points = new PositionSet<Position>();
  let H = lines.length;
  let W = lines[0].length;

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    for (let c = 0; c < line.length; c++) {
      const type = line[c];
      if (type !== '.') {
        antennas.push({ l, c, type });
      }
    }
  }

  for (let ai = 0; ai < antennas.length; ai++) {
    for (let bi = 0; bi < antennas.length; bi++) {
      if (ai === bi) continue;
      const a = antennas[ai];
      const b = antennas[bi];
      if (a.type !== b.type) continue;
      const dl = b.l - a.l;
      const dc = b.c - a.c;
      const nl = a.l - dl;
      const nc = a.c - dc;
      if (nl < 0 || nl >= H) continue;
      if (nc < 0 || nc >= W) continue;
      points.add([nl, nc]);
    }
  }

  log(antennas);
  log(points.toArray());

  return points.size;
};

export const correctOutput = 301;
