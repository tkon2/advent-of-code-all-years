import { Deque, PriorityQueue } from 'data-structure-typed';
import { addDelta, diff, equal, Position } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

type Item = {
  score: number;
  pos: Position;
  /** 0, 1, 2, 3 */
  facing: number;
};

const getDelta = (f: number): Position => {
  switch (f) {
    case 0:
      return [0, +1];
    case 1:
      return [+1, 0];
    case 2:
      return [0, -1];
    case 3:
      return [-1, 0];
    default:
      throw new Error(`Not a valid dir: ${f}`);
  }
};

export const solve: Solution = async (lines, { log, clip }) => {
  let answer = 0;
  let start: Position = [0, 0];
  let end: Position = [0, 0];
  let walls = new PositionSet<Position>();

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    for (let c = 0; c < line.length; c++) {
      const char = line[c];
      const pos: Position = [l, c];
      if (char === '#') walls.add(pos);
      if (char === 'S') start = pos;
      if (char === 'E') end = pos;
    }
  }

  let seen = new PositionSet<Position>();

  const queue = new PriorityQueue<Item>([], {
    comparator: (a, b) => a.score - b.score,
  });
  queue.add({ score: 0, pos: start, facing: 0 });
  let lowestDiff = Infinity;
  while (!queue.isEmpty()) {
    const item = queue.poll();
    if (!item) throw new Error('No items');
    if (diff(item.pos, end).reduce((p, v) => p + Math.abs(v), 0) < lowestDiff) {
      log(diff(item.pos, end).map((v) => Math.abs(v)));
      log(queue.size);
      lowestDiff = diff(item.pos, end).reduce((p, v) => p + Math.abs(v), 0);
    }
    if (equal(item.pos, end)) {
      answer = item.score;
      break;
    }
    seen.add(item.pos);

    [0, 1, 3].forEach((faceDiff) => {
      const newDir = (item.facing + faceDiff) % 4;
      const newPos = addDelta(item.pos, getDelta(newDir));
      const newScore = item.score + (faceDiff === 0 ? 1 : 1001);
      if (walls.has(newPos)) return;
      if (seen.has(newPos)) return;
      seen.add(newPos);
      queue.add({
        score: newScore,
        pos: newPos,
        facing: newDir,
      });
    });
  }

  return answer;
};

export const correctOutput = 98520;
