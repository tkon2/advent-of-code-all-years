import { PriorityQueue } from 'data-structure-typed';
import { addDelta, equal, Position } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

type Item = {
  cost: number;
  pos: Position;
  /** 0, 1, 2, 3 */
  dir: number;
};

const getDelta = (f: number): Position => {
  switch (f) {
    case 0:
      return [0, +1];
    case 1:
      return [+1, 0];
    case 2:
      return [0, -1];
    case 3:
      return [-1, 0];
    default:
      throw new Error(`Not a valid dir: ${f}`);
  }
};

export const solve: Solution = async (lines, { log, logging }) => {
  let start: Position = [0, 0];
  let end: Position = [0, 0];
  let walls = new PositionSet<Position>();
  const W = lines[0].length;
  const H = lines.length;

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    for (let c = 0; c < line.length; c++) {
      const char = line[c];
      const pos: Position = [l, c];
      if (char === '#') walls.add(pos);
      if (char === 'S') start = pos;
      if (char === 'E') end = pos;
    }
  }

  const getCostsFrom = (p: Position, f: number = 0) => {
    let minimums = new Map<string, Item>();
    const queue = new PriorityQueue<Item>([{ cost: 0, pos: p, dir: f }], {
      comparator: (a, b) => a.cost - b.cost,
    });
    while (!queue.isEmpty()) {
      const item = queue.poll();
      if (!item) throw new Error('No items');
      if (minimums.has(item.pos.join(','))) continue;
      minimums.set(item.pos.join(','), item);

      if (equal(item.pos, end)) break;

      [0, 1, 3].forEach((dirDiff) => {
        const newDir = (item.dir + dirDiff) % 4;
        const newPos = addDelta(item.pos, getDelta(newDir));
        const newCost = item.cost + (dirDiff === 0 ? 1 : 1001);
        if (walls.has(newPos)) return;
        if (minimums.has(newPos.join(','))) return;
        queue.add({
          cost: newCost,
          pos: newPos,
          dir: newDir,
        });
      });
    }
    return minimums;
  };

  const startCosts = getCostsFrom(start);
  const bestCost = startCosts.get(end.join(','))?.cost ?? 0;

  log({ start, end, W, H, bestCost });

  const onBestPath = new PositionSet<Position>();
  for (let [, { pos, dir, cost }] of [...startCosts]) {
    const costs = getCostsFrom(pos, dir);
    const endCost = costs.get(end.join(','))?.cost;
    if (endCost === undefined) continue;
    if (endCost + cost > bestCost) continue;
    onBestPath.add(pos);
  }

  if (logging) {
    for (let l = 0; l < H; l++) {
      let line = '';
      for (let c = 0; c < W; c++) {
        const pos: Position = [l, c];
        if (walls.has(pos)) line += '#';
        else if (equal(pos, start)) line += 'S';
        else if (equal(pos, end)) line += 'E';
        else if (onBestPath.has(pos)) line += '.';
        else line += ' ';
      }
      log(line);
    }
  }

  return onBestPath.size;
};

export const correctOutput = 609;
