import { asc } from '../../lib/sorting';
import { Solution } from '../../types/solution';
import { UndirectedGraph } from 'data-structure-typed';

export const solve: Solution = async (lines, { log }) => {
  let answer = 0;
  const G = new UndirectedGraph();
  const items = new Set<string>();
  const sets: [string, string, string][] = [];
  const seen = new Set<string>();

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];
    const [left, right] = line.split('-');
    if (!G.hasVertex(left)) G.addVertex(left);
    if (!G.hasVertex(right)) G.addVertex(right);
    items.add(left);
    items.add(right);
    G.addEdge(left, right);
  }

  for (const item of items) {
    const n = G.getNeighbors(item);
    for (let i = 0; i < n.length; i++) {
      const iN = G.getNeighbors(n[i]);
      for (let j = i; j < n.length; j++) {
        const key = [item, n[i].key, n[j].key].sort(asc).join(',');
        if (seen.has(key)) continue;
        seen.add(key);
        if (iN.includes(n[j])) {
          sets.push([item, `${n[i].key}`, `${n[j].key}`]);
        }
      }
    }
  }

  log(sets.map((s) => s.join(',')));
  log(sets.length);

  for (let i = 0; i < sets.length; i++) {
    if (sets[i].some((v) => v.startsWith('t'))) answer++;
  }

  return answer;
};

export const correctOutput = 1062;
