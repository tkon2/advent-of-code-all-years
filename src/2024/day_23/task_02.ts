import { asc } from '../../lib/sorting';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  const items = new Set<string>();
  const cons = new Map<string, Set<string>>();

  for (let l = 0; l < lines.length; l++) {
    const [left, right] = lines[l].split('-');
    items.add(left);
    items.add(right);
    cons.set(left, (cons.get(left) ?? new Set()).add(right));
    cons.set(right, (cons.get(right) ?? new Set()).add(left));
  }

  const getN = (v: string) => {
    const N = cons.get(v);
    if (!N) throw new Error('No mappings found');
    return N;
  };

  const groups = new Set<string>();
  for (const item of items) {
    let mapsTo = getN(item);
    for (const initial of mapsTo) {
      const group = new Set<string>([item, initial]);
      for (let check of mapsTo) {
        const n = getN(check);
        if ([...group].every((g) => n.has(g))) group.add(check);
      }
      groups.add([...group].sort(asc).join(','));
    }
  }

  const sorted = [...groups].toSorted((a, b) => b.length - a.length);
  log(sorted);

  return sorted[0];
};

export const correctOutput = "bz,cs,fx,ms,oz,po,sy,uh,uv,vw,xu,zj,zm";
