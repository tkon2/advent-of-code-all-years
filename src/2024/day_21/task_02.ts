import { Position } from '../../lib/position';
import { Solution } from '../../types/solution';
import { tn } from '../../lib/functions';

const mainKeypad: string[][] = [
  ['7', '8', '9'],
  ['4', '5', '6'],
  ['1', '2', '3'],
  [' ', '0', 'A'],
] as const;

const betweenKeypad: string[][] = [
  [' ', '^', 'A'],
  ['<', 'v', '>'],
] as const;

const getPos = (char: string, pad: string[][]) => {
  const fromR = pad.findIndex((v) => v.includes(char));
  if (fromR < 0) return null;
  const fromC = pad[fromR].indexOf(char);
  if (fromC < 0) return null;
  return [fromR, fromC] as Position;
};

const posValid = (pos: Position, pad: string[][]) => {
  return (pad[pos[0]]?.[pos[1]] ?? ' ') !== ' ';
};

export const solve: Solution = async (codes, { log }) => {
  let answer = 0;

  const goDeeper = (source: string, target: string, pad: string[][]) => {
    const sourcePos = getPos(source, pad);
    const targetPos = getPos(target, pad);
    if (!sourcePos) throw new Error('No source pos');
    if (!targetPos) throw new Error('No target pos');
    const [sr, sc] = sourcePos;
    const [tr, tc] = targetPos;
    const dr = tr - sr;
    const dc = tc - sc;
    const upDown = (dr > 0 ? 'v' : '^').repeat(Math.abs(dr));
    const leftRight = (dc > 0 ? '>' : '<').repeat(Math.abs(dc));

    // [tr, sc] is after upDown
    if (dc > 0 && posValid([tr, sc], pad)) {
      return upDown + leftRight + 'A';
      // [sr, tc] is after leftRight
    } else if (posValid([sr, tc], pad)) {
      return leftRight + upDown + 'A';
      // [tr, sc] is after upDown
    } else if (posValid([tr, sc], pad)) {
      return upDown + leftRight + 'A';
    } else {
      throw new Error('No valid path');
    }
  };

  const complexify = (code: string, pad: string[][]) => {
    let total = '';
    for (let i = 0; i < code.length; i++) {
      const start = i === 0 ? 'A' : code[i - 1];
      const end = code[i];
      total += goDeeper(start, end, pad);
    }
    return total;
  };

  const memo = new Map<string, number>();
  const findCost = (seq: string, remainingSteps: number) => {
    if (remainingSteps === 0) return seq.length;
    if (memo.has(`${remainingSteps}::${seq}`)) {
      return memo.get(`${remainingSteps}::${seq}`)!;
    }
    let total = 0;
    for (let i = 0; i < seq.length; i++) {
      const prev = i === 0 ? 'A' : seq[i - 1];
      const newSeq = goDeeper(prev, seq[i], betweenKeypad);
      total += findCost(newSeq, remainingSteps - 1);
    }
    memo.set(`${remainingSteps}::${seq}`, total);
    return total;
  };

  for (const code of codes) {
    const main = complexify(code, mainKeypad);
    const finalTotal = findCost(main, 25);
    const complexity = finalTotal * tn(code);
    log(code, complexity);
    answer += complexity;
  }

  return answer;
};

export const correctOutput = 218309335714068;
