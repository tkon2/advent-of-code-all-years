import { readdir } from 'node:fs/promises';

/** Check if a directory exists */
export const exists = async (dirPath: string) => {
  try {
    await readdir(dirPath);
    return true;
  } catch {
    return false;
  }
};
