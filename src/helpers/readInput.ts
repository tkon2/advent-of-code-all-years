import { paste } from 'copy-paste';
import { makePath } from './makePath';

/** This function reads a given file and returns an array of strings */
export const readStrings = async (year: number, day: number) => {
  const filename = `${makePath(year, day)}/data.txt`;
  const file = Bun.file(filename);

  const fileExists = await file.exists();
  if (fileExists) return (await file.text()).trimEnd().split(/\r?\n/);

  const res = await fetch(`https://adventofcode.com/${year}/day/${day}/input`, {
    headers: {
      cookie: `session=${Bun.env.TOKEN}`,
    },
  });

  if (res.status !== 200) throw new Error('Could not get data');

  const content = await res.text();
  Bun.write(filename, content);
  return content.trimEnd().split(/\r?\n/);
};

/** Read contents of clipboard */
export const readClipboard = async () => {
  return new Promise<string[]>((resolve, reject) => {
    paste((err, value) => {
      if (err) reject(err);
      else resolve(value.trimEnd().split(/\r?\n/));
    });
  });
};
