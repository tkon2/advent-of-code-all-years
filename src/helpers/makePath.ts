import { leftPad } from '../lib/stringFuncs';

/** Composes the path to a given year and day */
export const makePath = (year: number, day: number, task?: number) => {
  const srcPath = import.meta.dir.replace(/\/helpers$/, '');
  const dayPath = `${srcPath}/${year}/day_${leftPad(day, 2)}`;
  if (task) return `${dayPath}/task_0${task}.ts`;
  return dayPath;
};
