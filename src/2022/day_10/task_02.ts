import { chunk } from '../../lib/arrays';
import { fromBig } from '../../lib/bigChars';
import { log, toNumStrict } from '../../lib/functions';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let value = 1;
  let instructions = 0;
  let crt: string[] = [];

  const drawPixel = () => {
    if (Math.abs(((instructions - 1) % 40) - value) <= 1) {
      crt[instructions - 1] = '#';
    } else {
      crt[instructions - 1] = ' ';
    }
  };

  lines.forEach((l, i) => {
    if (l === 'noop') {
      instructions += 1;
      drawPixel();
    } else {
      instructions += 1;
      drawPixel();
      instructions += 1;
      drawPixel();
      value += toNumStrict(l);
    }
  });
  const ans = chunk(crt, 40);

  let finalString = '';
  let notFound = false;
  for (let i = 0; i < 40; i += 5) {
    const char = fromBig(ans.map((a) => a.slice(i, i + 4).join('')));
    if (!char) notFound = true;
    else finalString += char;
  }

  if (notFound) {
    log(ans.map((l) => l.join('')).join('\n'));
    return 'Look above';
  }

  return finalString;
};

export const correctOutput = 'EGJBGCFK';
