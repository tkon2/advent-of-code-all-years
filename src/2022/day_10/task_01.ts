import { toNumStrict } from '../../lib/functions';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let value = 1;
  let score = 0;
  let instructions = 0;

  const checkScore = () => {
    if (instructions === 20) {
      score += instructions * value;
    }
    if (instructions > 20 && (instructions - 20) % 40 === 0) {
      score += instructions * value;
    }
  };

  lines.forEach((l, i) => {
    if (l === 'noop') {
      instructions += 1;
      checkScore();
    } else {
      instructions += 1;
      checkScore();
      instructions += 1;
      checkScore();
      value += toNumStrict(l);
    }
  });
  return score;
};

export const correctOutput = 13480;
