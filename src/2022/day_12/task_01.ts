import { bfs } from '../../lib/pathfinding';
import { Solution } from '../../types/solution';

type Position = [number, number];

export const solve: Solution = async (lines) => {
  let board: number[][] = [];

  const char0 = 'a'.charCodeAt(0);

  let goal: Position = [0, 0];
  let start: Position = [0, 0];

  for (let i = 0; i < lines.length; i += 1) {
    board[i] = lines[i].split('').map((c, ci) => {
      if (c === 'S') {
        start = [i, ci];
        return 0;
      }
      if (c === 'E') {
        goal = [i, ci];
        return 'z'.charCodeAt(0) - char0;
      }
      return c.charCodeAt(0) - char0;
    });
  }

  const path = bfs(board, [start], goal);

  // 456
  return path?.cost ?? 1 - 1;
};

export const correctOutput = 456;
