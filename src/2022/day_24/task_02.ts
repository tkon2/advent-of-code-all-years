import { crossDirs, Position } from '../../lib/position';
import { Solution } from '../../types/solution';

type Bliz = {
  dir: Position;
  pos: Position;
};

export const solve: Solution = async (baseLines) => {
  const lines = baseLines.filter((l) => l !== '');

  const blizs: Bliz[] = [];
  let entry: Position = [0, 0];
  let exit: Position = [0, 0];

  const WIDTH = lines[0].length - 1;
  const HEIGHT = lines.length - 1;

  for (let y = 0; y < lines.length; y += 1) {
    const line = lines[y];
    line.split('').forEach((v, x) => {
      if (y === 0 && v === '.') entry = [x, y];
      if (y === lines.length - 1 && v === '.') exit = [x, y];
      if (v === '.' || v === '#') return;
      if (v === '>') {
        blizs.push({ dir: [1, 0], pos: [x, y] });
        return;
      }
      if (v === 'v') {
        blizs.push({ dir: [0, 1], pos: [x, y] });
        return;
      }
      if (v === '<') {
        blizs.push({ dir: [-1, 0], pos: [x, y] });
        return;
      }
      if (v === '^') {
        blizs.push({ dir: [0, -1], pos: [x, y] });
        return;
      }
    });
  }

  const blizPosMemo = new Map<number, Set<string>>([
    [0, new Set(blizs.map((b) => b.pos.toString()))],
  ]);
  const getBlizPos = (round: number) => {
    if (blizPosMemo.has(round)) return blizPosMemo.get(round);
    const localBliz: Bliz[] = blizs.map((b) => ({
      pos: [...b.pos],
      dir: [...b.dir],
    }));
    for (let i = 1; i <= round; i++) {
      localBliz.map((b) => {
        b.pos[0] += b.dir[0];
        b.pos[1] += b.dir[1];
        if (b.pos[0] > WIDTH - 1) b.pos[0] = 1;
        if (b.pos[1] > HEIGHT - 1) b.pos[1] = 1;
        if (b.pos[0] < 1) b.pos[0] = WIDTH - 1;
        if (b.pos[1] < 1) b.pos[1] = HEIGHT - 1;
        return b;
      });
      blizPosMemo.set(i, new Set(localBliz.map((b) => b.pos.toString())));
    }
    return blizPosMemo.get(round);
  };

  // Calculate the first 1000 blizard positions to save time
  getBlizPos(1000);

  type Entry = {
    pos: Position;
    round: number;
    history: Position[];
  };

  const entryToString = (e: Entry) => {
    return e.pos.toString() + ',' + e.round;
  };

  const navigate = (to: Position, startEntry?: Entry) => {
    const def = startEntry || { pos: entry, round: 0, history: [] };
    let open = new Map<number, Entry[]>([[0, [def]]]);
    let visited = new Set<string>(); // Format: x,y,cost

    while (open.size) {
      let lowest = Infinity;
      open.forEach((v, i, m) => {
        if (v.length === 0) m.delete(i);
        else lowest = Math.min(lowest, i);
      });
      const curr = open.get(lowest)?.pop();
      if (!curr) throw new Error('Open out of range');

      const round = curr.round + 1;
      const history = [...curr.history, curr.pos];
      const entryStr = entryToString(curr);
      if (visited.has(entryStr)) continue;
      visited.add(entryStr);
      if (curr.pos[0] === to[0] && curr.pos[1] === to[1]) {
        return curr;
      }
      const blizPos = getBlizPos(round);
      if (blizPos === undefined)
        throw new Error('Could not find bliz pos for ' + round);
      [...crossDirs, [0, 0]].forEach((d) => {
        const x = curr.pos[0] + d[0];
        const y = curr.pos[1] + d[1];
        // Make an exception for the goal because we're nice like that
        if (x === to[0] && y === to[1]) {
          const arr = open.get(round) || [];
          arr.push({ pos: [x, y], round, history });
          open.set(round, arr);
          return;
        }
        // Make sure coords aren't OOB unless just starting out
        if ((y !== 0 && y !== HEIGHT) || d[0] !== 0 || d[1] !== 0) {
          if (x < 1 || y < 1) return;
          if (x > WIDTH - 1 || y > HEIGHT - 1) return;
          if (blizPos.has([x, y].toString())) return;
        }
        const arr = open.get(round) || [];
        arr.push({ pos: [x, y], round, history });
        open.set(round, arr);
      });
    }
    return undefined;
  };

  const there = navigate(exit);
  const back = navigate(entry, there);
  const winner = navigate(exit, back);

  if (!there || !back || !winner) {
    throw new Error('At least one step failed');
  }

  return winner.round;
};

export const correctOutput = 861;
