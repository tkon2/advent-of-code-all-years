import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let score = 0;

  let pos = [0, 0];
  let visited: number[][] = [[0, 0]];
  let tail = [0, 0];

  const handleTail = () => {
    const diff = [pos[0] - tail[0], pos[1] - tail[1]];
    if (Math.abs(diff[0]) > 1 || Math.abs(diff[1]) > 1) {
      // Must move tail, but where
      tail[0] += Math.sign(diff[0]);
      tail[1] += Math.sign(diff[1]);
      visited.push([...tail]);
    }
  };

  const addVisited = (x: number, y: number) => {
    if (x < 0 || y < 0) {
      for (let i = 0; i > Math.min(x, y); i--) {
        // if (x) {
        //   visited.push([pos[0]+i,pos[1]+y]);
        // } else {
        //   visited.push([pos[0]+x,pos[1]+i]);
        // }
        pos[x < 0 ? 0 : 1] -= 1;
        handleTail();
      }
    } else {
      for (let i = 0; i < Math.max(x, y); i++) {
        // if (x) {
        //   visited.push([pos[0]+i,pos[1]+y]);
        // } else {
        //   visited.push([pos[0]+x,pos[1]+i]);
        // }
        pos[x > 0 ? 0 : 1] += 1;
        handleTail();
      }
    }
  };

  lines.forEach((l, i) => {
    const num = parseInt(l.replace(/\w /, ''));
    if (l.includes('U')) addVisited(0, num);
    if (l.includes('R')) addVisited(num, 0);
    if (l.includes('D')) addVisited(0, num * -1);
    if (l.includes('L')) addVisited(num * -1, 0);
    handleTail();
  });

  const unique = [...new Set(visited.map((v) => '' + v))];
  // log(unique);
  // const expected = [[0,0], [1,0], [2,0], [3,0], [4,1], [1,2], [2,2], [3,2], [4,2], [3,3], [4,3], [2,4], [3,4]];
  // log(expected);
  // log("Missing");
  // expected.forEach(e => visited.some(u => u[0] === e[0] && u[1] === e[1]) && log(e));
  // log("Extra");
  // visited.forEach(e => expected.some(u => u[0] === e[0] && u[1] === e[1]) && log(e));

  return unique.length;
};

// expected [0,0], [1,0], [2,0], [3,0], [4,1], [1,2], [2,2], [3,2], [4,2], [3,3], [4,3], [2,4], [3,4]

export const correctOutput = 6470;
