import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let pos = [0, 0];
  let visited: number[][] = [[0, 0]];
  let tails = [
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
  ];

  const handleTails = () => {
    tails.forEach((tail, i) => {
      let ref = pos;
      if (i > 0) ref = tails[i - 1];
      const diff = [ref[0] - tail[0], ref[1] - tail[1]];
      if (Math.abs(diff[0]) > 1 || Math.abs(diff[1]) > 1) {
        // Must move tail, but where
        tail[0] += Math.sign(diff[0]);
        tail[1] += Math.sign(diff[1]);
        if (i === 8) {
          visited.push([...tail]);
        }
      }
      tails[i] = tail;
    });
  };

  const addVisited = (x: number, y: number) => {
    if (x < 0 || y < 0) {
      for (let i = 0; i > Math.min(x, y); i--) {
        pos[x < 0 ? 0 : 1] -= 1;
        handleTails();
      }
    } else {
      for (let i = 0; i < Math.max(x, y); i++) {
        pos[x > 0 ? 0 : 1] += 1;
        handleTails();
      }
    }
  };

  lines.forEach((l, i) => {
    const num = parseInt(l.replace(/\w /, ''));
    if (l.includes('U')) addVisited(0, num);
    if (l.includes('R')) addVisited(num, 0);
    if (l.includes('D')) addVisited(0, num * -1);
    if (l.includes('L')) addVisited(num * -1, 0);
    handleTails();
  });

  const unique = [...new Set(visited.map((v) => '' + v))];

  return unique.length;
};

export const correctOutput = 2658;
