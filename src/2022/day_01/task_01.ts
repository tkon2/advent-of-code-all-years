import { Solution } from '../../types/solution';

export const solve: Solution = async (strs) => {
  let max = 0;
  let curr = 0;

  strs.forEach((s) => {
    if (s === '') {
      max = Math.max(max, curr);
      curr = 0;
      return;
    }
    curr += parseInt(s);
  });

  return max;
};

export const correctOutput = 67622;
