import { sum } from '../../lib/arrays';
import { log } from '../../lib/functions';
import { readClipboard, readStrings } from '../../helpers/readInput';
import { desc } from '../../lib/sorting';

import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  const elves: number[] = [];

  let curr = 0;

  lines.forEach((s) => {
    if (s === '') {
      elves.push(curr);
      curr = 0;
      return;
    }
    curr += parseInt(s);
  });

  const sorted = elves.sort(desc);
  const sliced = sorted.slice(0, 3);
  return sum(sliced);
};

export const correctOutput = 201491;
