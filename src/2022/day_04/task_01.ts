import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let points = 0;

  lines.forEach((l) => {
    const elves = l.split(',').map((e) => e.split('-').map((z) => parseInt(z)));
    if (elves[0][0] >= elves[1][0] && elves[0][1] <= elves[1][1]) points += 1;
    else if (elves[1][0] >= elves[0][0] && elves[1][1] <= elves[0][1])
      points += 1;
  });

  return points;
};

export const correctOutput = 456;
