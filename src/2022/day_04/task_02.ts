import { overlap } from '../../lib/functions';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let points = 0;

  lines.forEach((l) => {
    const elves = l.split(',').map((e) => e.split('-').map((z) => parseInt(z)));
    if (overlap(elves[0], elves[1])) points += 1;
  });

  return points;
};

export const correctOutput = 808;
