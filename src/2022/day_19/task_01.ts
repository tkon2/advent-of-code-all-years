import { toNumStrict } from '../../lib/functions';
import { Solution } from '../../types/solution';

type Items = {
  ore: number;
  clay: number;
  obsidian: number;
  geode: number;
};

type Blueprint = {
  id: number;
  ore: Items;
  clay: Items;
  obsidian: Items;
  geode: Items;
};

export const solve: Solution = async (lines) => {
  let score = 0;
  let blueprints: Blueprint[] = [];

  const getCost = (s: string): Items => {
    const items = s.split('and');
    let output: Items = {
      ore: 0,
      clay: 0,
      obsidian: 0,
      geode: 0,
    };
    items.forEach((i) => {
      i = i.replace(/Each.*robot/, '');
      if (i.includes('ore')) {
        output.ore = toNumStrict(i);
      }
      if (i.includes('clay')) {
        output.clay = toNumStrict(i);
      }
      if (i.includes('obsidian')) {
        output.obsidian = toNumStrict(i);
      }
      if (i.includes('geode')) {
        output.geode = toNumStrict(i);
      }
    });
    return output;
  };

  for (let i = 0; i < lines.length; i += 1) {
    const line = lines[i];
    if (line === '') continue;
    const [id, rest] = line.split(':');
    const [ore, clay, obsidian, geode] = rest.split('.').map(getCost);

    blueprints.push({
      id: toNumStrict(id),
      ore,
      clay,
      obsidian,
      geode,
    });
  }

  const canAfford = (ownedMaterial: Items, cost: Items) => {
    if (cost.ore > ownedMaterial.ore) return false;
    if (cost.clay > ownedMaterial.clay) return false;
    if (cost.obsidian > ownedMaterial.obsidian) return false;
    return true;
  };

  const subCost = (ownedMaterial: Items, cost: Items): Items => {
    return {
      ore: ownedMaterial.ore - cost.ore,
      clay: ownedMaterial.clay - cost.clay,
      obsidian: ownedMaterial.obsidian - cost.obsidian,
      geode: ownedMaterial.geode,
    };
  };

  type State = {
    ownedRobots: Items;
    ownedMaterial: Items;
    remaining: number;
  };

  const stringify = (s: State) => {
    return (
      s.remaining +
      ',(' +
      [
        s.ownedRobots.ore,
        s.ownedRobots.clay,
        s.ownedRobots.obsidian,
        s.ownedRobots.geode,
      ].join(',') +
      '),(' +
      [
        s.ownedMaterial.ore,
        s.ownedMaterial.clay,
        s.ownedMaterial.obsidian,
        s.ownedMaterial.geode,
      ].join(',') +
      ')'
    );
  };

  const getScore = (s: State) => {
    return s.ownedMaterial.geode + s.ownedRobots.geode * s.remaining;
  };

  blueprints.forEach((b) => {
    /** Max spendable of each type */
    const bluePrintMax: Items = {
      ore: Math.max(b.ore.ore, b.clay.ore, b.obsidian.ore, b.geode.ore),
      clay: Math.max(b.ore.clay, b.clay.clay, b.obsidian.clay, b.geode.clay),
      obsidian: Math.max(
        b.ore.obsidian,
        b.clay.obsidian,
        b.obsidian.obsidian,
        b.geode.obsidian,
      ),
      geode: 0,
    };

    let open: State[] = [
      {
        ownedRobots: {
          ore: 1,
          clay: 0,
          obsidian: 0,
          geode: 0,
        },
        ownedMaterial: {
          ore: 0,
          clay: 0,
          obsidian: 0,
          geode: 0,
        },
        remaining: 24,
      },
    ];
    let closed = new Set<string>();
    let currentBest: State = open[0];
    while (open.length > 0) {
      const current = open.pop();
      if (!current) throw new Error('Current not found');

      if (current.remaining <= 0) continue;

      if (current.ownedRobots.ore > bluePrintMax.ore) continue;
      if (current.ownedRobots.clay > bluePrintMax.clay) continue;
      if (current.ownedRobots.obsidian > bluePrintMax.obsidian) continue;

      current.ownedMaterial = {
        ore: Math.min(
          bluePrintMax.ore + current.ownedRobots.ore,
          current.ownedMaterial.ore,
        ),
        clay: Math.min(
          bluePrintMax.clay + current.ownedRobots.clay,
          current.ownedMaterial.clay,
        ),
        obsidian: Math.min(
          bluePrintMax.obsidian + current.ownedRobots.obsidian,
          current.ownedMaterial.obsidian,
        ),
        geode: current.ownedMaterial.geode,
      };

      const json = stringify(current);
      if (closed.has(json)) {
        continue;
      }
      closed.add(json);

      if (getScore(current) > getScore(currentBest)) {
        currentBest = current;
      }

      const base: State = {
        ownedRobots: { ...current.ownedRobots },
        ownedMaterial: {
          ore: current.ownedMaterial.ore + current.ownedRobots.ore,
          clay: current.ownedMaterial.clay + current.ownedRobots.clay,
          obsidian:
            current.ownedMaterial.obsidian + current.ownedRobots.obsidian,
          geode: current.ownedMaterial.geode + current.ownedRobots.geode,
        },
        remaining: current.remaining - 1,
      };
      // Add the case where we do nothing
      open.push(base);

      // Handle buying
      if (canAfford(current.ownedMaterial, b.ore)) {
        open.push({
          ...base,
          ownedMaterial: subCost({ ...base.ownedMaterial }, b.ore),
          ownedRobots: {
            ...base.ownedRobots,
            ore: base.ownedRobots.ore + 1,
          },
        });
      }

      if (canAfford(current.ownedMaterial, b.clay)) {
        open.push({
          ...base,
          ownedMaterial: subCost({ ...base.ownedMaterial }, b.clay),
          ownedRobots: {
            ...base.ownedRobots,
            clay: base.ownedRobots.clay + 1,
          },
        });
      }

      if (canAfford(current.ownedMaterial, b.obsidian)) {
        open.push({
          ...base,
          ownedMaterial: subCost({ ...base.ownedMaterial }, b.obsidian),
          ownedRobots: {
            ...base.ownedRobots,
            obsidian: base.ownedRobots.obsidian + 1,
          },
        });
      }

      if (canAfford(current.ownedMaterial, b.geode)) {
        open.push({
          ...base,
          ownedMaterial: subCost({ ...base.ownedMaterial }, b.geode),
          ownedRobots: {
            ...base.ownedRobots,
            geode: base.ownedRobots.geode + 1,
          },
        });
      }
    }

    score += b.id * getScore(currentBest);
  });

  return score;
};

export const correctOutput = 1487;
