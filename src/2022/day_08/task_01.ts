import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let score = 0;

  const trees: number[][] = [];

  lines.forEach((l, i) => {
    if (l === '') return;
    trees[i] = l.split('').map((t) => parseInt(t));
  });

  const check = (x: number, y: number) => {
    const num = trees[x][y];
    let l = true;
    let t = true;
    let r = true;
    let b = true;
    for (let i = 0; i < trees.length; i++) {
      if (i < y && trees[x][i] >= num) {
        l = false;
      }
      if (i > y && trees[x][i] >= num) {
        r = false;
      }
      if (i < x && trees[i][y] >= num) {
        t = false;
      }
      if (i > x && trees[i][y] >= num) {
        b = false;
      }
      if (!(l || t || b || r)) {
        return false;
      }
    }
    return true;
  };

  for (let i = 0; i < trees.length; i++) {
    for (let j = 0; j < trees.length; j++) {
      if (check(i, j)) {
        score += 1;
      }
    }
  }

  return score;
};

export const correctOutput = 1823;
