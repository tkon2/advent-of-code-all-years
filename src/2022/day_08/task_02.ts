import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let score = 0;

  const trees: number[][] = [];

  lines.forEach((l, i) => {
    if (l === '') return;
    trees[i] = l.split('').map((t) => parseInt(t));
  });

  const getScore = (x: number, y: number): number => {
    const num = trees[x][y];
    let l = true;
    let t = true;
    let r = true;
    let b = true;
    let scores = [0, 0, 0, 0];
    for (let i = Math.min(x, y); i < trees.length; i++) {
      if (r && i > y) scores[1] += 1;
      if (b && i > x) scores[2] += 1;
      if (i > y && trees[x][i] >= num) {
        r = false;
      }
      if (i > x && trees[i][y] >= num) {
        b = false;
      }
    }
    for (let i = Math.max(x, y); i >= 0; i--) {
      if (t && i < x) scores[0] += 1;
      if (l && i < y) scores[3] += 1;
      if (i < x && trees[i][y] >= num) {
        t = false;
      }
      if (i < y && trees[x][i] >= num) {
        l = false;
      }
    }
    const finalScore = scores.reduce((c, v) => c * v, 1);
    return finalScore;
  };

  for (let i = 0; i < trees.length; i++) {
    for (let j = 0; j < trees.length; j++) {
      const t = score;
      score = Math.max(score, getScore(i, j));
      if (t != score) {
      }
    }
  }

  return score;
};

export const correctOutput = 211680;
