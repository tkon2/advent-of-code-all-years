import { hasDuplicate } from '../../lib/functions';

import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let points = 0;

  const arr = lines[0].split('');
  for (let i = 13; i < arr.length; i++) {
    const check = arr.slice(i - 13, i + 1);
    if (!hasDuplicate(check)) {
      points = i + 1;
      break;
    }
  }

  return points;
};

export const correctOutput = 2447;
