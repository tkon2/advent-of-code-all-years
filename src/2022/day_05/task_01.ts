import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let moves = false;
  let stacks: string[][] = [[], [], [], [], [], [], [], [], []];

  let rawStacks: string[] = [];

  lines.forEach((l) => {
    if (l === '') {
      moves = true;
      rawStacks = rawStacks.reverse().filter((r) => r.includes('['));

      rawStacks.forEach((r) => {
        if (!r.includes('[')) return;
        for (let i = 0; i < 9; i++) {
          const n = i * 4 + 1;
          if (r[n] !== ' ' && r[n]) stacks[i].push(r[n]);
        }
      });
    } else if (!moves) {
      rawStacks.push(l);
    } else {
      // instructions
      const count = parseInt(l.replace('move ', ''));
      const [from, to] = l
        .replace(/move \d+ from /, '')
        .split(' to ')
        .map((v) => parseInt(v));
      for (let i = 0; i < count; i++) {
        const v = stacks[from - 1].pop();
        if (!v) continue;
        stacks[to - 1].push(v);
      }
    }
  });
  let ans = '';
  stacks.forEach((s) => (ans += s.pop()));

  return ans;
};

export const correctOutput = 'RFFFWBPNS';
