import { Solution } from '../../types/solution';

type Arr = number | (Arr | number)[];

const compare = (a: Arr | number, b: Arr): number => {
  const aIsNum = typeof a === 'number';
  const bIsNum = typeof b === 'number';
  if (aIsNum && bIsNum) {
    const res = Math.sign(b - a);
    if (Math.abs(res) > 0) return res;
  }
  if (!aIsNum && bIsNum) {
    return compare(a, [b]);
  }
  if (aIsNum && !bIsNum) {
    return compare([a], b);
  }
  if (!aIsNum && !bIsNum) {
    for (let i = 0; i < Math.min(a.length, b.length); i++) {
      const res = compare(a[i], b[i]);
      if (Math.abs(res) > 0) return res;
    }
    if (a.length < b.length) return 1;
    if (b.length < a.length) return -1;
  }
  return 0;
};

export const solve: Solution = async (lines) => {
  let score = 0;

  for (let i = 0; i < lines.length; i += 3) {
    let lineNum = i / 3;
    let a: Arr = eval(lines[i]);
    let b: Arr = eval(lines[i + 1]);

    const ans = compare(a, b);
    const match = ans > 0 ? true : false;

    score += match ? lineNum + 1 : 0;
  }

  return score;
};

export const correctOutput = 6656;
