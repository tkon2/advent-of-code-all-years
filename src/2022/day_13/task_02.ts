import { Solution } from '../../types/solution';

type Arr = number | (Arr | number)[];

const compare = (a: Arr | number, b: Arr): number => {
  const aIsNum = typeof a === 'number';
  const bIsNum = typeof b === 'number';
  if (aIsNum && bIsNum) {
    const res = Math.sign(b - a);
    if (Math.abs(res) > 0) return res;
  }
  if (!aIsNum && bIsNum) {
    return compare(a, [b]);
  }
  if (aIsNum && !bIsNum) {
    return compare([a], b);
  }
  if (!aIsNum && !bIsNum) {
    for (let i = 0; i < Math.min(a.length, b.length); i++) {
      const res = compare(a[i], b[i]);
      if (Math.abs(res) > 0) return res;
    }
    if (a.length < b.length) return 1;
    if (b.length < a.length) return -1;
  }
  return 0;
};

export const solve: Solution = async (lines) => {
  let score = 1;

  const arrs: Arr[] = lines.filter((l) => l !== '').map((l) => eval(l));
  arrs.push([[2]], [[6]]);

  arrs.sort((a, b) => compare(b, a));

  arrs.forEach((v, i) => {
    if (['2', '6'].includes(v.toString())) {
      score *= i + 1;
    }
  });

  return score;
};

export const correctOutput = 19716;
