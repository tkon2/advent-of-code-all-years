import { toNumStrict } from '../../lib/functions';
import { Solution } from '../../types/solution';

type Valve = {
  name: string;
  rate: number;
  children: string[];
};

export const solve: Solution = async (lines, { clip }) => {
  let valves: Valve[] = [];

  for (let i = 0; i < lines.length; i += 1) {
    const line = lines[i];
    if (line === '') continue;
    const [v, c] = line.split(';');
    const name = v.substring(6, 8);
    const rate = toNumStrict(v);
    const children = c
      .replace(/ [a-z ]* /, '')
      .split(', ')
      .map((v) => v.substring(0, 2));
    valves.push({ name, rate, children });
  }

  type Item = {
    scores: number[];
    remaining: number;
    pos: string;
    path: string[];
    opened: string[];
    scorePerTurn: number;
  };

  const gn = (curr: Item): Item[] => {
    if (curr.remaining <= 0) return [];
    const v = valves.find((i) => i.name === curr.pos);
    if (!v) return [];
    const next: Item[] = [];
    const currentScore =
      curr.scores.length <= 0 ? 0 : curr.scores[curr.scores.length - 1];
    if (v.rate !== 0 && curr.remaining > 1 && !curr.opened.includes(curr.pos)) {
      v.children.forEach((c) => {
        next.push({
          scores: [
            ...curr.scores,
            currentScore + curr.scorePerTurn,
            currentScore + curr.scorePerTurn + curr.scorePerTurn + v.rate,
          ],
          remaining: curr.remaining - 2,
          pos: c,
          path: [...curr.path, v.name, v.name],
          opened: [curr.pos, ...curr.opened],
          scorePerTurn: curr.scorePerTurn + v.rate,
        });
      });
    }
    v.children.forEach((c) => {
      next.push({
        scores: [...curr.scores, currentScore + curr.scorePerTurn],
        remaining: curr.remaining - 1,
        pos: c,
        path: [...curr.path, v.name],
        opened: [...curr.opened],
        scorePerTurn: curr.scorePerTurn,
      });
    });
    return next;
  };

  const getScoreAt = (item: Item, remaining: number) => {
    const index = 30 - remaining;
    if (index < 0) return 0;
    if (item.scores.length <= 0) return 0;
    if (index < item.scores.length) {
      return item.scores[index];
    }
    const lastKnown = item.scores.length;
    return item.scores[lastKnown - 1] + item.scorePerTurn * (index - lastKnown);
  };

  let start = valves.find((v) => v.name === 'AA');
  if (!start) return 0;
  let open: Item[] = [
    {
      pos: start.name,
      remaining: 30,
      scorePerTurn: 0,
      scores: [],
      path: [],
      opened: [],
    },
  ];

  const test = {
    pos: clip ? 'DD' : 'FX',
    remaining: 30,
    scores: [],
    scorePerTurn: 0,
    path: [],
    opened: [],
  };
  let closed: Item[] = [];
  let done: Set<string> = new Set();
  while (open.length) {
    open.sort((a, b) => a.remaining - b.remaining);
    const current = open.pop();
    if (!current) continue;
    closed.push(current);
    done.add(current.path.toString() + ',' + current.pos);

    const neighbours = gn(current);
    neighbours.forEach((n) => {
      if (done.has(n.path.toString() + ',' + n.pos)) {
        return;
      }
      let valSet = false;
      open = open.map((o) => {
        if (o.pos === n.pos && o.remaining === n.remaining) {
          valSet = true;
          return getScoreAt(o, o.remaining) > getScoreAt(n, o.remaining)
            ? o
            : n;
        }
        return o;
      });
      if (!valSet) {
        open.push(n);
      }
    });
  }
  closed.sort((a, b) => getScoreAt(b, 0) - getScoreAt(a, 0));
  const win = closed[0];

  return getScoreAt(win, 0);
};

export const correctOutput = 1896;
