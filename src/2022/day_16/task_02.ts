import { toNumStrict } from '../../lib/functions';
import { Solution } from '../../types/solution';

type Valve = {
  name: string;
  rate: number;
  children: string[];
};

export const solve: Solution = async (lines) => {
  let valves = new Map<string, Valve>();

  for (let i = 0; i < lines.length; i += 1) {
    const line = lines[i];
    if (line === '') continue;
    const [v, c] = line.split(';');
    const name = v.substring(6, 8);
    const rate = toNumStrict(v);
    const children = c
      .replace(/ [a-z ]* /, '')
      .split(', ')
      .map((v) => v.substring(0, 2));
    valves.set(name, { name, rate, children });
  }

  type Item = {
    scores: number[];
    remaining: number;
    santaPos: string;
    elephantPos: string;
    santaPath: string;
    elephantPath: string;
    opened: string[];
    scorePerTurn: number;
  };

  const gn = (curr: Item, l: boolean = false): Item[] => {
    if (curr.remaining <= 0) return [];
    const santa = valves.get(curr.santaPos);
    const elephant = valves.get(curr.elephantPos);
    if (!santa || !elephant) return [];
    const nextItems: Item[] = [];
    const currentScore =
      curr.scores.length <= 0 ? 0 : curr.scores[curr.scores.length - 1];

    [...elephant.children, elephant.name].forEach((ec) => {
      const currentElephant = valves.get(ec);
      if (!currentElephant)
        throw new Error('Could not find valve with name ' + ec);

      [...santa.children, santa.name].forEach((sc) => {
        const currentSanta = valves.get(sc);
        if (!currentSanta)
          throw new Error('Could not find valve with name ' + sc);

        const obj: Item = {
          scores: [...curr.scores, currentScore + curr.scorePerTurn],
          remaining: curr.remaining - 1,
          santaPos: sc,
          santaPath: curr.santaPath + ',' + currentSanta.name,
          elephantPos: ec,
          elephantPath: curr.elephantPath + ',' + currentElephant.name,
          opened: [...curr.opened],
          scorePerTurn: curr.scorePerTurn,
        };

        // handle santa
        if (santa.name === currentSanta.name) {
          if (currentSanta.rate <= 0) return;
          if (obj.opened.includes(curr.santaPos)) return;
          if (curr.santaPos !== sc) return;
          // We're looking at the current one, check if we want to open
          // CurrentSanta is the position santa is currently at in this scenario
          obj.scorePerTurn += currentSanta.rate;
          obj.opened.push(currentSanta.name);
          obj.scores[obj.scores.length - 1] += currentSanta.rate;
        }

        // handle elephant
        if (elephant.name === currentElephant.name) {
          if (currentElephant.rate <= 0) return;
          if (obj.opened.includes(curr.elephantPos)) return;
          if (curr.elephantPos !== ec) return;
          // We're looking at the current one, check if we want to open
          obj.scorePerTurn += currentElephant.rate;
          obj.opened.push(currentElephant.name);
          obj.scores[obj.scores.length - 1] += currentElephant.rate;
        }
        nextItems.push(obj);
      });
    });
    return nextItems;
  };

  const getScoreAt = (item: Item, remaining: number, l: boolean = false) => {
    const index = 26 - remaining;
    if (index < 0) return 0;
    if (item.scores.length <= 0) return 0;
    if (index < item.scores.length) {
      return item.scores[index];
    }
    const firstUnknown = item.scores.length;
    if (!firstUnknown) return 0;
    return (
      item.scores[firstUnknown - 1] + item.scorePerTurn * (index - firstUnknown)
    );
  };

  let start = valves.get('AA');
  if (!start) return 0;
  let open: Item[] = [
    {
      santaPos: start.name,
      elephantPos: start.name,
      remaining: 26,
      scorePerTurn: 0,
      scores: [0],
      santaPath: '',
      elephantPath: '',
      opened: [],
    },
  ];

  let closed: Item[] = [];
  let done: Set<string> = new Set();
  while (open.length) {
    const current = open.shift();
    if (!current) continue;
    if (done.has(current.santaPath + ':' + current.elephantPath)) {
      continue;
    }
    closed.push(current);
    done.add(current.santaPath + ':' + current.elephantPath);

    const neighbours = gn(current);
    neighbours.forEach((n) => {
      if (done.has(n.santaPath + ':' + n.elephantPath)) {
        return;
      }
      let valSet = false;
      open = open.map((o) => {
        if (
          o.santaPos === n.santaPos &&
          o.elephantPos === n.elephantPos &&
          o.remaining === n.remaining
        ) {
          valSet = true;
          return getScoreAt(o, o.remaining) > getScoreAt(n, o.remaining)
            ? o
            : n;
        }
        return o;
      });
      if (!valSet) {
        open.push(n);
      }
    });
  }

  closed.sort((a, b) => getScoreAt(b, 0) - getScoreAt(a, 0));

  return getScoreAt(closed[0], 0, true) - closed[0].scorePerTurn;
};

export const correctOutput = 2576;
