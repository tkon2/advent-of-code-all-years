import { Position } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  const WIDTH = 7;

  type Piece = Position[];

  const PIECES: Piece[] = [
    [
      [0, 0],
      [1, 0],
      [2, 0],
      [3, 0],
    ],
    [
      [1, 0],
      [0, 1],
      [1, 1],
      [2, 1],
      [1, 2],
    ],
    [
      [2, 2],
      [2, 1],
      [0, 0],
      [1, 0],
      [2, 0],
    ],
    [
      [0, 0],
      [0, 1],
      [0, 2],
      [0, 3],
    ],
    [
      [0, 0],
      [1, 0],
      [0, 1],
      [1, 1],
    ],
  ];

  const pattern: string[] = [];

  for (let i = 0; i < lines.length; i += 1) {
    const line = lines[i];
    if (line === '') continue;
    line
      .replace('\r', '')
      .split('')
      .forEach((c) => {
        if (!['<', '>'].includes(c)) return;
        pattern.push(c);
      });
  }

  let blocked = new PositionSet();

  const isStoppedY = (piece: Piece, x: number, y: number): boolean => {
    // Check that nothing in the block is below zero
    if (piece.some((p) => y + p[1] < 0)) {
      return true;
    }
    // Check if one below each position is blocked
    const hit = piece.some((p) => blocked.has([x + p[0], y + p[1]]));
    return hit;
  };

  const isStoppedX = (
    piece: Piece,
    x: number,
    y: number,
    dir: number,
  ): boolean => {
    // Check if one left or right of each position is blocked
    if (
      piece.some((p) => {
        return blocked.has([x + p[0] + dir, y + p[1]]);
      })
    )
      return true;
    return false;
  };

  const maxBlocked = () => {
    if (blocked.size <= 0) return 0;
    const blockedYs = blocked.map((b) => b[1]);
    return Math.max(...blockedYs) + 1;
  };

  let movementIndex = 0;

  for (let i = 0; i < 2022; i++) {
    const piece = PIECES[i % PIECES.length];
    const pieceWidth = Math.max(...piece.map((p) => p[0]));
    let x = 2;
    let y = maxBlocked() + 3;
    const applyX = () => {
      const p = pattern[movementIndex++ % pattern.length];
      if (isStoppedX(piece, x, y, p === '<' ? -1 : 1)) {
        return;
      }
      x += p === '<' ? -1 : 1;
      if (x < 0) x = 0;
      if (x > WIDTH - pieceWidth - 1) x = WIDTH - pieceWidth - 1;
    };
    do {
      applyX();
      y -= 1;
    } while (!isStoppedY(piece, x, y));
    y++;
    piece.forEach((p) => blocked.add([p[0] + x, p[1] + y]));
  }

  return maxBlocked();
};

export const correctOutput = 3206;
