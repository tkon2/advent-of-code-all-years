import { Position } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  const WIDTH = 7;

  type Piece = Position[];

  const PIECES: Piece[] = [
    [
      [0, 0],
      [1, 0],
      [2, 0],
      [3, 0],
    ],
    [
      [1, 0],
      [0, 1],
      [1, 1],
      [2, 1],
      [1, 2],
    ],
    [
      [2, 2],
      [2, 1],
      [0, 0],
      [1, 0],
      [2, 0],
    ],
    [
      [0, 0],
      [0, 1],
      [0, 2],
      [0, 3],
    ],
    [
      [0, 0],
      [1, 0],
      [0, 1],
      [1, 1],
    ],
  ];

  const pattern: string[] = [];

  for (let i = 0; i < lines.length; i += 1) {
    const line = lines[i];
    if (line === '') continue;
    line
      .replace('\r', '')
      .split('')
      .forEach((c) => {
        if (!['<', '>'].includes(c)) return;
        pattern.push(c);
      });
  }

  let blocked = new PositionSet<Position>();

  const isStoppedY = (piece: Piece, x: number, y: number): boolean => {
    // Check that nothing in the block is below zero
    if (piece.some((p) => y + p[1] < 0)) {
      return true;
    }
    // Check if one below each position is blocked
    const hit = piece.some((p) => blocked.has([x + p[0], y + p[1]]));
    return hit;
  };

  const isStoppedX = (
    piece: Piece,
    x: number,
    y: number,
    dir: number,
  ): boolean => {
    // Check if one left or right of each position is blocked
    if (
      piece.some((p) => {
        return blocked.has([x + p[0] + dir, y + p[1]]);
      })
    )
      return true;
    return false;
  };

  const maxBlocked = () => {
    if (blocked.size <= 0) return 0;
    const blockedYs = blocked.map((b) => b[1]);
    return Math.max(...blockedYs) + 1;
  };

  let movementIndex = 0;

  const seen: Set<string> = new Set();

  let matched = false;

  const checkSame = (l: boolean = false) => {
    if (matched) return false;
    if (blocked.size < 100) return false;
    const max = maxBlocked();
    const signature = blocked
      .toArray()
      .slice(-100)
      .map(([x, y]) => {
        return [x, max - y].join(',');
      })
      .join(':');
    if (seen.has(signature)) {
      const index = [...seen].indexOf(signature);
      matched = true;
      return seen.size - index;
    }
    seen.add(signature);
    return false;
  };

  const totalLoops = 1000000000000;
  const maxHeights = new Map<number, number>();

  for (let i = 0; i < totalLoops; i++) {
    if (blocked.size > 1000) {
      blocked = new PositionSet(blocked.toArray().slice(-100));
    }
    const piece = PIECES[i % PIECES.length];
    const pieceWidth = Math.max(...piece.map((p) => p[0]));
    let x = 2;
    const blockedMax = maxBlocked();
    maxHeights.set(i, blockedMax);
    let y = blockedMax + 3;
    const applyX = () => {
      const p = pattern[movementIndex++ % pattern.length];
      if (isStoppedX(piece, x, y, p === '<' ? -1 : 1)) {
        return;
      }
      x += p === '<' ? -1 : 1;
      if (x < 0) x = 0;
      if (x > WIDTH - pieceWidth - 1) x = WIDTH - pieceWidth - 1;
    };
    do {
      applyX();
      y -= 1;
    } while (!isStoppedY(piece, x, y));
    y++;
    const d = checkSame();
    piece.forEach((p) => blocked.add([p[0] + x, p[1] + y]));
    if (d !== false) {
      // i is the current iteration
      // p is the previous time this iteration came up
      // d is the difference between these two
      /** Previous time pattern came up */
      const p = i - d;

      // Step 1: Get the difference in height between the tower at i
      //         and the tower at p
      // Step 2: Find the highest amount of times p can be added to i without
      //         going over the total loops
      // Step 3: Add the height difference found in step 1 to each blocked
      //         the amount of times found in step 2
      // Step 4: Add d to i the amount of times found in step 2

      // Step 1
      const heightAtP = maxHeights.get(p);
      const heightAtI = maxHeights.get(i);
      if (!heightAtI || !heightAtP) throw new Error('Index not found');
      const heightDifference = heightAtI - heightAtP;

      // Step 2
      const almost = Math.floor((totalLoops - i) / d);
      // No idea why the above isn't right, but we can fix it
      let maxTimes = almost - 500;
      while (i + maxTimes * d <= totalLoops - i) {
        maxTimes += 1;
      }
      maxTimes -= 1;

      // Step 3
      blocked = new PositionSet(
        blocked.map(([x, y]) => {
          return [x, y + maxTimes * heightDifference];
        }),
      );

      // Step 4
      i = i + d * maxTimes;
    }
  }

  return maxBlocked();
};

export const correctOutput = 1602881844347;
