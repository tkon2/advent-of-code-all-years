import { log as logFn } from 'mathjs';
import { toNumStrict } from '../../lib/functions';

import { Solution } from '../../types/solution';

const toDecimal = (str: string) => {
  const rev = str.split('').reverse();
  let res = 0;
  rev.forEach((r, i) => {
    if (r === '-') {
      res += -1 * Math.pow(5, i);
    } else if (r === '=') {
      res += -2 * Math.pow(5, i);
    } else {
      res += toNumStrict(r) * Math.pow(5, i);
    }
  });
  return res;
};

const toSNAFU = (num: number) => {
  if (num === 0) return '0';
  const logVal = Math.ceil(logFn(num, 5) as number) + 1;
  let finalNum = '0'.repeat(logVal).split('');

  for (let i = 0; i < finalNum.length; i++) {
    const D = finalNum.length - i;
    const valuePerStep = Math.pow(5, D - 1);
    const thresholds = [
      -1 * (valuePerStep + valuePerStep / 2),
      -1 * (valuePerStep / 2),
      valuePerStep / 2,
      valuePerStep + valuePerStep / 2,
    ];
    const remaining = num - toDecimal(finalNum.join(''));
    const values = ['=', '-', '0', '1', '2'];
    for (let t = 0; t <= thresholds.length; t++) {
      const low = thresholds[t - 1];
      const high = thresholds[t];
      if (low && remaining < low) continue;
      if (high && remaining > high) continue;
      finalNum[i] = values[t];
    }
  }
  return finalNum.join('').replace(/^0+/, '');
};

export const solve: Solution = async (lines) => {
  let decimal = 0;
  for (let i = 0; i < lines.length; i += 1) {
    const line = lines[i];
    if (line === '') continue;
    decimal += toDecimal(line);
  }

  return toSNAFU(decimal);
};

export const correctOutput = '20-1-0=-2=-2220=0011';
