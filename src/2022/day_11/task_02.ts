import { toNum } from '../../lib/functions';
import { Solution } from '../../types/solution';

type Operator = '*' | '^' | '+';

type Monkey = {
  items: number[];
  operand: number;
  operator: Operator;
  divisor: number;
  resultPositive: number;
  resultNegative: number;
  inspections: number;
};

export const solve: Solution = async (lines) => {
  const monkeys: Monkey[] = [];

  for (let i = 0; i < lines.length; i += 7) {
    const activeLines: string[] = [];
    for (let j = 1; j <= 5; j++) {
      activeLines.push(lines[i + j]);
    }
    let operator: Operator = '+';
    let operand = toNum(activeLines[1]) || 2;
    if (activeLines[1].includes('*')) {
      operator = '*';
    }
    if (activeLines[1].includes('old * old')) {
      operator = '^';
    }
    monkeys.push({
      items: activeLines[0]
        .replace('Starting items: ', '')
        .split(',')
        .map((v) => parseInt(v)),
      operator,
      operand,
      divisor: toNum(activeLines[2]) || 0,
      resultPositive: toNum(activeLines[3]) || 0,
      resultNegative: toNum(activeLines[4]) || 0,
      inspections: 0,
    });
  }

  let common = 1;
  monkeys.forEach((m) => {
    common *= m.divisor;
  });

  for (let round = 0; round < 10000; round++) {
    for (let i = 0; i < monkeys.length; i++) {
      const monkey = monkeys[i];
      const items = [...monkey.items];
      for (let j = 0; j < items.length; j++) {
        const item = items[j];
        monkey.inspections += 1;
        let newWl = item;
        if (monkey.operator === '*') {
          newWl = item * monkey.operand;
        } else if (monkey.operator === '+') {
          newWl = item + monkey.operand;
        } else {
          newWl = item ** monkey.operand;
        }

        newWl %= common;
        const result = newWl % monkey.divisor === 0;

        if (result) {
          monkeys[monkey.resultPositive].items.push(newWl);
        } else {
          monkeys[monkey.resultNegative].items.push(newWl);
        }
        monkey.items.shift();
      }
    }
  }

  const totals = monkeys.map((m) => m.inspections);
  totals.sort((a, b) => b - a);

  return totals[0] * totals[1];
};

export const correctOutput = 29703395016;
