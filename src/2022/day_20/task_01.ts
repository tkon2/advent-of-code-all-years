import { toNumStrict } from '../../lib/functions';

import { Solution } from '../../types/solution';

type Item = {
  next: Item;
  prev: Item;
  value: number;
  index: number;
};
type AddingItem = {
  next: AddingItem | null;
  prev: AddingItem | null;
  value: number;
  index: number;
};

export const solve: Solution = async (lines) => {
  let score = 0;
  let inputList: AddingItem[] = [];
  let prev: AddingItem | null = null;

  for (let i = 0; i < lines.length; i += 1) {
    const line = lines[i];
    if (line === '') continue;
    const obj: AddingItem = {
      next: null,
      prev,
      value: toNumStrict(line),
      index: i,
    };
    inputList.push(obj);
    if (prev != null) prev.next = obj;
    prev = obj;
  }
  inputList[0].prev = prev;
  inputList[inputList.length - 1].next = inputList[0];

  // Map to item
  const list = inputList.map((v, i): Item => {
    if (v.next === null) throw new Error('Next not defined at ' + i);
    if (v.prev === null) throw new Error('Prev not defined at ' + i);
    return v as Item;
  });

  const move = (item: Item) => {
    if (item.value === 0) return;
    item.prev.next = item.next;
    item.next.prev = item.prev;

    let curr = item;
    for (let i = 0; i < Math.abs(item.value) + (item.value < 0 ? 1 : 0); i++) {
      if (item.value < 0) {
        curr = curr.prev;
      } else {
        curr = curr.next;
      }
    }
    item.next = curr.next;
    curr.next.prev = item;
    curr.next = item;
    item.prev = curr;
  };

  for (let i = 0; i < list.length; i++) {
    const obj = list.find((l) => l.index === i);
    if (!obj) throw new Error('Object not defined');
    move(obj);
  }

  const zero = list.find((i) => i.value === 0);
  if (!zero) throw new Error('Could not find zero');

  // Expecting 4, -3, 2
  let curr = zero;
  for (let i = 1; i <= 3000; i++) {
    curr = curr.next;
    if (i % 1000 === 0) {
      score += curr.value;
    }
  }

  return score;
};

export const correctOutput = 3473;
