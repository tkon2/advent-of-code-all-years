import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let points = 0;
  type file = { name: string; size: number };
  type dir = { name: string; files: Map<string, dir | file> };

  const home: dir = { name: '/', files: new Map() };
  let cwd = home;
  let mode = 'normal';

  for (let i = 0; i < lines.length; i++) {
    if (i === 0) continue;
    const l = lines[i];
    if (mode === 'dir') {
      if (l.includes('$')) {
        mode = 'normal';
      } else {
        if (l.includes('dir')) {
          const name = l.replace('dir ', '');
          cwd.files.set(name, { name, files: new Map([['..', cwd]]) });
        } else {
          const [size, name] = l.split(' ');
          cwd.files.set(name, { name, size: parseInt(size) });
        }
      }
    }
    if (l.includes('$')) {
      // command
      if (l.includes('cd')) {
        l.replace('$ cd ', '')
          .split('/')
          .forEach((p) => {
            const newDir = cwd.files.get(p);
            if (newDir && 'files' in newDir) {
              cwd = newDir;
            }
          });
      }
      if (l.includes('ls')) {
        mode = 'dir';
      }
    }
  }

  const calculateSize = (start: dir, max: number) => {
    const toVisit: dir[] = [start];
    let total = 0;
    while (toVisit.length) {
      if (total > max) return 0;
      const next = toVisit.pop();
      if (!next) continue;
      const files = [...next.files]
        .filter((f) => f[0] !== '..')
        .map((f) => f[1]);
      files.forEach((d) => {
        if ('files' in d) {
          toVisit.push(d);
        } else {
          total += d.size || 0;
        }
      });
    }
    if (total > max) return 0;
    return total;
  };

  const toVisit: dir[] = [home];
  let total = 0;
  while (toVisit.length) {
    const next = toVisit.pop();
    if (!next) continue;
    const val = calculateSize(next, 100000);
    if (val) {
    }
    total += val;
    const files = [...next.files].filter((f) => f[0] !== '..').map((f) => f[1]);
    files.forEach((d) => {
      if ('files' in d) {
        toVisit.push(d);
      } else {
      }
    });
  }

  return total;
};

// 48381165

export const correctOutput = 1206825;
