import { log } from '../../lib/functions';
import { readClipboard, readStrings } from '../../helpers/readInput';

import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  // A for Rock, B for Paper, and C for Scissors
  // X for Rock, Y for Paper, and Z for Scissors

  const options = ['A', 'B', 'C'];

  const getOpt = (n: number) => {
    return options[(n + options.length) % options.length];
  };

  const getIndex = (s: string) => {
    return options.indexOf(s);
  };

  let points = 0;

  lines.forEach((s) => {
    let h = s
      .replace('X', 'A')
      .replace('Y', 'B')
      .replace('Z', 'C')
      .replace(' ', '');

    // Get move that fits with strategy
    const i = getIndex(h[0]);
    let n = getOpt(i);
    if (h[1] === 'A') {
      n = getOpt(i - 1);
    } else if (h[1] === 'C') {
      n = getOpt(i + 1);
    }
    h = h.replace(/\w$/, n);

    if (h === 'AA' || h === 'BB' || h === 'CC') {
      points += 3;
    } else if (h === 'AB' || h === 'BC' || h === 'CA') {
      points += 6;
    } else {
      points += 0;
    }
    switch (h[1]) {
      case 'A':
        points += 1;
        break;
      case 'B':
        points += 2;
        break;
      case 'C':
        points += 3;
        break;
    }
  });

  return points;
};

export const correctOutput = 14184;
