import { log } from '../../lib/functions';
import { readClipboard, readStrings } from '../../helpers/readInput';

import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  // A for Rock, B for Paper, and C for Scissors
  // X for Rock, Y for Paper, and Z for Scissors

  let points = 0;

  lines.forEach((s) => {
    const h = s
      .replace('X', 'A')
      .replace('Y', 'B')
      .replace('Z', 'C')
      .replace(' ', '');
    if (h === 'AA' || h === 'BB' || h === 'CC') {
      points += 3;
    } else if (h === 'AB' || h === 'BC' || h === 'CA') {
      points += 6;
    } else {
      points += 0;
    }
    switch (h[1]) {
      case 'A':
        points += 1;
        break;
      case 'B':
        points += 2;
        break;
      case 'C':
        points += 3;
        break;
    }
  });

  return points;
};

export const correctOutput = 13675;
