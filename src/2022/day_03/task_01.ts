import { alphabet } from '../../lib/constants';
import { log } from '../../lib/functions';
import { readClipboard, readStrings } from '../../helpers/readInput';

import { Solution } from '../../types/solution';
const worth = [...alphabet.split(''), ...alphabet.toUpperCase().split('')];
const getPoints = (s: string) => {
  const p = worth.indexOf(s);
  if (!p) return null;
  return p + 1;
};

export const solve: Solution = async (lines) => {
  let points = 0;

  lines.forEach((s) => {
    const a = s.slice(0, s.length / 2);
    const b = s.slice(s.length / 2);
    const common = a.split('').find((v) => b.includes(v));

    if (!common) return;
    const p = getPoints(common);
    if (!p) return;
    points += p;
  });

  return points;
};

export const correctOutput = 8394;
