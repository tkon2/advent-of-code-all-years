import { alphabet } from '../../lib/constants';
import { Solution } from '../../types/solution';

const worth = [...alphabet.split(''), ...alphabet.toUpperCase().split('')];
const getPoints = (s: string) => {
  const p = worth.indexOf(s);
  if (!p) return null;
  return p + 1;
};

export const solve: Solution = async (lines) => {
  let points = 0;

  for (let i = 0; i < lines.length - 2; i += 3) {
    const g = [lines[i], lines[i + 1], lines[i + 2]];
    const common = g[0]
      .split('')
      .find((v) => g[1].includes(v) && g[2].includes(v));

    if (!common) return null;
    const p = getPoints(common);
    if (!p) return null;
    points += p;
  }

  return points;
};

export const correctOutput = 2413;
