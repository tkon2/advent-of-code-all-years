import { Position } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let elves = new PositionSet<Position>();

  const getBounds = () => {
    /** Top, Right, Bottom, Left */
    let bounds = [Infinity, 0, 0, Infinity];
    elves.forEach(([x, y]) => {
      bounds = [
        Math.min(bounds[0], y),
        Math.max(bounds[1], x),
        Math.max(bounds[2], y),
        Math.min(bounds[3], x),
      ];
    });
    return bounds;
  };

  for (let y = 0; y < lines.length; y += 1) {
    const line = lines[y];
    if (line === '') continue;
    line.split('').forEach((c, x) => {
      if (c === '#') elves.add([x, y]);
    });
  }

  let moves: Position[][] = [
    [
      [-1, -1],
      [0, -1],
      [1, -1],
    ], // North
    [
      [-1, 1],
      [0, 1],
      [1, 1],
    ], // South
    [
      [-1, -1],
      [-1, 0],
      [-1, 1],
    ], // West
    [
      [1, -1],
      [1, 0],
      [1, 1],
    ], // East
  ];

  let socialBubble: Position[] = [
    [-1, -1],
    [0, -1],
    [1, -1],
    [-1, 0],
    /* elf */ [1, 0],
    [-1, 1],
    [0, 1],
    [1, 1],
  ];

  for (let round = 0; round < 10; round++) {
    const nextMoves = new Map<string, Position>();
    const nextDestinations = new PositionSet<Position>();
    const doubledDestinations = new PositionSet<Position>();
    elves.forEach(([ex, ey]) => {
      if (
        socialBubble.every((sb) => {
          return !elves.has([ex + sb[0], ey + sb[1]]);
        })
      ) {
        nextMoves.set([ex, ey].toString(), [ex, ey]);
        return;
      }
      const foundMove = moves.some((m) => {
        const clear = m.every((p) => {
          return !elves.has([ex + p[0], ey + p[1]]);
        });
        if (clear) {
          let pos = m[1];
          const nextX = ex + pos[0];
          const nextY = ey + pos[1];
          const nextPos: Position = [nextX, nextY];
          if (nextDestinations.has(nextPos)) {
            doubledDestinations.add(nextPos);
          }
          nextDestinations.add(nextPos);
          nextMoves.set([ex, ey].toString(), nextPos);
        }
        return clear;
      });
      if (!foundMove) {
        nextMoves.set([ex, ey].toString(), [ex, ey]);
      }
    });

    const finalMoves = new Set<string>();

    nextMoves.forEach((next, current) => {
      if (doubledDestinations.has(next)) {
        finalMoves.add(current);
      } else {
        finalMoves.add(next.join(','));
      }
    });

    elves = new PositionSet(finalMoves);

    // Put current first to last position
    const direction = moves.shift();
    if (!direction) throw new Error('Directionless and confused');
    moves.push(direction);
  }

  const getScore = () => {
    const bounds = getBounds();
    const w = bounds[2] + 1 - bounds[0];
    const h = bounds[1] + 1 - bounds[3];
    const area = w * h;
    return area - elves.size;
  };

  return getScore();
};

export const correctOutput = 3815;
