import { toNumStrict } from '../../lib/functions';
import { Position } from '../../lib/position';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let elves = new Set<string>();

  for (let y = 0; y < lines.length; y += 1) {
    const line = lines[y];
    if (line === '') continue;
    line.split('').forEach((c, x) => {
      if (c === '#') elves.add([x, y].toString());
    });
  }

  let moves: Position[][] = [
    [
      [-1, -1],
      [0, -1],
      [1, -1],
    ], // North
    [
      [-1, 1],
      [0, 1],
      [1, 1],
    ], // South
    [
      [-1, -1],
      [-1, 0],
      [-1, 1],
    ], // West
    [
      [1, -1],
      [1, 0],
      [1, 1],
    ], // East
  ];

  let socialBubble: Position[] = [
    [-1, -1],
    [0, -1],
    [1, -1],
    [-1, 0],
    /* elf */ [1, 0],
    [-1, 1],
    [0, 1],
    [1, 1],
  ];

  let i = 0;
  while (true) {
    i++;
    const nextMoves = new Map<string, string>();
    const nextDestinations = new Set<string>();
    const doubledDestinations = new Set<string>();
    elves.forEach((elf) => {
      const [ex, ey] = elf.split(',').map(toNumStrict);
      // log("Considering elf at", [ex,ey]);
      if (
        socialBubble.every((sb) => {
          return !elves.has([ex + sb[0], ey + sb[1]].toString());
        })
      ) {
        nextMoves.set([ex, ey].toString(), [ex, ey].toString());
        return;
      }
      const foundMove = moves.some((m) => {
        const clear = m.every((p) => {
          return !elves.has([ex + p[0], ey + p[1]].toString());
        });
        if (clear) {
          let pos = m[1];
          const nextX = ex + pos[0];
          const nextY = ey + pos[1];
          const nextPosStr = [nextX, nextY].toString();
          if (nextDestinations.has(nextPosStr)) {
            doubledDestinations.add(nextPosStr);
          }
          nextDestinations.add(nextPosStr);
          nextMoves.set([ex, ey].toString(), nextPosStr);
        }
        return clear;
      });
      if (!foundMove) {
        nextMoves.set([ex, ey].toString(), [ex, ey].toString());
      }
    });

    const finalMoves = new Set<string>();

    nextMoves.forEach((next, current) => {
      if (doubledDestinations.has(next)) {
        finalMoves.add(current);
      } else {
        finalMoves.add(next);
      }
    });

    // Break if next state is equal
    if ([...finalMoves].toString() === [...elves].toString()) break;

    elves = finalMoves;

    // Put current first to last position
    const direction = moves.shift();
    if (!direction) throw new Error('Directionless and confused');
    moves.push(direction);
  }

  return i;
};

export const correctOutput = 893;
