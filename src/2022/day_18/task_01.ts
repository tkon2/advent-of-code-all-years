import { toNumStrict } from '../../lib/functions';
import { crossDirs3, Position3 } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';

import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let score = 0;
  let cubes = new PositionSet<Position3>();

  for (let i = 0; i < lines.length; i += 1) {
    const line = lines[i];
    if (line === '') continue;
    cubes.add(line.replace('\r', '').split(',').map(toNumStrict) as Position3);
  }

  cubes.forEach(([x, y, z]) => {
    let sides = 6;
    crossDirs3.forEach((d) => {
      if (cubes.has([x + d[0], y + d[1], z + d[2]])) {
        sides -= 1;
      }
    });
    score += sides;
  });

  return score;
};

export const correctOutput = 3542;
