import { toNumStrict } from '../../lib/functions';
import { Position3, crossDirs3 } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let lim = 0;
  let score = 0;
  let cubes = new PositionSet<Position3>();

  for (let i = 0; i < lines.length; i += 1) {
    const line = lines[i];
    if (line === '') continue;
    cubes.add(line.replace('\r', '').split(',').map(toNumStrict) as Position3);
    const [x, y, z] = line.split(',').map(toNumStrict);
    lim = Math.max(lim, x, y, z);
  }

  // Add 1 so we can navigate around the outer cubes
  lim += 1;

  let closed = new PositionSet<Position3>();
  let open: Position3[] = [[-1, -1, -1]];
  while (open.length > 0) {
    const current = open.pop();
    if (!current) continue;

    if (closed.has(current)) continue;
    closed.add(current);

    const [cx, cy, cz] = current;
    crossDirs3.forEach((d) => {
      const x = cx + d[0];
      const y = cy + d[1];
      const z = cz + d[2];

      if (x < -1 || x > lim) return;
      if (y < -1 || y > lim) return;
      if (z < -1 || z > lim) return;

      if (cubes.has([x, y, z])) {
        score += 1;
      } else {
        open.push([x, y, z]);
      }
    });
  }

  return score;
};

export const correctOutput = 2080;
