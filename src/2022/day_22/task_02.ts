import { toNumStrict } from '../../lib/functions';
import { Position } from '../../lib/position';

import { Solution } from '../../types/solution';
const cubeSize = 50;

const cubeAreas = [
  [1, 0],
  [2, 0],
  [1, 1],
  [0, 2],
  [1, 2],
  [0, 3],
];

/** Gets the next step after you would walk off the edge */
const getCoords = (
  pos: Position,
  heading: number,
): { pos: Position; heading: number } => {
  const cubecoords = [
    Math.floor(pos[0] / cubeSize),
    Math.floor(pos[1] / cubeSize),
  ];
  const cubeIndex = cubeAreas.findIndex(
    (ca) => ca[0] === cubecoords[0] && ca[1] === cubecoords[1],
  );
  if (cubeIndex < 0)
    throw new Error(
      `Could not find index for [${cubecoords}] with real coords [${pos}]`,
    );
  const cubePos: Position = [pos[0] % cubeSize, pos[1] % cubeSize];

  const apply = (cubeRel: Position, applyRot: number) => {
    let rot: Position = [...cubePos];
    for (let i = 0; i < applyRot; i++) {
      rot = [
        cubeSize - 1 - rot[1], // size - 1 - Y
        rot[0], // X
      ];
    }
    let head = (heading + applyRot) % 4;
    if (head % 2 === 0) {
      rot[1] = cubeSize - 1 - rot[1];
    }
    if (head % 2 === 1) {
      rot[0] = cubeSize - 1 - rot[0];
    }
    head = (head + 2) % 4;
    let actual = rot.map(
      (r, i) => r + (cubeAreas[cubeIndex][i] + cubeRel[i]) * cubeSize,
    ) as Position;
    if (actual[0] < 0 || actual[1] < 0)
      throw new Error('Coodinates out of bound: ' + actual);
    return { pos: actual, heading: head };
  };

  if (cubeIndex === 0 && heading === 3) return apply([-1, 3], 3); // Up
  if (cubeIndex === 0 && heading === 2) return apply([-1, 2], 0); // Left

  if (cubeIndex === 1 && heading === 0) return apply([-1, 2], 0); // Right
  if (cubeIndex === 1 && heading === 1) return apply([-1, 1], 3); // Down
  if (cubeIndex === 1 && heading === 3) return apply([-2, 3], 2); // Up

  if (cubeIndex === 2 && heading === 0) return apply([1, -1], 1); // Right
  if (cubeIndex === 2 && heading === 2) return apply([-1, 1], 1); // Left

  if (cubeIndex === 3 && heading === 2) return apply([1, -2], 0); // Left
  if (cubeIndex === 3 && heading === 3) return apply([1, -1], 3); // Up

  if (cubeIndex === 4 && heading === 0) return apply([1, -2], 0); // Right
  if (cubeIndex === 4 && heading === 1) return apply([-1, 1], 3); // Down

  if (cubeIndex === 5 && heading === 0) return apply([1, -1], 1); // Right
  if (cubeIndex === 5 && heading === 1) return apply([2, -3], 2); // Down
  if (cubeIndex === 5 && heading === 2) return apply([1, -3], 1); // Left

  throw new Error(`Case not implemented for ${cubeIndex} heading ${heading}`);
};

const getDir = (heading: number): [number, number] => {
  if (heading === 0) {
    return [1, 0];
  }
  if (heading === 1) {
    return [0, 1];
  }
  if (heading === 2) {
    return [-1, 0];
  }
  if (heading === 3) {
    return [0, -1];
  }
  return [0, 0];
};

export const solve: Solution = async (lines) => {
  let instructions: (string | number)[] = [];
  let board = new Map<string, boolean>();
  let lims: [number, number] = [0, 0];

  for (let i = 0; i < lines.length; i += 1) {
    const line = lines[i];
    if (line === '') continue;
    if (line.match(/[LR]/)) {
      let prevNum = '';
      line.split('').forEach((c) => {
        if (c.match(/[LR]/)) {
          instructions.push(toNumStrict(prevNum));
          instructions.push(c);
          prevNum = '';
        } else {
          prevNum += c;
        }
      });
      instructions.push(toNumStrict(prevNum));
    } else {
      line.split('').forEach((c, ci) => {
        if (c === '.') {
          board.set([ci, i].toString(), false);
          lims = [Math.max(lims[0], ci), Math.max(lims[1], i)];
        } else if (c === '#') {
          board.set([ci, i].toString(), true);
          lims = [Math.max(lims[0], ci), Math.max(lims[1], i)];
        }
      });
    }
  }
  lims[0]++;
  lims[1]++;

  /** Position is where to check, dir is where to go if none found */
  const getPos = (pos: [number, number], dir: [number, number]): Position => {
    if (board.has(pos.toString())) return pos;
    let curr: Position = [...pos];
    while (!board.has(curr.toString())) {
      const newX = (curr[0] + lims[0] + dir[0]) % lims[0];
      const newY = (curr[1] + lims[1] + dir[1]) % lims[1];
      curr = [newX, newY];
    }
    return curr;
  };

  // 0: right, 1: down, 2: left, 3: up
  let heading: number = 0;
  let position: Position = getPos([0, 0], [1, 0]);

  const move = (amt: number, moveH: number) => {
    let dir = getDir(moveH);
    for (let i = 1; i <= amt; i++) {
      let x = position[0] + dir[0];
      let y = position[1] + dir[1];
      let h = moveH;
      if (!board.has([x, y].toString())) {
        const newPos = getCoords(position, moveH);
        [x, y] = newPos.pos;
        h = newPos.heading;
      }
      if (board.get([x, y].toString()) === true) {
        // hit a wall
        break;
      }
      position = [x, y];
      heading = h;
      moveH = h;
      dir = getDir(moveH);
    }
  };

  move(1, 0);
  instructions.forEach((instruction) => {
    if (instruction === 'R') {
      heading = (heading + 1) % 4;
    } else if (instruction === 'L') {
      heading = (heading + 3) % 4;
    } else {
      if (typeof instruction !== 'number')
        throw new Error('Instruction not numeric');
      try {
        move(instruction, heading);
      } catch (e) {
        throw e;
      }
    }
  });

  // 61220 too high

  // 31422 too low
  // 123149

  return 1000 * (position[1] + 1) + 4 * (position[0] + 1) + heading;
};

export const correctOutput = 123149;
