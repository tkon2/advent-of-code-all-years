import { toNumStrict } from '../../lib/functions';
import { Position } from '../../lib/position';
import { Solution } from '../../types/solution';

const getDir = (heading: number): [number, number] => {
  if (heading === 0) {
    return [1, 0];
  }
  if (heading === 1) {
    return [0, 1];
  }
  if (heading === 2) {
    return [-1, 0];
  }
  if (heading === 3) {
    return [0, -1];
  }
  return [0, 0];
};

export const solve: Solution = async (lines) => {
  let instructions: (string | number)[] = [];
  let board = new Map<string, boolean>();
  let lims: [number, number] = [0, 0];

  for (let i = 0; i < lines.length; i += 1) {
    const line = lines[i];
    if (line === '') continue;
    if (line.match(/[LR]/)) {
      let prevNum = '';
      line.split('').forEach((c) => {
        if (c.match(/[LR]/)) {
          instructions.push(toNumStrict(prevNum));
          instructions.push(c);
          prevNum = '';
        } else {
          prevNum += c;
        }
      });
      instructions.push(toNumStrict(prevNum));
    } else {
      line.split('').forEach((c, ci) => {
        if (c === '.') {
          board.set([ci, i].toString(), false);
          lims = [Math.max(lims[0], ci), Math.max(lims[1], i)];
        } else if (c === '#') {
          board.set([ci, i].toString(), true);
          lims = [Math.max(lims[0], ci), Math.max(lims[1], i)];
        }
      });
    }
  }
  lims[0]++;
  lims[1]++;

  /** Position is where to check, dir is where to go if none found */
  const getPos = (pos: [number, number], dir: [number, number]): Position => {
    if (board.has(pos.toString())) return pos;
    let curr: Position = [...pos];
    while (!board.has(curr.toString())) {
      const newX = (curr[0] + lims[0] + dir[0]) % lims[0];
      const newY = (curr[1] + lims[1] + dir[1]) % lims[1];
      curr = [newX, newY];
    }
    return curr;
  };

  // 0: right, 1: down, 2: left, 3: up
  let heading: number = 0;
  let position: Position = [0, 0];

  const move = (amt: number, heading: number) => {
    const dir = getDir(heading);
    for (let i = 1; i <= amt; i++) {
      const x = position[0] + dir[0];
      const y = position[1] + dir[1];
      const pos = getPos([x, y], dir);
      if (board.get(pos.toString()) === true) {
        // hit a wall
        break;
      }
      position = pos;
    }
  };

  move(1, 0);
  instructions.forEach((instruction) => {
    if (instruction === 'R') {
      heading = (heading + 1) % 4;
    } else if (instruction === 'L') {
      heading = (heading + 3) % 4;
    } else {
      if (typeof instruction !== 'number')
        throw new Error('Instruction not numeric');
      move(instruction, heading);
    }
  });

  return 1000 * (position[1] + 1) + 4 * (position[0] + 1) + heading;
};

export const correctOutput = 55244;
