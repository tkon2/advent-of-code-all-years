import { range } from '../../lib/arrays';
import { toNumStrict } from '../../lib/functions';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { clip }) => {
  const goalY = clip ? 10 : 2000000;
  let goalValues = new PositionSet();

  for (let i = 0; i < lines.length; i += 1) {
    const line = lines[i];
    if (line === '') continue;
    const [sensor, beacon] = line.split(':');
    /** Sensor X and Y Actual */
    const [sx, sy] = sensor.split(',').map((v) => toNumStrict(v));
    /** Beacon X and Y Actual */
    const [bx, by] = beacon.split(',').map((v) => toNumStrict(v));
    const bDeltaX = sx - bx;
    const bDeltaY = sy - by;
    const manhattan = Math.abs(bDeltaX) + Math.abs(bDeltaY);
    const halfWidth = manhattan;
    const lineWidth = halfWidth - Math.abs(sy - goalY);
    if (lineWidth <= 0) continue;
    const startX = sx - lineWidth;
    const endX = sx + lineWidth;

    // width reduces by two each y down or up, but always centered at bax
    range(startX, endX + 1).forEach((goalX) => {
      if (goalX === bx && goalY === by) return;
      goalValues.add([goalX, goalY]);
    });
  }
  return goalValues.size;
};

export const correctOutput = 5144286;
