import { toNumStrict } from '../../lib/functions';
import { Position } from '../../lib/position';

import { Solution } from '../../types/solution';

const manhattanDistance = (
  [sx, sy]: [number, number],
  [ex, ey]: [number, number],
) => {
  const bDeltaX = Math.abs(sx - ex);
  const bDeltaY = Math.abs(sy - ey);
  const radius = bDeltaX + bDeltaY;
  return radius;
};

type Scan = {
  pos: Position;
  radius: number;
};

export const solve: Solution = async (lines, { clip }) => {
  const goalSize = clip ? 20 : 4000000;
  let score = 0;
  let scans: Scan[] = [];

  for (let i = 0; i < lines.length; i += 1) {
    const line = lines[i];
    if (line === '') continue;
    const [sensor, beacon] = line.split(':');
    /** Sensor X and Y Actual */
    const sensorPos = sensor.split(',').map(toNumStrict) as [number, number];
    /** Beacon X and Y Actual */
    const beaconPos = beacon.split(',').map(toNumStrict) as [number, number];
    if (sensorPos.length < 2) continue;
    if (beaconPos.length < 2) continue;
    const radius = manhattanDistance(sensorPos, beaconPos);
    scans.push({ pos: sensorPos, radius });
  }

  const isWithin = (scan: Scan, pos: [number, number]) => {
    return manhattanDistance(scan.pos, pos) <= scan.radius;
  };

  const check = ([x, y]: [number, number]) => {
    if (score !== 0) return;
    if (x < 0 || y < 0) return false;
    if (x > goalSize || y > goalSize) return false;
    if (scans.every((s) => !isWithin(s, [x, y]))) {
      score = x * 4000000 + y;
    }
  };

  scans.forEach((s) => {
    if (score !== 0) return;
    // Loop through all points manhattan+1 distance away, and check agains other
    // scans to check it's not used. Since there is only one valid point it
    // *must* be next to an area of effect
    const radius = s.radius + 1;
    const [x, y] = s.pos;

    // Top Right
    for (let i = 0; i < radius; i++) {
      const px = x + i;
      const py = y + (radius - i);
      check([px, py]);
    }

    // Bottom Right
    for (let i = 0; i < radius; i++) {
      const px = x + (radius - i);
      const py = y - i;
      check([px, py]);
    }

    // Bottom Left
    for (let i = 0; i < radius; i++) {
      const px = x - (radius - i);
      const py = y - i;
      check([px, py]);
    }

    // Top Left
    for (let i = 0; i < radius; i++) {
      const px = x - i;
      const py = y + (radius - i);
      check([px, py]);
    }
  });

  return score;
};

export const correctOutput = 10229191267339;
