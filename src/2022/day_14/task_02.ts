import { range } from '../../lib/arrays';
import { toNumStrict } from '../../lib/functions';
import { equal, Position } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let score = 0;

  const blocked = new PositionSet<Position>();
  const source: Position = [500, 0];

  for (let i = 0; i < lines.length; i += 1) {
    const line = lines[i];
    const points = line.split(' -> ');
    let prev: Position | null = null;
    points.forEach((p) => {
      const [x, y] = p.split(',').map((v) => toNumStrict(v));
      blocked.add([x, y]);
      if (prev === null) {
        prev = [x, y];
        return;
      }
      const [px, py] = prev;
      blocked.add([px, py]);
      range(Math.min(px, x), Math.max(x, px)).forEach((v) => {
        blocked.add([v, y]);
      });
      range(Math.min(py, y), Math.max(y, py)).forEach((v) => {
        blocked.add([x, v]);
      });
      prev = [x, y];
    });
  }
  const rockLims = [Infinity, 0, 0, Infinity];
  let rockBottom = 0;
  blocked.toArray().forEach(([x, y]) => {
    rockBottom = Math.max(rockBottom, y);
    rockLims[0] = Math.min(rockLims[0], y);
    rockLims[1] = Math.max(rockLims[1], x);
    rockLims[2] = Math.max(rockLims[2], y);
    rockLims[3] = Math.min(rockLims[3], x);
  });
  rockBottom += 2;

  const hit = (p: Position) => blocked.has(p);

  let i = 0;
  let finalPath: Position[] = [];
  while (score === 0) {
    i++;
    if (hit(source)) {
      score = i - 1;
    }
    let sand: Position = [...source];
    let prev: Position | null = null;
    finalPath = [];
    do {
      const [x, y] = sand;
      finalPath.push([x, y]);
      prev = [x, y];
      if (sand[1] + 1 === rockBottom) {
      } else if (!hit([x, y + 1])) {
        sand[1] += 1;
      } else if (!hit([x - 1, y + 1])) {
        sand[0] -= 1;
        sand[1] += 1;
      } else if (!hit([x + 1, y + 1])) {
        sand[0] += 1;
        sand[1] += 1;
      }
    } while (!equal(sand, prev));
    if (score === 0) {
      blocked.add(sand);
    }
  }

  return score;
};

export const correctOutput = 26170;
