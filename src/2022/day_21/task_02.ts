import { toNumStrict } from '../../lib/functions';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let known = new Map<string, number>();
  let todo: { name: string; data: string }[] = [];

  for (let i = 0; i < lines.length; i += 1) {
    const line = lines[i];
    if (line === '') continue;
    const [name, data] = line.split(': ');
    if (data.match(/\d/)) {
      known.set(name, toNumStrict(data));
    } else {
      todo.push({ name, data });
    }
  }

  const test = (num: number): number | null => {
    const lTodo = [...todo];
    const lKnown = new Map([...known]);
    lKnown.set('humn', num);
    while (lTodo.length) {
      const current = lTodo.shift();
      if (!current) throw new Error('Out of range');
      const [a, operator, b] = current.data.split(' ');
      if (lKnown.has(a) && lKnown.has(b)) {
        if (current.name === 'root') {
          return (lKnown.get(a) as number) - (lKnown.get(b) as number);
        }
        lKnown.set(
          current.name,
          eval('' + lKnown.get(a) + operator + lKnown.get(b)),
        );
      } else {
        lTodo.push(current);
      }
    }
    return null;
  };

  let i = 0;
  let ans: number | null = null;

  // Get closer to the answer one digit at a time
  for (let N = 12; N >= 0; N--) {
    ans = test(i);
    if (ans && ans > 0) {
      do {
        ans = test(i);
        i += Math.pow(10, N);
      } while (ans != null && ans > 0);
    } else {
      do {
        ans = test(i);
        i -= Math.pow(10, N);
      } while (ans != null && ans < 0);
    }
  }

  // Because we go one over
  i -= 1;

  return i;
};

export const correctOutput = 3403989691757;
