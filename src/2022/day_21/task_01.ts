import { toNumStrict } from '../../lib/functions';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines) => {
  let score = 0;
  let known = new Map<string, number>();
  let todo: { name: string; data: string }[] = [];

  for (let i = 0; i < lines.length; i += 1) {
    const line = lines[i];
    if (line === '') continue;
    const [name, data] = line.split(': ');
    if (data.match(/\d/)) {
      known.set(name, toNumStrict(data));
    } else {
      todo.push({ name, data });
    }
  }

  while (todo.length) {
    const current = todo.shift();
    if (!current) throw new Error('Out of range');
    const [a, operator, b] = current.data.split(' ');
    if (known.has(a) && known.has(b)) {
      known.set(
        current.name,
        eval('' + known.get(a) + operator + known.get(b)),
      );
      if (current.name === 'root') {
        return known.get('root') ?? null;
      }
    } else {
      todo.push(current);
    }
  }

  return known.get('root') ?? null;
};

export const correctOutput = 56490240862410;
