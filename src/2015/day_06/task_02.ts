import { sum } from '../../lib/arrays';
import { Solution } from '../../types/solution';

const vowels = 'aeiou'.split('');

export const solve: Solution = async (strs) => {
  const arr: number[][] = [];
  for (let i = 0; i < 1000; i++) {
    arr[i] = [];
    for (let j = 0; j < 1000; j++) {
      arr[i][j] = 0;
    }
  }

  strs.forEach((instruction) => {
    const coords = instruction
      .replace(/(turn on|turn off|toggle)/, '')
      .replace(' through ', 'x')
      .split('x')
      .map((v) => v.split(',').map((c) => parseInt(c)));
    let f = (v: number) => v;
    if (instruction.includes('turn off')) {
      f = (v: number) => Math.max(0, v - 1);
    } else if (instruction.includes('turn on')) {
      f = (v: number) => v + 1;
    } else {
      f = (v: number) => v + 2;
    }

    const xl = Math.min(coords[0][0], coords[1][0]);
    const yl = Math.min(coords[0][1], coords[1][1]);
    const xh = Math.max(coords[0][0], coords[1][0]);
    const yh = Math.max(coords[0][1], coords[1][1]);

    for (let x = xl; x <= xh; x += 1) {
      for (let y = yl; y <= yh; y += 1) {
        arr[x][y] = f(arr[x][y]);
      }
    }
  });

  return sum(arr.map((a) => sum(a)));
};

export const correctOutput = 15343601;
