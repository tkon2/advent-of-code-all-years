import { sum } from '../../lib/arrays';
import { Solution } from '../../types/solution';

export const solve: Solution = async (strs) => {
  const arr: boolean[][] = [];
  for (let i = 0; i < 1000; i++) {
    arr[i] = [];
    for (let j = 0; j < 1000; j++) {
      arr[i][j] = false;
    }
  }

  strs.forEach((instruction) => {
    const coords = instruction
      .replace(/(turn on|turn off|toggle)/, '')
      .replace(' through ', 'x')
      .split('x')
      .map((v) => v.split(',').map((c) => parseInt(c)));
    let f = (v: boolean) => v;
    if (instruction.includes('turn off')) {
      f = (v: boolean) => false;
    } else if (instruction.includes('turn on')) {
      f = (v: boolean) => true;
    } else {
      f = (v: boolean) => !v;
    }

    const xl = Math.min(coords[0][0], coords[1][0]);
    const yl = Math.min(coords[0][1], coords[1][1]);
    const xh = Math.max(coords[0][0], coords[1][0]);
    const yh = Math.max(coords[0][1], coords[1][1]);

    for (let x = xl; x <= xh; x += 1) {
      for (let y = yl; y <= yh; y += 1) {
        arr[x][y] = f(arr[x][y]);
      }
    }
  });

  return sum(arr.map((a) => sum(a.map((b) => (b ? 1 : 0)))));
};

export const correctOutput = 400410;
