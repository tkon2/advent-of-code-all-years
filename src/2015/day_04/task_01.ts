import md5 from 'md5';
import { Solution } from '../../types/solution';

export const solve: Solution = async (strs) => {
  const prefix = strs[0];

  let i = 0;

  while (true) {
    const hash = md5(`${prefix}${i}`);
    if (hash.slice(0, 5) === '00000') {
      return i;
    }
    i++;
  }
};

export const correctOutput = 254575;
