import { Solution } from '../../types/solution';

const vowels = 'aeiou'.split('');

export const solve: Solution = async (strs) => {
  let total = 0;

  strs.forEach((s) => {
    if (s.match(/(ab|cd|pq|xy)/)) return;
    let double = false;
    let vs = 0;
    for (let i = 0; i < s.length; i++) {
      if (i > 0 && !double && s[i] === s[i - 1]) double = true;
      if (vowels.includes(s[i])) vs++;
    }

    if (!double) return;
    if (vs < 3) return;
    total += 1;
  });

  return total;
};

export const correctOutput = 238;
