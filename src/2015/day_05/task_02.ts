import { Solution } from '../../types/solution';

export const solve: Solution = async (strs) => {
  let total = 0;

  strs.forEach((s) => {
    let oneApart = false;
    let repeat = false;
    for (let i = 0; i < s.length; i++) {
      if (i > 1 && !oneApart && s[i - 2] === s[i]) oneApart = true;
      if (i > 0 && !repeat && s.slice(i + 1).includes(s[i - 1] + s[i]))
        repeat = true;
    }

    if (!oneApart) return;
    if (!repeat) return;

    total += 1;
  });

  return total;
};

export const correctOutput = 69;
