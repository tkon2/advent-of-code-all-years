import { Solution } from '../../types/solution';

export const solve: Solution = async ([line]) => {
  const arr = line.split('');
  let current = 0;
  for (let i = 0; i < arr.length; i++) {
    current += arr[i] === '(' ? 1 : -1;
    if (current < 0) return i + 1;
  }
  return null;
};

export const correctOutput = 1795;
