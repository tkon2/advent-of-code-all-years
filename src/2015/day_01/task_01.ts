import { Solution } from '../../types/solution';

export const solve: Solution = async (data) => {
  return data[0].split('').reduce((c, v) => c + (v === '(' ? 1 : -1), 0);
};

export const correctOutput = 74;
