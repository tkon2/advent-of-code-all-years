import { Solution } from '../../types/solution';

export const solve: Solution = async (strs) => {
  let total = 0;
  strs.forEach((s) => {
    const [l, w, h] = s.split('x').map((s) => parseInt(s));
    const volume = l * w * h;
    const max = Math.max(l, w, h);
    let shortest = l + w;
    if (l === max) {
      shortest = w + h;
    } else if (w === max) {
      shortest = l + h;
    }
    total += shortest * 2 + volume;
  });

  return total;
};

export const correctOutput = 3737498;
