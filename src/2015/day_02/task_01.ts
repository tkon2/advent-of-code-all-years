import { Solution } from '../../types/solution';

export const solve: Solution = async (strs) => {
  let total = 0;
  strs.forEach((s) => {
    const [l, w, h] = s.split('x').map((s) => parseInt(s));
    const sides = [l * w, w * h, h * l];
    const extra = Math.min(...sides);
    const boxTotal = sides.reduce((c, v) => c + v * 2, 0);
    total += extra + boxTotal;
  });

  return total;
};

export const correctOutput = 1586300;
