import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

const getDir = (str: string) => {
  if (str === 'v') return [0, -1];
  if (str === '<') return [-1, 0];
  if (str === '>') return [1, 0];
  if (str === '^') return [0, 1];
  return [0, 0];
};

export const solve: Solution = async (strs) => {
  const instructions = strs[0].split('');

  const visited = new PositionSet([[0, 0]]);
  const santa: [number, number] = [0, 0];

  const robot: [number, number] = [0, 0];

  instructions.forEach((i, iterator) => {
    const delta = getDir(i);
    if (iterator % 2 === 0) {
      santa[0] += delta[0];
      santa[1] += delta[1];
      visited.add(santa);
    } else {
      robot[0] += delta[0];
      robot[1] += delta[1];
      visited.add(robot);
    }
  });

  return visited.size;
};

export const correctOutput = 2631;
