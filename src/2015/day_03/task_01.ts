import { Position } from '../../lib/position';
import { PositionSet } from '../../lib/PositionSet';
import { Solution } from '../../types/solution';

const getDir = (str: string): Position => {
  if (str === 'v') return [0, -1];
  if (str === '<') return [-1, 0];
  if (str === '>') return [1, 0];
  if (str === '^') return [0, 1];
  return [0, 0];
};

export const solve: Solution = async (strs) => {
  const instructions = strs[0].split('');

  const visited = new PositionSet([[0, 0]]);
  const current: Position = [0, 0];

  instructions.forEach((i) => {
    const delta = getDir(i);
    current[0] += delta[0];
    current[1] += delta[1];
    visited.add(current);
  });

  return visited.size;
};

export const correctOutput = 2572;
