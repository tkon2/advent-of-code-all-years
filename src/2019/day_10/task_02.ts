import { distance } from 'mathjs';
import { Position } from '../../lib/position';
import { Solution } from '../../types/solution';
import { roundTo } from '../../lib/numbers';

const angleBetween = (source: Position, target: Position) => {
  return roundTo(
    Math.atan2(target[0] - source[0], target[1] - source[1]) + Math.PI,
    3,
  );
};

export const solve: Solution = async (lines, { log }) => {
  let asteroids: [number, number][] = [];

  for (let i = 0; i < lines.length; i++) {
    for (let j = 0; j < lines[i].length; j++) {
      if (lines[i][j] === '#') {
        asteroids.push([i, j]);
      }
    }
  }

  let highest = 0;
  let match: Position = [0, 0];
  for (let ai = 0; ai < asteroids.length; ai++) {
    const a = asteroids[ai];
    const seenAngles = new Set<number>();
    for (let bi = 0; bi < asteroids.length; bi++) {
      if (bi === ai) continue;
      const b = asteroids[bi];
      const angle = angleBetween(a, b);
      seenAngles.add(angle);
    }
    const canSee = seenAngles.size;
    if (canSee > highest) {
      highest = canSee;
      match = a;
    }
  }

  asteroids = asteroids.filter((a) => a[0] !== match[0] || a[1] !== match[1]);
  asteroids.sort((a, b) => +distance(match, a) - +distance(match, b));

  log('Source', match);

  const TWO_PI = roundTo(Math.PI * 2, 3);

  let lastDestroyed = match;
  let currentRotation = +angleBetween(match, [match[0] - 1, match[1]]);
  for (let r = 0; r < 200; r++) {
    log('\nIteration %d,', r + 1, 'rotated to', currentRotation);
    let bestAngle: number | undefined = undefined;
    let bestAsteroid: Position | undefined = undefined;
    for (let i = 0; i < asteroids.length; i++) {
      const asteroid = asteroids[i];
      let angle = +angleBetween(match, asteroid);
      if (angle < currentRotation) angle = roundTo(angle + TWO_PI, 3);
      if (bestAngle !== undefined && angle >= bestAngle) continue;
      if (r === 0 && angle < currentRotation) continue;
      if (r !== 0 && angle <= currentRotation) continue;
      bestAngle = angle;
      bestAsteroid = asteroid;
      log('New best', bestAngle, bestAsteroid);
    }
    if (!bestAsteroid || !bestAngle) throw new Error('No new asteroid to kill');
    lastDestroyed = bestAsteroid;
    log('Destroying', bestAsteroid.toReversed(), 'at', bestAngle);
    asteroids = asteroids.filter(
      (a) => a[0] !== bestAsteroid[0] || a[1] !== bestAsteroid[1],
    );
    currentRotation = roundTo(bestAngle % TWO_PI, 3);
  }

  log(lastDestroyed);

  log('Angle', angleBetween(match, [12, 11]));

  return lastDestroyed[1] * 100 + lastDestroyed[0];
};

export const correctOutput = 204;
