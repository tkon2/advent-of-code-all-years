import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  const asteroids: [number,number][] = [];

  for (let i = 0; i < lines.length; i++) {
    for (let j = 0; j < lines[i].length; j++) {
      if (lines[i][j] === '#') {
        asteroids.push([i,j]);
      }
    }
  }

  let highest = 0;
  let match = [0,0];
  for (let ai = 0; ai < asteroids.length; ai++) {
    const a = asteroids[ai];
    const seenAngles = new Set<number>();
    log("CHECKING", a, seenAngles.size);
    for (let bi = 0; bi < asteroids.length; bi++) {
      if (bi === ai) continue;
      const b = asteroids[bi];
      const angle = Math.atan2((b[0]-a[0]),(b[1]-a[1]));
      log(' ',b, angle, seenAngles.has(angle) ? 'ALREADY SEEN' : 'NEW');
      seenAngles.add(angle);
    }
    const canSee = seenAngles.size;
    log("  TOTAL", canSee);
    if (canSee > highest) {
      highest = canSee;
      match = a;
    }

  }

  log(match);
  return highest;
};


export const correctOutput = 296;
