import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let total = 0;

  for (let i = 0; i < lines.length; i++) {
    const mass = tn(lines[i]);
    const reqFuel = Math.floor(mass / 3) - 2;
    log(mass, reqFuel);
    total += reqFuel;
  }

  return total;
};

export const correctOutput = 3262991;
