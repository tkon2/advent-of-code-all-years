import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

const getFuel = (mass: number) => {
  let total = 0;
  let maxTries = 5000;
  for (let remaining = mass; remaining > 0 && maxTries-- > 0; ) {
    remaining = Math.floor(remaining / 3) - 2;
    total += Math.max(0, remaining);
  }
  return total;
};

export const solve: Solution = async (lines, { log }) => {
  let total = 0;

  for (let i = 0; i < lines.length; i++) {
    const mass = tn(lines[i]);
    const fuel = getFuel(mass);
    log(mass, fuel);
    total += fuel;
  }

  return total;
};

export const correctOutput = 4891620;
