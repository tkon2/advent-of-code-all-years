/**
 * A function that gets the contents of data.txt as a list of lines, and
 * should return the correct output for that day
 */
export type Solution = (
  input: string[],
  utils: {
    /** True if the program should log dev output.
     * Only true when single task is ran */
    logging: boolean;
    /** Function to log output that is only enabled in single-solves */
    log: (...args: any[]) => void;
    /** True if the data comes from the clipboard */
    clip: boolean;
  },
) => Promise<number | string | null>;
