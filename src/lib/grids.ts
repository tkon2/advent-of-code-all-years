/** Uses the provided logging function to display a 2D array */
export const show = (log: (...args: any[]) => void, arr: any[][]) => {
  const maxX = Math.max(...arr.map((a) => a.length));
  log(`/${'-'.repeat(1 + 2 * maxX)}\\`);
  for (let y = 0; y < arr.length; y++) {
    let line = '';
    for (let x = 0; x < maxX; x++) {
      line += arr[y][x] ?? ' ';
      line += ' ';
    }
    log(`| ${line}|`);
  }
  log(`\\${'-'.repeat(1 + 2 * maxX)}/`);
};

/** Rotates a 2D array clockwise */
export const rotate = <T>(arr: T[][]) => {
  const flipped: T[][] = [];
  for (let x = 0; x < arr.length; x++) {
    for (let y = 0; y < arr[arr.length - 1 - x].length; y++) {
      const value = arr[arr.length - 1 - x]?.[y];
      if (!flipped[y]) flipped[y] = [];
      flipped[y][x] = value;
    }
  }
  return flipped;
};

/**
 * Rotate a list of strings clockwise as if it was an array.
 * Assumes a rectangle, missing values are replaced by spaces
 */
export const rotateStr = (arr: string[]) => {
  const maxC = Math.max(...arr.map((a) => a.length));
  let out: string[] = [];
  for (let c = 0; c < maxC; c++) {
    let str = '';
    for (let r = 0; r < arr.length; r++) {
      str += arr[r]?.[c] ?? ' ';
    }
    out.push(str);
  }
  return out;
};

/**
 * Return the four neigboring spots in the array,
 * or undefined if they don't exist.
 */
export const get4N = <T>(
  /** 2D array of items */
  arr: T[][],
  /** Row or X coordinate */
  x: number,
  /** Column or Y coordinate */
  y: number,
): { x: number; y: number; v: T | undefined }[] => {
  return [
    { x: x + 1, y, v: arr[y]?.[x + 1] },
    { x, y: y + 1, v: arr[y + 1]?.[x] },
    { x: x - 1, y, v: arr[y]?.[x - 1] },
    { x, y: y - 1, v: arr[y - 1]?.[x] },
  ];
};

/**
 * Return the eight neigboring spots in the array,
 * or undefined if they don't exist.
 */
export const get8N = <T>(
  /** 2D array of items */
  arr: T[][],
  /** Row or X coordinate */
  x: number,
  /** Column or Y coordinate */
  y: number,
): { x: number; y: number; v: T | undefined }[] => {
  return [
    { y: y + 0, x: x + 1, v: arr[y + 0]?.[x + 1] },
    { y: y + 1, x: x + 1, v: arr[y + 1]?.[x + 1] },
    { y: y + 1, x: x + 0, v: arr[y + 1]?.[x + 0] },
    { y: y + 1, x: x - 1, v: arr[y + 1]?.[x - 1] },
    { y: y + 0, x: x - 1, v: arr[y + 0]?.[x - 1] },
    { y: y - 1, x: x - 1, v: arr[y - 1]?.[x - 1] },
    { y: y - 1, x: x + 0, v: arr[y - 1]?.[x + 0] },
    { y: y - 1, x: x + 1, v: arr[y - 1]?.[x + 1] },
  ];
};

/**
 * Return the eight neigboring spots and gets the value or undefined with the
 * getter function.
 */
export const get8NGeneric = <T>(
  /** Row or X coordinate */
  x: number,
  /** Column or Y coordinate */
  y: number,
  getter: (x: number, y: number) => T | undefined,
): { x: number; y: number; v: T | undefined }[] => {
  return [
    { x: x + 1, y: y + 0, v: getter(x + 1, y + 0) },
    { x: x + 1, y: y + 1, v: getter(x + 1, y + 1) },
    { x: x + 0, y: y + 1, v: getter(x + 0, y + 1) },
    { x: x - 1, y: y + 1, v: getter(x - 1, y + 1) },
    { x: x - 1, y: y + 0, v: getter(x - 1, y + 0) },
    { x: x - 1, y: y - 1, v: getter(x - 1, y - 1) },
    { x: x + 0, y: y - 1, v: getter(x + 0, y - 1) },
    { x: x + 1, y: y - 1, v: getter(x + 1, y - 1) },
  ];
};
