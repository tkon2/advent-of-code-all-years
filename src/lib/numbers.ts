import { tn } from './functions';

/**
 * Split a string by anything that isn't a number
 * and convert to a list of numbers
 */
export const splitTN = (str: string, includeNegatives: boolean = false) => {
  if (includeNegatives) {
    return str
      .split(/[^-\d]+/)
      .filter((v) => v.replaceAll('-', '').length)
      .map(tn);
  }
  return str
    .split(/[^\d]+/)
    .filter((v) => v.length)
    .map(tn);
};

/** Rounds to the given numbers of decimals */
export const roundTo = (number: number, digits: number) => {
  const mult = tn(`1${'0'.repeat(digits)}`);
  return Math.round(number * mult) / mult;
};
