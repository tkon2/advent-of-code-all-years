/** A logger that only logs a new value when it is different from all previous */
export const makeUniqueLogger = (log: (...args: any[]) => void) => {
  const alreadyPrinted = new Set<string>();
  return (toLog: string) => {
    if (alreadyPrinted.has(toLog)) return;
    alreadyPrinted.add(toLog);
    log(toLog);
  };
};
