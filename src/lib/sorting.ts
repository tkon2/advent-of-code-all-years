/** Sort array in [1,2,3] or [a,b,c] order */
export const asc = <T extends string | number>(a: T, b: T) => {
  if (typeof a === 'string' && typeof b === 'string') {
    return a.localeCompare(b);
  }
  return (a as number) - (b as number);
};

/** Sort array in [3,2,1] or [c,b,a] order */
export const desc = <T extends string | number>(a: T, b: T) => {
  if (typeof a === 'string' && typeof b === 'string') {
    return b.localeCompare(a);
  }
  return (b as number) - (a as number);
};
