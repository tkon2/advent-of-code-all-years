/** String containing all letters of the alphabet in order
 *
 * Use .toUpperCase for uppercase, and .split('') for arrays
 */
export const alphabet = 'abcdefghijklmnopqrstuvwxyz';
