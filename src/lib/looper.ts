/**
 * Take some part of an array and optionally run a function on each element
 * @param arr Input array
 * @param start Start index
 * @param step Step size
 * @param end End index
 * @returns Function to map the array
 */
export const take = <T extends string | number>(
  arr: T[],
  start: number = 0,
  step: number = 1,
  end: number | null = null,
) => {
  const foreach = (f: (v: T, i: number, c: number) => T = (v): T => v) => {
    if (step === 0) return [f(arr[Math.floor(start)], Math.floor(start), 1)];
    const actualEnd = end ?? arr.length;
    const dir = start < actualEnd ? 1 : -1;
    const actualStep = Math.abs(step) * dir;

    const value = [];

    let i = start;
    let loopCounter = 0;
    while (dir === 1 ? i <= actualEnd : i >= actualEnd) {
      if (i < 0 || i >= arr.length) break;
      value.push(f(arr[Math.floor(i)], Math.floor(i), loopCounter));
      i += actualStep;
      loopCounter += 1;
    }
    return value;
  };
  return foreach;
};
