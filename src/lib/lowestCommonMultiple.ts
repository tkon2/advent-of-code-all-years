/*

Taken from here:
https://github.com/TheAlgorithms/TypeScript/blob/master/maths/lowest_common_multiple.ts


*/

export const binaryGCF = (a: number, b: number): number => {
  if (!Number.isInteger(a) || !Number.isInteger(b) || a < 0 || b < 0) {
    throw new Error('numbers must be natural to determine factors');
  }

  while (b) {
    [a, b] = [b, a % b];
  }
  return a;
};

export const greatestCommonFactor = (nums: number[]): number => {
  if (nums.length === 0) {
    throw new Error('at least one number must be passed in');
  }

  return nums.reduce(binaryGCF);
};

export const binaryLCM = (a: number, b: number): number => {
  if (a < 0 || b < 0) {
    throw new Error(
      'numbers must be positive to determine lowest common multiple',
    );
  }

  if (!Number.isInteger(a) || !Number.isInteger(b)) {
    throw new Error(
      'this method, which utilizes GCF, requires natural numbers.',
    );
  }

  return (a * b) / greatestCommonFactor([a, b]);
};

export const lowestCommonMultiple = (nums: number[]): number => {
  if (nums.length === 0) {
    throw new Error('at least one number must be passed in');
  }

  return nums.reduce(binaryLCM);
};

export const lcm = lowestCommonMultiple;
