/** Shorter log statements */
export const log = console.log;

export const overlap = (
  a: number[] | [number | number],
  b: number[] | [number | number],
) => {
  if (a.length < 2) return false;
  if (b.length < 2) return false;
  if (a[0] <= b[1] && a[1] >= b[0]) return true;
  return false;
};

/** Checks the string or array for duplicates */
export const hasDuplicate = (v: string | (string | number)[]) => {
  return new Set(v).size !== v.length;
};

/**
 * Parse string and find first number
 * @param v String value to parse
 * @returns Parsed number or null if none found
 */
export const toNum = (v: string): number | null => {
  try {
    const val = parseInt(v.replace(/^[^-\d]*/, ''));
    if (isNaN(val)) return null;
    return val;
  } catch (e) {
    return null;
  }
};

/**
 * Parse string and find first number
 * @param v String value to parse
 * @param d Default number if none found
 * @returns Parsed number or default if none found
 */
export const toNumNice = (v: string, d: number = 0): number => {
  try {
    const val = parseInt(v.replace(/^[^-\d]*/, ''));
    if (isNaN(val)) return d;
    return val;
  } catch (e) {
    return d;
  }
};

/** Convert first number in string to an integer number or throw an error */
export const toNumStrict = (v: string): number => {
  try {
    const val = parseInt(v.replace(/^[^-\d]*/, ''));
    if (isNaN(val)) throw new Error();
    return val;
  } catch (e) {
    throw new Error('Could not parse number in string "' + v + '"');
  }
};

// short version, 'cause we gotta go fast
export const tn = toNumStrict;

export const toFloat = (v: string): number | null => {
  return parseFloat(v.replace(/^[^-\d]*/, '')) ?? null;
};
