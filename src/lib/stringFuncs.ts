/**
 * Adds the given char (0 by default) to the left side of the string until it is
 * the given length. If the string is longer than the length, the string is not
 * altered
 */
export const leftPad = (
  str: string | number,
  count: number,
  character = '0',
) => {
  return character.repeat(Math.max(0, count - `${str}`.length)) + str;
};

/**
 * Adds the given char (0 by default) to the right side of the string until it is
 * the given length. If the string is longer than the length, the string is not
 * altered
 */
export const rightPad = (
  str: string | number,
  count: number,
  character = '0',
) => {
  return str + character.repeat(Math.max(0, count - `${str}`.length));
};
