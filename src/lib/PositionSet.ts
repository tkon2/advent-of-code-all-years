import { toNumStrict } from './functions';
import { Position, Position3 } from './position';

/** Wrapper around Set to work with position data */
export class PositionSet<T extends Position | Position3> {
  private intern = new Set<string>();

  constructor(p: Set<string> | T[] = []) {
    if (p instanceof Set) {
      this.intern = p;
    } else {
      this.intern = new Set(p.map((v) => v.join(',')));
    }
  }

  add = (p: T) => {
    this.intern.add(p.join(','));
    return this;
  };

  has = (p: T) => {
    return this.intern.has(p.join(','));
  };

  delete = (p: T) => {
    return this.intern.delete(p.join(','));
  };

  get size() {
    return this.intern.size;
  }

  forEach(f: (v: T, i: string) => void) {
    this.intern.forEach((iv, i) => {
      const value = iv.split(',').map(toNumStrict) as T;
      f(value, i);
    });
  }

  map<K>(f: (v: T, i: string) => K) {
    const out: K[] = [];
    this.intern.forEach((iv, i) => {
      const value = iv.split(',').map(toNumStrict) as T;
      out.push(f(value, i));
    });
    return out;
  }

  toArray() {
    return this.map((v) => v);
  }

  get raw() {
    return this.intern;
  }

  set raw(newVal: Set<string>) {
    this.intern = newVal;
  }

  copy() {
    const newSet = new PositionSet<T>();
    newSet.raw = new Set<string>([...this.intern]);
    return newSet;
  }
}
