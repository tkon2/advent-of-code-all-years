import { log } from './functions';
import { equal, Position } from './position';

export type Entry = { pos: Position; cost: number; path: Position[] };

/**
 * Calculate neighbours of current position in search
 * @param board Board of number type
 * @param current Current position to calculate neighbours for
 * @returns List of new entries
 */
const getNeighbours = (board: number[][], current: Entry) => {
  const [x, y] = current.pos;
  const val = board[x][y];
  let ns: Entry[] = [];
  for (let dx = -1; dx <= 1; dx += 1) {
    for (let dy = -1; dy <= 1; dy += 1) {
      // Avoid corners
      if (Math.abs(dx) === Math.abs(dy)) continue;
      // Avoid self
      if (dx === 0 && dy === 0) continue;
      // Avoid edges
      if (x + dx >= board.length) continue;
      if (y + dy >= board[0].length) continue;
      if (x + dx < 0) continue;
      if (y + dy < 0) continue;

      const pos: Position = [x + dx, y + dy];

      // Can't go up more than 1 elevation
      if (board[pos[0]][pos[1]] - val <= 1) {
        ns.push({
          pos,
          cost: current.cost + 1,
          path: [...current.path, pos],
        });
      }
    }
  }
  return ns;
};

/**
 * Calculate optimal path from any starts to goal using breadth first search
 * @param board Board of number type
 * @param s Starting points
 * @param g Goal position
 * @param gn GetNeighbours function
 * @returns Entry object with total value, path, and final position
 */
export const bfs = (
  board: number[][],
  s: Position[],
  g: Position,
  gn: typeof getNeighbours = getNeighbours,
) => {
  let open: Entry[] = s.map((v) => ({ pos: v, cost: 0, path: [v] }));
  let closed: Entry[] = [];

  while (open.length) {
    open.sort((a, b) => b.cost - a.cost);
    const current = open.pop();
    if (!current) continue;

    if (equal(current.pos, g)) {
      return current;
    } else {
      closed.push(current);
    }
    const neighbours = gn(board, current);
    neighbours.forEach((n) => {
      if (closed.some((c) => equal(c.pos, n.pos))) return;
      let valSet = false;
      open = open.map((o) => {
        if (equal(o.pos, n.pos)) {
          valSet = true;
          return o.cost < n.cost ? o : n;
        }
        return o;
      });
      if (!valSet) {
        open.push(n);
      }
    });
  }
  return null;
};

/**
 * Prints board (converting to characters) with a given path overlaid
 * @param board Board of number
 * @param path Path to show
 */
export const printBoard = (board: number[][], path: Position[] = []) => {
  const localBoard: (number | string)[][] = board.map((b) =>
    b.map((v) => String.fromCharCode('a'.charCodeAt(0) + v)),
  ); // Copy board
  path.forEach((p) => {
    localBoard[p[0]][p[1]] = '@';
  });
  log(board);
  for (let x = 0; x < localBoard.length; x++) {
    let line = '';
    for (let y = 0; y < localBoard[0].length; y++) {
      line += localBoard[x][y];
    }
    log(line);
  }
};
