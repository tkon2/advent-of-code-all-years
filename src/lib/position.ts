/** Array of length two representing x and y position */
export type Position = [number, number];

/** Array of length three representing x, y, and z positions */
export type Position3 = [number, number, number];

/** Get difference between two positions */
export const diff = (a: Position, b: Position): Position => {
  return [a[0] - b[0], a[1] - b[1]];
};

/** Check if two positions are equal */
export const equal = (a: Position, b: Position): boolean => {
  return a[0] === b[0] && a[1] === b[1];
};

export const addDelta = <T extends Position | Position3>(p: T, d: T): T => {
  return p.map((v, i) => v + d[i]) as T;
};

/** Directions for non-diagonal relative positions */
export const crossDirs: Position[] = [
  [1, 0],
  [-1, 0],
  [0, 1],
  [0, -1],
];

/** Directions for non-diagonal relative positions in 3D space */
export const crossDirs3: Position3[] = [
  [1, 0, 0],
  [-1, 0, 0],
  [0, 1, 0],
  [0, -1, 0],
  [0, 0, 1],
  [0, 0, -1],
];
