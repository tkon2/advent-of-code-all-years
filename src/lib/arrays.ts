import { log } from './functions';

/** Sum numbers in array, automatically converts strings */
export const sum = (arr: (string | number)[]) => {
  return arr.reduce<number>(
    (c, v) => c + (typeof v === 'number' ? v : parseInt(v)),
    0,
  );
};

/** Create an array of numbers from start to end, or from 0 to start if end is not supplied */

/**
 * Creates an array from start to end, or between 0 and start if end is not supplied.
 * Additionally, providing a negative start and no end gives a reversed array.
 *
 * Examples:
 *
 * ```
 * range(-3) // [2,1,0]
 * range(2,4) // [2,3]
 * range(5) // [0,1,2,3,4]
 * range(-2,2) // [-2,-1,0,1]
 * range(2,-2) // [1,0,-1,-2]
 * ```
 *
 * To create an empty array of length N:
 * ```range(N).map(v => null)```
 *
 * Notice that the higher value is never included, while the lower is always included
 *
 * @param start Start or length
 * @param end End or undefined
 * @returns Array of numbers from start to end
 */
export const range = (start: number, end?: number) => {
  if (end === undefined) {
    if (start < 0) {
      start = Math.abs(start);
      end = 0;
    } else {
      end = start;
      start = 0;
    }
  }

  const arr: number[] = [];
  const dir = start < end ? 1 : -1;
  let i = start;
  while (i !== end) {
    arr.push(i + Math.min(0, dir));
    i += dir;
  }

  return arr;
};

export const chunk = <T extends number | string>(
  arr: T[],
  size: number,
): T[][] => {
  const out: T[][] = [[]];
  let outI = 0;
  for (let i = 0; i < arr.length; i++) {
    if (i > 0 && i % size === 0) {
      outI += 1;
      out[outI] = [];
    }
    out[outI].push(arr[i]);
  }
  return out;
};
