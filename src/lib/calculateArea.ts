/**
 * Calculates the total area within a list of points.
 * The points must be ordered either clockwise or counter-clockwise
 * for this to work.
 * The end does not have to connect back to the start.
 *
 * If it does, the final distance is essentially ignored.
 *
 * If it doesn't, it assumes the shape closes at the end.
 *
 * Assumes input is `[Y,X][]` (or `[C,R][]`)
 */
export const calculateArea = (arr: [number, number][]) => {
  // Initialize area
  let area = 0;

  let n = arr.length;

  // Calculate value of shoelace formula
  let j = n - 1;
  for (let i = 0; i < n; i++) {
    area += (arr[j][1] + arr[i][1]) * (arr[j][0] - arr[i][0]);

    // j is previous vertex to i
    j = i;
  }

  // Return absolute value
  return Math.abs(area / 2);
};
