const chars = new Map<string, string[]>([
  ['E', ['####', '#   ', '### ', '#   ', '#   ', '####']],
  ['G', [' ## ', '#  #', '#   ', '# ##', '#  #', ' ###']],
  ['J', ['  ##', '   #', '   #', '   #', '#  #', ' ## ']],
  ['B', ['### ', '#  #', '### ', '#  #', '#  #', '### ']],
  ['G', [' ## ', '#  #', '#   ', '# ##', '#  #', ' ###']],
  ['C', [' ## ', '#  #', '#   ', '#   ', '#  #', ' ## ']],
  ['F', ['####', '#   ', '### ', '#   ', '#   ', '#   ']],
  ['K', ['#  #', '# # ', '##  ', '# # ', '# # ', '#  #']],
]);

export const toBig = (char: string) => {
  return chars.get(char);
};

export const fromBig = (big: string[]) => {
  let small: string | null = null;
  if (big.length !== 6) return null;
  if (big.some((b) => b.length !== 4)) return null;
  chars.forEach((val, key) => {
    if (small != null) return;
    if (val.every((l, i) => l === big[i])) {
      small = key;
    }
  });
  return small;
};
