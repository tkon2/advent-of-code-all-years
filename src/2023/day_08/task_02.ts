import { Solution } from '../../types/solution';
import { lcm } from '../../lib/lowestCommonMultiple';

export const solve: Solution = async (lines, { log }) => {
  let total = 0;

  const moves = lines[0];

  const instructions = lines.slice(2);

  const inst = new Map(
    instructions.map((i) => {
      const [key, value] = i.split(' = ');
      const fixedVal = value.replaceAll(/[()]/g, '').split(', ');
      return [key, fixedVal];
    }),
  );

  let currents = instructions
    .filter((l) => l.slice(2, 3) === 'A')
    .map((l) => l.split(' = ')[0]);

  let i = 0;

  let times = currents.map(() => 0);

  while (times.some((t) => t === 0)) {
    if (total % 1000000 === 0) log(total, currents);
    const dir = moves[i];
    i = (i + 1) % moves.length;
    currents = currents.map((c, ci): string => {
      if (c.endsWith('Z')) times[ci] = total;
      const [l, r] = inst.get(c) ?? [];
      if (dir == 'L') return l;
      else return r;
    });
    total += 1;
  }

  log(times);

  return lcm(times);
};

export const correctOutput = 13129439557681;
