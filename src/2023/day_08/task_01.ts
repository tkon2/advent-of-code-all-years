import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  let total = 0;

  const moves = lines[0];

  const instructions = lines.slice(2);

  let current = instructions.findIndex((l) => l.startsWith('AAA'));

  let i = 0;

  while (!instructions[current].startsWith('ZZZ')) {
    total += 1;
    const dir = moves[i];
    i = (i + 1) % moves.length;
    const [l, r] = instructions[current]
      .split(' = ')[1]
      .replaceAll(/[)(]/g, '')
      .split(', ');
    log([dir, l, r]);
    if (dir == 'L') current = instructions.findIndex((x) => x.startsWith(l));
    if (dir == 'R') current = instructions.findIndex((x) => x.startsWith(r));
  }

  return total;
};

export const correctOutput = 13771;
