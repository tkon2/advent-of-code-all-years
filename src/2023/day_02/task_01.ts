import { Solution } from '../../types/solution';
import { tn } from '../../lib/functions';

const max = { red: 12, green: 13, blue: 14 };

export const solve: Solution = async (data) => {
  let idTotal = 0;
  outer: for (let d of data) {
    const [game, colors] = d.split(': ');
    const cols = colors.split(/[,;] /);

    for (let c of cols) {
      if (c.includes('red') && tn(c) > max.red) continue outer;
      if (c.includes('green') && tn(c) > max.green) continue outer;
      if (c.includes('blue') && tn(c) > max.blue) continue outer;
    }
    idTotal += tn(game);
  }
  return idTotal;
};

export const correctOutput = 2164;
