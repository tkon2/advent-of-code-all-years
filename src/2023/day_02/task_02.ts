import { Solution } from '../../types/solution';
import { tn } from '../../lib/functions';

export const solve: Solution = async (data) => {
  let sum = 0;
  for (let d of data) {
    const max = { red: 0, green: 0, blue: 0 };

    const [, colors] = d.split(': ');
    const cols = colors.split(/[,;] /);

    for (let c of cols) {
      const v = tn(c);
      if (c.includes('red') && v > max.red) max.red = v;
      if (c.includes('green') && v > max.green) max.green = v;
      if (c.includes('blue') && v > max.blue) max.blue = v;
    }

    sum += max.red * max.green * max.blue;
  }
  return sum;
};

export const correctOutput = 69929;
