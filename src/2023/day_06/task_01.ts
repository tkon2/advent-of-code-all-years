import { Solution } from '../../types/solution';
import { tn } from '../../lib/functions';

export const solve: Solution = async (lines, { log }) => {
  const times = lines[0].split(/: +/)[1].split(/ +/).map(tn);
  const distances = lines[1].split(/: +/)[1].split(/ +/).map(tn);

  log(times, distances);

  let total = 1;

  for (let i = 0; i < times.length; i++) {
    const time = times[i];
    const distance = distances[i];

    log({ time, distance });
    let wonRounds = 0;
    for (let r = 1; r < time; r++) {
      if (r * (time - r) > distance) {
        log(r, 'is winning');
        wonRounds++;
      }
    }
    log(wonRounds);
    total *= wonRounds;
  }

  log('Correct:');
  log(2374848);
  return total;
};

export const correctOutput = 2374848;
