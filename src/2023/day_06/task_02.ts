import { Solution } from '../../types/solution';
import { tn } from '../../lib/functions';

export const solve: Solution = async (lines, { log }) => {
  const times = lines[0].split(/: +/)[1].split(/ +/);
  const distances = lines[1].split(/: +/)[1].split(/ +/);

  const time = tn(times.join(''));
  const distance = tn(distances.join(''));

  log({ time, distance });
  let wonRounds = 0;
  for (let r = 1; r < time; r++) {
    if (r * (time - r) > distance) {
      wonRounds++;
    }
  }

  log('Correct:');
  log(39132886);
  return wonRounds;
};

export const correctOutput = 39132886;
