import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

export const solve: Solution = async (rows, { log }) => {
  let total = 0;

  for (let r = 0; r < rows.length; r++) {
    const row = rows[r];

    const [rawsprings, rawpattern] = row.split(' ');
    const springs = [rawsprings].join('?');
    const pattern = [rawpattern].join(',').split(',').map(tn);
    log(springs);

    const found = new Map<string, number>();
    const f = (i: number, patternI: number, current: number): number => {
      const key = `${i}-${patternI}-${current}`;
      if (found.has(key)) return found.get(key) as number;
      // At end of spring string
      if (i == springs.length) {
        // Ran out of pattern
        if (patternI >= pattern.length && current === 0) return 1;
        // At the end of the final pattern
        else if (
          patternI === pattern.length - 1 &&
          pattern[patternI] === current
        )
          return 1;
        // Still in the middle of a pattern
        else return 0;
      }
      let ans = 0;
      if (springs[i] !== '#' && current === 0) {
        // Try not starting the pattern
        ans += f(i + 1, patternI, 0);
      }
      if (
        springs[i] !== '#' &&
        current > 0 &&
        patternI < pattern.length &&
        pattern[patternI] == current
      ) {
        // Try ending a finished pattern
        ans += f(i + 1, patternI + 1, 0);
      }
      if (springs[i] !== '.') {
        // Try starting or continuing a pattern
        ans += f(i + 1, patternI, current + 1);
      }
      found.set(key, ans);
      return ans;
    };
    total += f(0, 0, 0);
  }

  return total;
};

export const correctOutput = 7084;
