import { Solution } from '../../types/solution';

export const solve: Solution = async (rows, { log }) => {
  let total = 0;

  const lines = rows[0].split(',');

  for (let l = 0; l < lines.length; l++) {
    let current = 0;
    const line = lines[l];
    for (let c = 0; c < line.length; c++) {
      const char = line[c];
      current += char.charCodeAt(0);
      current *= 17;
      current %= 256;
    }
    log(line, '=>', current);
    total += current;
  }

  return total;
};

export const correctOutput = 513172;
