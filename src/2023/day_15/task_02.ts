import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

export const solve: Solution = async (rows, { log }) => {
  let total = 0;

  const lines = rows[0].split(',');

  const boxes: Record<string, Record<string, number>> = {};

  for (let l = 0; l < lines.length; l++) {
    let current = 0;
    const line = lines[l];
    const [code, val] = line.split(/[=-]/);
    const isAdding = line.includes('=');
    for (let c = 0; c < code.length; c++) {
      const char = code[c];
      current += char.charCodeAt(0);
      current *= 17;
      current %= 256;
    }
    if (isAdding) {
      boxes[current] = { ...boxes[current], [code]: tn(val) };
    } else if (boxes?.[current]?.[code]) {
      delete boxes[current][code];
    }
  }

  log(boxes);

  Object.keys(boxes).forEach((bk) => {
    const box = boxes[bk];
    Object.keys(box).forEach((lk, i) => {
      const lense = box[lk];
      const boxNum = +bk + 1;
      const lenseNum = i + 1;
      total += lense * boxNum * lenseNum;
    });
  });

  return total;
};

export const correctOutput = 237806;
