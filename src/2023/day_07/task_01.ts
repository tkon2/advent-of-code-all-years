import { Solution } from '../../types/solution';
import { tn } from '../../lib/functions';

export const solve: Solution = async (lines, { log }) => {
  let total = 0;

  let hands: { hand: string; bid: number }[] = [];

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];

    const [hand, bid] = line.split(' ');

    hands.push({ hand: hand, bid: tn(bid) });
  }

  const cardVals = '123456789TJQKA'.split('');

  /** Get an object of number of matches to value */
  const getDups = (s: string): { strength: number; count: number } => {
    const found: Record<string, number> = {};
    s.split('').forEach((c) => {
      found[c] = (found[c] ?? 0) + 1;
    });
    let obj = { strength: 0, count: 0 };
    for (let [key, val] of Object.entries(found)) {
      if (val >= obj.count) {
        if (val !== obj.count) obj.strength = 0;
        obj.count = val;
        obj.strength += 1;
      }
    }
    if (obj.count > 3) obj.count += 1;
    if (obj.count === 3) {
      if ([...new Set(s.split(''))].length === 2) obj.count += 1;
    }
    return obj;
  };

  hands.sort((a, b) => {
    const ad = getDups(a.hand);
    const bd = getDups(b.hand);

    if (ad.count > bd.count) return 1;
    if (ad.count < bd.count) return -1;

    if (ad.strength > bd.strength) return 1;
    if (ad.strength < bd.strength) return -1;

    for (let i = 0; i < a.hand.length; i++) {
      const ac = a.hand[i];
      const bc = b.hand[i];
      if (cardVals.indexOf(ac) > cardVals.indexOf(bc)) return 1;
      if (cardVals.indexOf(ac) < cardVals.indexOf(bc)) return -1;
    }
    return 0;
  });

  log(
    hands
      .map((h) => ({ ...h, ...getDups(h.hand) }))
      .map((o) => `${o.hand} ${o.bid} : ${o.count} * ${o.strength}`)
      .join('\n'),
  );

  hands.forEach((h, i) => {
    total += h.bid * (i + 1);
  });

  log('Correct:');
  log(249483956);
  return total;
};

export const correctOutput = 249483956;
