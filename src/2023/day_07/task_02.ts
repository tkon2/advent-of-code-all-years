import { Solution } from '../../types/solution';
import { tn } from '../../lib/functions';

export const solve: Solution = async (lines, { log }) => {
  let total = 0;

  let hands: { hand: string; bid: number }[] = [];

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l];

    const [hand, bid] = line.split(' ');

    hands.push({ hand: hand, bid: tn(bid) });
  }

  const cardVals = 'J123456789TQKA'.split('');

  /** Get an object of number of matches strength (number of matches) */
  const getDups = (s: string): { strength: number; count: number } => {
    const found: Record<string, number> = {};
    s.split('').forEach((c) => {
      found[c] = (found[c] ?? 0) + 1;
    });
    let obj = { strength: 0, count: 0 };
    for (let [key, val] of Object.entries(found)) {
      if (key === 'J') continue;
      if (val >= obj.count) {
        if (val !== obj.count) obj.strength = 0;
        obj.count = val;
        obj.strength += 1;
      }
    }

    const hasJoker = s.includes('J');
    if (hasJoker) {
      const jokerCount = s.split('').filter((c) => c === 'J').length;
      // Joker rules..
      obj.count = obj.count + jokerCount;
      obj.strength = 1; // Because it's a new category
    }
    if (obj.count > 3) obj.count += 1;
    if (obj.count === 3) {
      // Don't count jokers
      if ([...new Set(s.split('').filter((v) => v !== 'J'))].length === 2) {
        obj.count += 1;
      }
    }
    return obj;
  };

  hands.sort((a, b) => {
    const ad = getDups(a.hand);
    const bd = getDups(b.hand);

    if (ad.count > bd.count) return 1;
    if (ad.count < bd.count) return -1;

    if (ad.strength > bd.strength) return 1;
    if (ad.strength < bd.strength) return -1;

    for (let i = 0; i < a.hand.length; i++) {
      const ac = a.hand[i];
      const bc = b.hand[i];
      if (cardVals.indexOf(ac) > cardVals.indexOf(bc)) return 1;
      if (cardVals.indexOf(ac) < cardVals.indexOf(bc)) return -1;
    }
    return 0;
  });

  log(
    hands
      .map((h, i) => ({ ...h, ...getDups(h.hand) }))
      .map((o) => `${o.hand} ${o.bid} : ${o.count} * ${o.strength}`)
      .join('\n'),
  );

  hands.forEach((h, i) => {
    total += h.bid * (i + 1);
  });

  log('Correct:');
  log(252137472);
  return total;
};

export const correctOutput = 252137472;
