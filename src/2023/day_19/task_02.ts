import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

type Field = 'x' | 'm' | 'a' | 's';

type Range = [number, number];

type Rule = {
  field: Field;
  operator: '<' | '>';
  value: number;
  next: string;
};

type Ruleset = {
  rules: Rule[];
  default: string;
};

export const solve: Solution = async (rows, { log }) => {
  let rules = new Map<string, Ruleset>();

  for (let r = 0; r < rows.length; r++) {
    const row = rows[r];
    if (row.trim() === '') break;

    const [id, rest] = row.split('{');
    const ruleStr = rest.replace('}', '');
    const ruleRules = ruleStr.split(',');
    const ruleObj: Ruleset = {
      rules: ruleRules.slice(0, -1).map((r) => {
        const [def, next] = r.split(':');
        const operator = def.includes('<') ? '<' : '>';
        const [field, value] = def.split(/<|>/);
        return { field, operator, value: tn(value), next } as Rule;
      }),
      default: ruleRules[ruleRules.length - 1],
    };
    rules.set(id, ruleObj);
  }

  /** Recursively find solutions */
  const getSol = (v: Record<Field, Range>, currentRuleId: string): number => {
    if (v.x[1] < v.x[0]) return 0;
    if (v.m[1] < v.m[0]) return 0;
    if (v.a[1] < v.a[0]) return 0;
    if (v.s[1] < v.s[0]) return 0;
    if (currentRuleId === 'R') return 0;
    if (currentRuleId === 'A') {
      const x = v.x[1] - v.x[0] + 1;
      const m = v.m[1] - v.m[0] + 1;
      const a = v.a[1] - v.a[0] + 1;
      const s = v.s[1] - v.s[0] + 1;
      return x * m * a * s;
    }

    /** CurrentRule */
    const cr = rules.get(currentRuleId);
    if (!cr) throw new Error('Current rule not found: ' + currentRuleId);

    /** CurrentValues */
    let cv = { ...v };
    let total = 0;

    for (let r of cr.rules) {
      if (r.operator === '<') {
        total += getSol(
          {
            ...cv,
            [r.field]: [cv[r.field][0], r.value - 1],
          },
          r.next,
        );
        cv[r.field] = [r.value, cv[r.field][1]];
      }
      if (r.operator === '>') {
        total += getSol(
          {
            ...cv,
            [r.field]: [r.value + 1, cv[r.field][1]],
          },
          r.next,
        );
        cv[r.field] = [cv[r.field][0], r.value];
      }
    }
    total += getSol(cv, cr.default);
    return total;
  };

  return getSol(
    {
      x: [1, 4000],
      m: [1, 4000],
      a: [1, 4000],
      s: [1, 4000],
    },
    'in',
  );
};

export const correctOutput = 130291480568730;
