import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

type Field = 'x' | 'm' | 'a' | 's';

type Rule = {
  field: Field;
  operator: '<' | '>';
  value: number;
  next: string;
};

type Ruleset = {
  rules: Rule[];
  default: string;
};

type Item = Record<Field, number>;

export const solve: Solution = async (rows, { log }) => {
  let total = 0;

  let rules = new Map<string, Ruleset>();
  let items: Item[] = [];

  let takingItems = false;
  for (let r = 0; r < rows.length; r++) {
    const row = rows[r];
    if (!takingItems && row.trim() === '') {
      takingItems = true;
      continue;
    }
    if (takingItems) {
      const data = row.replaceAll(/\{|\}/g, '');
      const fields = data.split(',').map((d) => d.split('='));
      const newItem: Item = {
        x: 0,
        m: 0,
        a: 0,
        s: 0,
      };
      fields.forEach(([key, value]) => {
        newItem[key as Field] = tn(value);
      });
      items.push(newItem);
    } else {
      const [id, rest] = row.split('{');
      const ruleStr = rest.replace('}', '');
      const ruleRules = ruleStr.split(',');
      const ruleObj: Ruleset = {
        rules: ruleRules.slice(0, -1).map((r) => {
          const [def, next] = r.split(':');
          const operator = def.includes('<') ? '<' : '>';
          const [field, value] = def.split(/<|>/);
          return { field, operator, value: tn(value), next } as Rule;
        }),
        default: ruleRules[ruleRules.length - 1],
      };
      rules.set(id, ruleObj);
    }
  }

  for (let item of items) {
    log();
    log('Starting new item:', item);
    let currentRuleId = 'in';
    while (true) {
      if (currentRuleId === 'R') break;
      if (currentRuleId === 'A') {
        total += item.a + item.m + item.s + item.x;
        log('Adding to total:', item.a + item.m + item.s + item.x);
        break;
      }
      const currentRule = rules.get(currentRuleId);
      if (!currentRule) throw new Error('No rule found');
      let ruleSet = false;
      for (let r of currentRule.rules) {
        const startStr = [
          'Checking',
          r.field + ':',
          item[r.field],
          r.operator,
          r.value,
          '=',
        ];
        if (r.operator === '>' && item[r.field] > r.value) {
          currentRuleId = r.next;
          if (r.next === 'R') {
            log(...startStr, true, '- REJECTED');
          } else if (r.next === 'A') {
            log(...startStr, true, '- ACCEPTED');
          } else {
            log(...startStr, true, '- going to', r.next);
          }
          ruleSet = true;
          break;
        } else if (r.operator === '<' && item[r.field] < r.value) {
          currentRuleId = r.next;
          if (r.next === 'R') {
            log(...startStr, true, '- REJECTED');
          } else if (r.next === 'A') {
            log(...startStr, true, '- ACCEPTED');
          } else {
            log(...startStr, true, '- going to', r.next);
          }
          ruleSet = true;
          break;
        } else {
          log(...startStr, false);
        }
      }
      if (!ruleSet) {
        const startStr = 'No other rules matched, using default -';
        if (currentRule.default === 'R') {
          log(startStr, 'REJECTED');
        } else if (currentRule.default === 'A') {
          log(startStr, 'ACCEPTED');
        } else {
          log(startStr, currentRule.default);
        }
        currentRuleId = currentRule.default;
      }
    }
  }

  return total;
};

export const correctOutput = 402185;
