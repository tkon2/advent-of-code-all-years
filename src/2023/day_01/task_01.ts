import { Solution } from '../../types/solution';
import { sum } from '../../lib/arrays';

export const solve: Solution = async (data) => {
  return sum(
    data.map((d): number => {
      const first = d.replace(/^.*?(\d).*/, '$1');
      const second = d.replace(/^.*(\d).*/, '$1');
      return +(first + second);
    }),
  );
};

export const correctOutput = 54990;
