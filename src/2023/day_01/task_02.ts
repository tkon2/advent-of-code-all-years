import { Solution } from '../../types/solution';
import { sum } from '../../lib/arrays';

const toNum = (str: string) => {
  switch (str) {
    case 'one':
      return 1;
    case 'two':
      return 2;
    case 'three':
      return 3;
    case 'four':
      return 4;
    case 'five':
      return 5;
    case 'six':
      return 6;
    case 'seven':
      return 7;
    case 'eight':
      return 8;
    case 'nine':
      return 9;
    default:
      return +str;
  }
};

export const solve: Solution = async (data) => {
  return sum(
    data.map((d): number => {
      const first = d.replace(
        /^.*?(\d|one|two|three|four|five|six|seven|eight|nine|zero).*/,
        '$1',
      );
      const second = d.replace(
        /^.*(\d|one|two|three|four|five|six|seven|eight|nine|zero).*/,
        '$1',
      );
      return +`${toNum(first)}${toNum(second)}`;
    }),
  );
};

export const correctOutput = 54473;
