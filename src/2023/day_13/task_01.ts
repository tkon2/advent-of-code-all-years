import { rotate } from '../../lib/grids';
import { Solution } from '../../types/solution';

export const solve: Solution = async (rows, { log }) => {
  let total = 0;

  const s = (arr: string[]) => {
    log();
    log(arr.join('\n'));
    log();
    for (let r = 0; r < arr.length - 1; r++) {
      const row = arr[r];
      log(row);

      let match = true;
      for (let c = 0; r + c + 1 < arr.length && r - c >= 0 && match; c++) {
        log(arr[r + c + 1], ' | ', arr[r - c], arr[r + c + 1] === arr[r - c]);
        if (arr[r + c + 1] !== arr[r - c]) match = false;
      }
      if (match) {
        return r + 1;
      }
    }
    return null;
  };

  const findS = (arr: string[][]) => {
    const ans = s(arr.map((a) => a.join('')));
    log('ANSWER 1', ans);
    if (ans) return 100 * ans;
    else {
      let flipped = rotate(arr);
      const ans2 = s(flipped.map((f) => f.join('')));
      log('ANSWER 2', ans2);
      if (ans2) return ans2;
    }
    throw new Error('No solution found');
  };

  let arr: string[] = [];
  for (let r = 0; r < rows.length; r++) {
    if (rows[r] === '') {
      total += findS(arr.map((a) => a.split('')));
      arr = [];
      continue;
    }
    arr.push(rows[r]);
  }
  total += findS(arr.map((a) => a.split('')));

  return total;
};

export const correctOutput = 35232;
