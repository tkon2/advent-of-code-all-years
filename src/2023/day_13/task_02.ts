import { rotateStr } from '../../lib/grids';
import { Solution } from '../../types/solution';

export const solve: Solution = async (rows, { log }) => {
  let total = 0;

  const s = (arr: string[]) => {
    log();
    log(arr.join('\n'));
    log();
    for (let r = 0; r < arr.length - 1; r++) {
      let smudges = 0;
      for (
        let c = 0;
        r + c + 1 < arr.length && r - c >= 0 && smudges < 2;
        c++
      ) {
        const first = arr[r + c + 1];
        const second = arr[r - c];
        if (first === second) {
          log(first, ' | ', second, 'smudges:', smudges);
          continue;
        }
        for (let i = 0; i < first.length && smudges < 2; i++) {
          if (first[i] === second[i]) continue;
          smudges += 1;
        }
        log(first, ' | ', second, 'smudges:', smudges);
      }
      if (smudges === 1) {
        return r + 1;
      }
    }
    return null;
  };

  const findS = (arr: string[]) => {
    const ans = s(arr);
    log('ANSWER 1', ans);
    if (ans) return 100 * ans;
    else {
      const flipped = rotateStr(arr);
      const ans2 = s(flipped);
      log('ANSWER 2', ans2);
      if (ans2) return ans2;
    }
    throw new Error('No solution found');
  };

  let arr: string[] = [];
  for (let r = 0; r < rows.length; r++) {
    if (rows[r] === '') {
      total += findS(arr);
      arr = [];
      continue;
    }
    arr.push(rows[r]);
  }
  total += findS(arr);

  return total;
};

export const correctOutput = 37982;
