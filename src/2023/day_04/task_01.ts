import { Solution } from '../../types/solution';

export const solve: Solution = async (data) => {
  let total = 0;
  for (let i = 0; i < data.length; i++) {
    const line = data[i];
    const list = line.replace(/^.*: */, '');
    const [wants, haves] = list.split(/ +\| +/).map((p) => p.split(/ +/));
    const matches = haves.filter((h) => wants.includes(h)).length;
    if (matches <= 1) total += matches;
    else total += Math.pow(2, matches - 1);
  }
  return total;
};

export const correctOutput = 25174;
