import { Solution } from '../../types/solution';
import { tn } from '../../lib/functions';
import { sum } from '../../lib/arrays';

export const solve: Solution = async (data) => {
  /** Multiplier for each card */
  let mult: Record<number, number> = {};

  for (let i = 0; i < data.length; i++) {
    // Adding the original card
    mult[i] = (mult[i] ?? 0) + 1;

    const line = data[i];
    const list = line.replace(/^.*: */, '');
    const [wants, haves] = list.split(/ +\| +/).map((p) => p.split(/ +/));
    const matches = haves.filter((h) => wants.includes(h)).length;

    // Insert new extras
    for (let j = i + 1; j < i + 1 + matches; j++) {
      mult[j] = (mult[j] ?? 0) + mult[i];
    }
  }

  const values = Object.keys(mult).map((k) => mult[tn(k)]);
  const corr = values.slice(0, data.length);
  return sum(corr);
};

export const correctOutput = 6420979;
