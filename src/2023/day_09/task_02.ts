import { Solution } from '../../types/solution';
import { tn } from '../../lib/functions';

export const solve: Solution = async (lines, { log }) => {
  let total = 0;

  for (let l = 0; l < lines.length; l++) {
    const line = lines[l].split(' ').map(tn);

    let solutions: number[][] = [line];
    while (solutions[solutions.length - 1].some((c) => c !== 0)) {
      const sol = solutions[solutions.length - 1];
      const nextSol: number[] = [];
      for (let i = 0; i < sol.length - 1; i++) {
        nextSol.push(sol[i + 1] - sol[i]);
      }
      solutions.push(nextSol);
    }

    // Reverse the sequence
    solutions = solutions.map((s) => s.toReversed());

    // Adding back up
    for (let i = solutions.length - 2; i >= 0; i--) {
      // Subtraction rather than addition
      solutions[i].push(
        solutions[i][solutions[i].length - 1] -
          solutions[i + 1][solutions[i + 1].length - 1],
      );
    }
    log(solutions.map((s) => s.join(', ')).join('\n'));
    log();
    const num = solutions[0][solutions[0].length - 1];
    total += num;
  }

  return total;
};

export const correctOutput = 905;
