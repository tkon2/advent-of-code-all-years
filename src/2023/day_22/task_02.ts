import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';
import { Position3 } from '../../lib/position';

type Brick = {
  id: number;
  /** Width */
  w: number;
  /** Depth */
  d: number;
  /** Height */
  h: number;
  /** Position */
  p: Position3;
};

const pointInBrick = (p: Position3, b: Brick) => {
  if (p[0] < b.p[0]) return false;
  if (p[1] < b.p[1]) return false;
  if (p[2] < b.p[2]) return false;
  if (p[0] > b.p[0] + b.w) return false;
  if (p[1] > b.p[1] + b.d) return false;
  if (p[2] > b.p[2] + b.h) return false;
  return true;
};

const pointInBrick2D = (
  p: Position3,
  b: Brick,
  dim1: 0 | 1 | 2,
  dim2: 0 | 1 | 2,
) => {
  if (p[dim1] < b.p[dim1]) return false;
  if (p[dim2] < b.p[dim2]) return false;
  const bd = [b.w, b.d, b.h];
  if (p[dim1] > b.p[dim1] + bd[dim1]) return false;
  if (p[dim2] > b.p[dim2] + bd[dim2]) return false;
  return true;
};

const identities = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split(
  '',
);

const show = (log: (...v: any[]) => void, b: Brick[], target?: Position3) => {
  const maxX = Math.max(...b.map((b) => b.p[0] + b.w)) + 1;
  const maxY = Math.max(...b.map((b) => b.p[1] + b.d)) + 1;
  const maxZ = Math.max(...b.map((b) => b.p[2] + b.h)) + 1;
  for (let z = maxZ; z >= 0; z--) {
    for (let y = maxY; y >= 0; y--) {
      let line = ' '.repeat(y);
      for (let x = 0; x < maxX; x++) {
        const xB = b.findIndex((bb) => pointInBrick([x, y, z], bb));
        if (target && target[0] === x && target[1] === y && target[2] === z)
          line += '@';
        else if (xB >= 0) line += identities[xB % identities.length];
        else if (z === 0) line += '=';
        else line += '.';
      }
      if (Math.floor(maxY / 2) - 1 === y)
        line += '  ' + `${z}`.padStart(3, '0');
      log(line);
    }
  }
};

export const solve: Solution = async (rows, { log, logging }) => {
  let total = 0;

  const bricks: Brick[] = [];

  for (let r = 0; r < rows.length; r++) {
    const row = rows[r];
    const [start, end] = row.split('~');
    const [sx, sy, sz] = start.split(',').map(tn);
    const [ex, ey, ez] = end.split(',').map(tn);

    const w = ex - sx;
    const d = ey - sy;
    const h = ez - sz;

    const p: Position3 = [sx, sy, sz];

    bricks.push({ id: r, w, d, h, p });
  }

  bricks.sort((a, b) => a.p[2] - b.p[2]);

  const drop = (original: Brick[]) => {
    const pool = original.map((o): Brick => ({ ...o, p: [...o.p] }));
    pool.sort((a, b) => a.p[2] - b.p[2]);
    const outp: Brick[] = [];

    while (pool.length) {
      const b = pool.shift()!;
      if (b.p[2] === 1) {
        outp.push(b);
        continue;
      }
      const applies = outp.filter((r) => {
        for (let x = 0; x <= b.w; x++) {
          for (let y = 0; y <= b.d; y++) {
            if (pointInBrick2D([b.p[0] + x, b.p[1] + y, 0], r, 0, 1))
              return true;
          }
        }
        return false;
      });
      b.p[2] = Math.max(0, ...applies.map((a) => a.p[2] + a.h)) + 1;
      outp.push(b);
    }
    return outp;
  };

  const dropCount = (original: Brick[]) => {
    let count = 0;
    const pool = original.map((o) => ({ ...o }));
    pool.sort((a, b) => a.p[2] - b.p[2]);
    const outp: Brick[] = [];

    while (pool.length) {
      const b = pool.shift()!;
      if (b.p[2] === 1) {
        outp.push(b);
        continue;
      }
      const applies = outp.filter((r) => {
        for (let x = 0; x <= b.w; x++) {
          for (let y = 0; y <= b.d; y++) {
            if (pointInBrick2D([b.p[0] + x, b.p[1] + y, 0], r, 0, 1))
              return true;
          }
        }
        return false;
      });
      const newZ = Math.max(0, ...applies.map((a) => a.p[2] + a.h)) + 1;
      if (b.p[2] !== newZ) count += 1;
      b.p[2] = newZ;
      outp.push(b);
    }
    return count;
  };

  const resting = drop(bricks);

  show(log, resting);

  for (let i = 0; i < resting.length; i++) {
    const arr = [...resting.slice(0, i), ...resting.slice(i + 1)];
    arr.sort((a, b) => a.p[2] - b.p[2]);
    if (resting.length !== arr.length + 1) throw new Error('Wrong!');
    const newArr = drop(arr);
    let newCount = 0;
    if (arr.length !== newArr.length) throw new Error('Mismatch');
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].p[2] !== newArr[i].p[2]) newCount += 1;
      if (arr[i].id !== newArr[i].id) throw new Error('Order changed');
    }
    if (newCount > 0) {
      log(identities[i % identities.length], newCount);
    }
    total += newCount;
  }

  log('Too high:', 78488);
  return total;
};

export const correctOutput = 64714;
