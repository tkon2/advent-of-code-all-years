import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';
import { Position3 } from '../../lib/position';

type Brick = {
  /** Width */
  w: number;
  /** Depth */
  d: number;
  /** Height */
  h: number;
  /** Position */
  p: Position3;
};

const pointInBrick2D = (
  p: Position3,
  b: Brick,
  dim1: 0 | 1 | 2,
  dim2: 0 | 1 | 2,
) => {
  if (p[dim1] < b.p[dim1]) return false;
  if (p[dim2] < b.p[dim2]) return false;
  const bd = [b.w, b.d, b.h];
  if (p[dim1] > b.p[dim1] + bd[dim1]) return false;
  if (p[dim2] > b.p[dim2] + bd[dim2]) return false;
  return true;
};

const identities = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split(
  '',
);

const show = (log: (...v: any[]) => void, b: Brick[]) => {
  const maxX = Math.max(...b.map((b) => b.p[0] + b.w)) + 2;
  const maxY = Math.max(...b.map((b) => b.p[1] + b.d)) + 2;
  const maxZ = Math.max(...b.map((b) => b.p[2] + b.h)) + 2;
  let fw = 0;
  let sw = 0;
  for (let z = maxZ; z >= 0; z--) {
    let line = '';
    for (let x = -1; x < maxX; x++) {
      const xB = b.findIndex((bb) => pointInBrick2D([x, 0, z], bb, 0, 2));
      if (xB >= 0) line += identities[xB % identities.length];
      else if (z === 0) line += '=';
      else line += '.';
    }
    if (!fw) fw = line.length;
    line += ' ' + `${z}`.padStart(3, '0') + ' ';
    for (let y = -1; y < maxY; y++) {
      const yB = b.findIndex((bb) => pointInBrick2D([0, y, z], bb, 1, 2));
      if (yB >= 0) line += identities[yB % identities.length];
      else if (z === 0) line += '=';
      else line += '.';
    }
    if (!sw) sw = line.length - fw;
    log(line);
  }
  log(
    ' '.repeat(fw / 2) +
      'X' +
      ' '.repeat(fw / 2) +
      '  ' +
      ' '.repeat(sw / 2) +
      'Y',
  );
};

export const solve: Solution = async (rows, { log, logging }) => {
  let total = 0;

  const bricks: Brick[] = [];

  for (let r = 0; r < rows.length; r++) {
    const row = rows[r];
    const [start, end] = row.split('~');
    const [sx, sy, sz] = start.split(',').map(tn);
    const [ex, ey, ez] = end.split(',').map(tn);

    const w = ex - sx;
    const d = ey - sy;
    const h = ez - sz;

    const p: Position3 = [sx, sy, sz];

    bricks.push({ w, d, h, p });
  }

  bricks.sort((a, b) => a.p[2] - b.p[2]);

  const drop = (original: Brick[]) => {
    const pool = original.map((o) => ({ ...o }));
    pool.sort((a, b) => a.p[2] - b.p[2]);
    const outp: Brick[] = [];

    while (pool.length) {
      const b = pool.shift()!;
      if (b.p[2] === 1) {
        outp.push(b);
        continue;
      }
      const applies = outp.filter((r) => {
        for (let x = 0; x <= b.w; x++) {
          for (let y = 0; y <= b.d; y++) {
            if (pointInBrick2D([b.p[0] + x, b.p[1] + y, 0], r, 0, 1))
              return true;
          }
        }
        return false;
      });
      b.p[2] = Math.max(0, ...applies.map((a) => a.p[2] + a.h)) + 1;
      outp.push(b);
    }
    return outp;
  };

  const willDrop = (original: Brick[]) => {
    const pool = original.map((o) => ({ ...o }));
    pool.sort((a, b) => a.p[2] - b.p[2]);
    const outp: Brick[] = [];

    while (pool.length) {
      const b = pool.shift()!;
      if (b.p[2] === 1) {
        outp.push(b);
        continue;
      }
      const applies = outp.filter((r) => {
        for (let x = 0; x <= b.w; x++) {
          for (let y = 0; y <= b.d; y++) {
            if (pointInBrick2D([b.p[0] + x, b.p[1] + y, 0], r, 0, 1))
              return true;
          }
        }
        return false;
      });
      const newZ = Math.max(0, ...applies.map((a) => a.p[2] + a.h)) + 1;
      if (b.p[2] !== newZ) return true;
      outp.push(b);
    }
    return false;
  };

  const resting = drop(bricks);

  show(log, resting);

  for (let i = 0; i < resting.length; i++) {
    if (i % 100 === 0) log(i);
    const arr = [...resting.slice(0, i), ...resting.slice(i + 1)];
    if (!willDrop(arr)) total += 1;
  }

  return total;
};

export const correctOutput = 395;
