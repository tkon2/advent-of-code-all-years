import { PriorityQueue, Queue } from 'data-structure-typed';
import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';
import { intersect, number as mnum, MathNumericType } from 'mathjs';

type Stone = {
  id: number;
  x: number;
  y: number;
  z: number;
  /** Delta X */
  dx: number;
  /** Delta Y */
  dy: number;
  /** Delta Z */
  dz: number;
};

const tbn = (s: string): bigint => {
  if (!s) throw new Error('Could not parse bigint');
  const number = BigInt(s);
  if (Number.isNaN(number)) throw new Error('Could not parse bigint');
  return number;
};

export const solve: Solution = async (rows, { log, clip }) => {
  let total = 0;

  const searchMin = clip ? 7 : 200000000000000;
  const searchMax = clip ? 27 : 400000000000000;

  const willCollide = (a: Stone, b: Stone): boolean => {
    let a1 = [a.x, a.y];
    let a2 = [a1[0] + a.dx, a1[1] + a.dy];
    let b1 = [b.x, b.y];
    let b2 = [b1[0] + b.dx, b1[1] + b.dy];

    const value = intersect(a1, a2, b1, b2);
    if (!value) return false;
    const [x, y] = value;
    if (typeof x !== 'number') throw new Error('Type error');
    if (typeof y !== 'number') throw new Error('Type error');
    if (a.dx >= 0 && x < a.x) return false;
    if (b.dx >= 0 && x < b.x) return false;

    if (a.dy >= 0 && y < a.y) return false;
    if (b.dy >= 0 && y < b.y) return false;

    if (a.dx < 0 && x > a.x) return false;
    if (b.dx < 0 && x > b.x) return false;

    if (a.dy < 0 && y > a.y) return false;
    if (b.dy < 0 && y > b.y) return false;
    if (x < searchMin || x > searchMax) return false;
    if (y < searchMin || y > searchMax) return false;
    log(a, b, [x, y]);
    return true;
  };

  let stones: Stone[] = [];
  for (let r = 0; r < rows.length; r++) {
    const row = rows[r];
    const [pos, vel] = row.split(' @ ');
    const [x, y, z] = pos.split(', ').map(tn);
    const [dx, dy, dz] = vel.split(', ').map(tn);
    stones.push({ id: r, x, y, z, dx, dy, dz });
  }

  for (let i = 0; i < stones.length; i++) {
    for (let j = i + 1; j < stones.length; j++) {
      if (willCollide(stones[i], stones[j])) total += 1;
    }
  }

  return total;
};

export const correctOutput = 27328;
