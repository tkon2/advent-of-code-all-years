import { intersect } from 'mathjs';
import { splitTN } from '../../lib/numbers';
import { Position3 } from '../../lib/position';
import { Solution } from '../../types/solution';

type Stone = {
  pos: Position3;
  vel: Position3;
};

export const solve: Solution = async (lines, { log, clip }) => {
  let answer = 0;
  const hailStones: Stone[] = lines.map((l): Stone => {
    const [x, y, z, ...vel] = splitTN(l, true);
    return { pos: [x, y, z], vel: vel as Position3 };
  });

  const valueSpan = clip ? 10 : 300;

  outer: for (let sx = -valueSpan; sx < valueSpan; sx++) {
    log('Remaining iterations:', valueSpan * 2 - (valueSpan + sx));
    for (let sy = -valueSpan; sy < valueSpan; sy++) {
      for (let sz = -valueSpan; sz < valueSpan; sz++) {
        const offset = [sx, sy, sz];
        for (let i = 1; i < hailStones.length; i++) {
          /** HailStone */
          const { pos: posA1, vel: velA } = hailStones[i];
          const { pos: posB1, vel: velB } = hailStones[i - 1];
          const posA2 = posA1.map((v, i) => v + velA[i] - offset[i]);
          const posB2 = posB1.map((v, i) => v + velB[i] - offset[i]);
          const intersection = intersect(posA1, posA2, posB1, posB2);
          if (intersection === null) break;
          if (i < hailStones.length - 1) continue;
          const [ix, iy, iz] = intersection;
          if (typeof ix !== 'number') throw new Error('Type error');
          if (typeof iy !== 'number') throw new Error('Type error');
          if (typeof iz !== 'number') throw new Error('Type error');
          log(i - 1, lines[i - 1]);
          log(i, lines[i]);
          log('Best guess', intersection, '@', offset);
          answer = ix + iy + iz;
          log('Answer:', answer);
          break outer;
        }
      }
    }
  }
  if (clip) {
    log('Should be', [24, 13, 10], '@', [-3, 1, 2]);
  } else {
    log(
      'Should be',
      [349084334634500, 252498326441926, 121393830576313.98],
      '@',
      [-125, 25, 272],
    );
  }

  return answer;
};

export const correctOutput = 722976491652740;
