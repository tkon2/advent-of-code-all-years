/** Item in the queue */
export type QueueItem = {
  /** X Position */
  x: number;
  /** Y Position */
  y: number;
  /** Heading */
  h: number;
  /** StraightCount */
  sc: number;
  /** Total cost so far */
  total: number;
  /** History of the current item */
  history?: { x: number; y: number }[];
};
