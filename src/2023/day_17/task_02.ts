import { HashMap, PriorityQueue } from 'data-structure-typed';
import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';
import { QueueItem } from './types';

export const solve: Solution = async (rows, { log, logging }) => {
  // Precalc numeric values
  const goalX = rows[0].length - 1;
  const goalY = rows.length - 1;
  const numVals = rows.map((r) => r.split('').map(tn));

  // Set up data structures
  const seen = new HashMap<string, number>();
  let answer = Infinity;
  let pool = new PriorityQueue<QueueItem>(
    [
      {
        x: -1,
        y: 0,
        h: 0,
        sc: -1,
        total: -1 * (numVals[0][0] ?? 0),
        history: [],
      },
    ],
    { comparator: (a, b) => a.total - b.total },
  );

  while (!pool.isEmpty()) {
    // Get values
    let { x, y, h, sc, total, history } = pool.poll()!;

    // Check win
    if (x === goalX && y === goalY && total < answer && (sc >= 4 || sc === 0)) {
      if (logging) {
        log('WINNER:', answer, '->', total, '(', pool.size, 'states left)');
        for (let r = 0; r < rows.length; r++) {
          let line = '';
          for (let c = 0; c < rows[0].length; c++) {
            if (history?.some((hh) => hh.x === c && hh.y === r)) line += '#';
            else line += '.';
          }
          log(line);
        }
      }
      answer = total;
      continue;
    }

    // Update position
    switch (h) {
      case 0: {
        x += 1;
        break;
      }
      case 1: {
        y += 1;
        break;
      }
      case 2: {
        x -= 1;
        break;
      }
      case 3: {
        y -= 1;
        break;
      }
    }

    // Check bounds
    if (0 > x || x > goalX) continue;
    if (0 > y || y > goalY) continue;

    // Check if we are over the current best
    total += numVals[y][x] ?? 0;
    if (total >= answer) continue;

    for (const next of [
      { sc: sc + 1, h, turning: false },
      { sc: 0, h: (h + 1) % 4, turning: true },
      { sc: 0, h: (h + 3) % 4, turning: true },
    ]) {
      const turning = next.turning;
      // To be able to turn, sc must be at least 3
      if (turning && sc < 3) continue;
      // We can go max 10 steps without turning
      if (!turning && sc >= 9) continue;
      // Check if we've been here before
      const key = `${x},${y},${next.h},${next.sc}`;
      const s = seen.get(key);
      if (s && s <= total) continue;
      seen.set(key, total);
      pool.add({
        x,
        y,
        h: next.h,
        sc: next.sc,
        total,
        history: logging ? [...(history ?? []), { x, y }] : undefined,
      });
    }
  }

  const maxStates = rows.length * rows[0].length * 4 * (11 - 3);
  const checkedStates = seen.size;

  log('Maximum possible states: ', maxStates);
  log('Actual number of states: ', checkedStates);

  log(
    'Checked',
    `${((checkedStates / maxStates) * 100).toFixed(2)}%`,
    'of all states',
  );

  return answer;
};

export const correctOutput = 877;
