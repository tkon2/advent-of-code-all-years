import { Queue } from 'data-structure-typed';
import { Solution } from '../../types/solution';

type Signal = {
  target: string;
  value: boolean;
  source: string;
};

export const solve: Solution = async (rows, { log }) => {
  let totalOn = 0;
  let totalOff = 0;
  const modules = new Map<
    string,
    {
      type: string;
      targets: string[];
      ffState: boolean;
      accState: Record<string, boolean>;
    }
  >();

  for (let r = 0; r < rows.length; r++) {
    const row = rows[r];
    const [name, body] = row.split(' -> ');
    const targets = body.split(', ');
    let accState: Record<string, boolean> = {};
    if (name.startsWith('&')) {
      const found = rows.filter(
        (r) => !r.startsWith(name) && r.includes(name.slice(1)),
      );
      found.forEach((f) => {
        accState[f.split(' -> ')[0].slice(1)] = false;
      });
    }
    modules.set(name.slice(1), {
      type: name[0],
      targets,
      ffState: false,
      accState,
    });
  }

  // log(modules);

  const pushThatButton = () => {
    const pool = new Queue<Signal>([
      { target: 'roadcaster', value: false, source: 'button' },
    ]);
    while (!pool.isEmpty()) {
      const signal = pool.shift();
      if (!signal) throw new Error('NO ITEM');
      // log(signal)

      if (signal.value) totalOn++;
      else totalOff++;
      const target = modules.get(signal.target);
      // Assume it's an output node?
      if (!target) continue;
      // log(signal, target);

      if (target.type === 'b') {
        target.targets.forEach((t) => {
          pool.push({
            target: t,
            value: signal.value,
            source: signal.target,
          });
        });
        continue;
      }
      if (target.type === '%') {
        if (signal.value) continue;
        target.ffState = !target.ffState;
        target.targets.forEach((t) => {
          pool.push({
            target: t,
            value: target.ffState,
            source: signal.target,
          });
        });
        continue;
      }
      if (target.type === '&') {
        if (target.accState[signal.source] !== signal.value) {
          target.accState[signal.source] = signal.value;
        }
        // log("awdwdawdawdawdawddw", accState, Object.values(accState).reduce((acc, val) => acc && val, true));
        let val = true;
        if (
          Object.values(target.accState).reduce((acc, val) => acc && val, true)
        ) {
          val = false;
        }
        target.targets.forEach((t) => {
          pool.push({ target: t, value: val, source: signal.target });
        });

        continue;
      }

      throw new Error('Unhandled type');
    }
  };

  for (let i = 0; i < 1000; i++) {
    pushThatButton();
  }

  // log({ totalOn, totalOff })

  log(806332748);
  return totalOn * totalOff;
};

export const correctOutput = 806332748;
