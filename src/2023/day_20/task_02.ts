import { Queue } from 'data-structure-typed';
import { Solution } from '../../types/solution';
import { lcm } from '../../lib/lowestCommonMultiple';

type Signal = {
  target: string;
  value: boolean;
  source: string;
};

export const solve: Solution = async (rows, { log }) => {
  let total = 1;
  const modules = new Map<
    string,
    {
      type: string;
      targets: string[];
      ffState: boolean;
      accState: Record<string, boolean>;
    }
  >();

  for (let r = 0; r < rows.length; r++) {
    const row = rows[r];
    const [name, body] = row.split(' -> ');
    const targets = body.split(', ');
    let accState: Record<string, boolean> = {};
    if (name.startsWith('&')) {
      const found = rows.filter(
        (r) => !r.startsWith(name) && r.includes(name.slice(1)),
      );
      found.forEach((f) => {
        accState[f.split(' -> ')[0].slice(1)] = false;
      });
    }
    modules.set(name.slice(1), {
      type: name[0],
      targets,
      ffState: false,
      accState,
    });
  }

  const known: Record<string, number> = {};
  modules.forEach((v, k) => {
    if (v.targets.includes('xn')) {
      known[k] = 0;
    }
  });

  const pushThatButton = (count: number): boolean => {
    const pool = new Queue<Signal>([
      { target: 'roadcaster', value: false, source: 'button' },
    ]);
    while (!pool.isEmpty()) {
      const signal = pool.shift()!;

      if (signal.target === 'rx' && !signal.value) {
        log(signal);
        return true;
      }

      const target = modules.get(signal.target);
      // Assume it's an output node?
      if (!target) continue;
      if (
        count > 0 &&
        signal.target == 'xn' &&
        signal.value &&
        known[signal.source] === 0
      ) {
        // loop detected
        known[signal.source] = count;
        log(known);
        log(signal, target);
        if (Object.values(known).reduce<boolean>((a, v) => !!(a && v), true)) {
          return true;
        }
      }
      let { type, targets, ffState, accState } = target;

      if (type === 'b') {
        targets.forEach((t) => {
          pool.push({
            target: t,
            value: signal.value,
            source: signal.target,
          });
        });
        continue;
      }
      if (type === '%') {
        if (signal.value) continue;
        ffState = !ffState;
        modules.set(signal.target, { ...target, ffState });
        targets.forEach((t) => {
          pool.push({ target: t, value: ffState, source: signal.target });
        });
        continue;
      }
      if (type === '&') {
        if (accState[signal.source] !== signal.value) {
          accState[signal.source] = signal.value;
          modules.set(signal.target, { ...target, accState });
        }
        let val = true;
        if (Object.values(accState).reduce((acc, val) => acc && val, true)) {
          val = false;
        }
        targets.forEach((t) => {
          pool.push({ target: t, value: val, source: signal.target });
        });

        continue;
      }
      throw new Error('Unhandled type');
    }
    return false;
  };

  while (!pushThatButton(total)) {
    total++;
    if (total % 100000 === 0) log(total);
  }

  const lowest = lcm(Object.values(known));

  log('Too low', 131843326056);

  return lowest;
};

export const correctOutput = 228060006554227;
