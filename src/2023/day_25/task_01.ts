import { UndirectedGraph } from 'data-structure-typed';
import { Solution } from '../../types/solution';

export const solve: Solution = async (lines, { log }) => {
  const allNames = new Set<string>();
  const graph = new UndirectedGraph<string>();

  for (let line of lines) {
    const names = line.split(/:? /);
    names.forEach((n) => allNames.add(n));
  }

  for (let name of allNames) {
    graph.addVertex(name);
  }

  for (let line of lines) {
    const [source, ...rest] = line.split(/:? /);

    rest.forEach((d) => {
      graph.addEdge(source, d, 1);
    });
  }

  // Randomly select two points and find the path between them many many times.
  // Add all nodes used in the path to a total count for each edge.
  // The three most used edges are *probably* the ones we want to cut.
  const spotCount = new Map<string, number>();
  const nameList = [...allNames];
  for (let i = 0; i < Math.max(200, nameList.length); i++) {
    const a = Math.floor(Math.random() * nameList.length);
    const b = Math.floor(Math.random() * nameList.length);
    if (a === b) continue;
    const path = graph.getMinPathBetween(nameList[a], nameList[b], true);
    if (!path) throw new Error('No path found');
    for (let p = 1; p < path.length; p++) {
      const keys = [path[p - 1].key, path[p].key].sort((a, b) =>
        `${a}`.localeCompare(`${b}`),
      );
      const id = keys.join('-');
      spotCount.set(id, (spotCount.get(id) ?? 0) + 1);
    }
  }

  const toCut = [...spotCount]
    .sort((a, b) => b[1] - a[1])
    .slice(0, 3)
    .map((v) => v[0].split('-'));
  log('Edges to cut:', toCut);

  toCut.forEach(([a, b]) => {
    graph.deleteEdgeBetween(a, b);
  });

  const comparitor = toCut[0][0];
  const inLeft = new Set<string>();
  const inRight = new Set<string>();

  for (let i = 0; i < nameList.length; i++) {
    const pathCost = graph.getMinPathBetween(nameList[i], comparitor, true);

    // For some reason it still tries to find a path..
    if (pathCost?.[pathCost.length - 1].key === comparitor) {
      inRight.add(nameList[i]);
    } else {
      inLeft.add(nameList[i]);
    }
  }

  log(
    'Left: %d,',
    inLeft.size,
    'Right: %d,',
    inRight.size,
    'Total: %d',
    inLeft.size + inRight.size,
    'Original node count:',
    nameList.length,
  );

  return inLeft.size * inRight.size;
};

export const correctOutput = 598120;
