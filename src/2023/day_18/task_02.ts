import { calculateArea } from '../../lib/calculateArea';
import { Solution } from '../../types/solution';

export const solve: Solution = async (rows, { log }) => {
  const poses: [number, number][] = [];
  let current: [number, number] = [0, 0];
  let permimiter = 0;
  poses.push([...current]);

  for (let r = 0; r < rows.length; r++) {
    const row = rows[r];

    let [, , hash] = row.split(' ');
    hash = hash.replaceAll(/[()]/g, '');
    const rawDist = hash.split('').slice(0, -1).join('');
    const len = parseInt(rawDist.replace('#', ''), 16);
    const dir = hash.split('').slice(-1).join('');
    log({ len, dir });
    let dx = 0;
    let dy = 0;
    if (dir == '0') dx = 1 * len;
    else if (dir == '1') dy = 1 * len;
    else if (dir == '2') dx = -1 * len;
    else if (dir == '3') dy = -1 * len;
    else throw new Error('Dir out of bounds');
    permimiter += len;

    current[0] += dy;
    current[1] += dx;

    poses.push([...current]);
  }

  log(poses);

  const ans = calculateArea(poses) + permimiter / 2 + 1;

  return ans;
};

export const correctOutput = 48797603984357;
