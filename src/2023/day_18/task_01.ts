import { Queue } from 'data-structure-typed';
import { tn } from '../../lib/functions';
import { Solution } from '../../types/solution';

export const solve: Solution = async (rows, { log }) => {
  const taken = new Set<string>();
  let current = [0, 0];
  taken.add(current.join(','));

  for (let r = 0; r < rows.length; r++) {
    const row = rows[r];

    const [dir, len] = row.split(' ');
    let dx = 0;
    let dy = 0;
    if (dir == 'R') dx = 1;
    if (dir == 'D') dy = 1;
    if (dir == 'L') dx = -1;
    if (dir == 'U') dy = -1;

    for (let i = 0; i < tn(len); i++) {
      current[0] += dy;
      current[1] += dx;
      taken.add(current.join(','));
    }
  }

  const arr = [...taken].map((t) => t.split(',').map(tn));
  const minX = Math.min(...arr.map((r) => r[1]));
  const minY = Math.min(...arr.map((r) => r[0]));
  const maxX = Math.max(...arr.map((r) => r[1]));
  const maxY = Math.max(...arr.map((r) => r[0]));

  for (let r = minY; r <= maxY; r++) {
    let line = '';
    for (let c = minX; c <= maxX; c++) {
      if (r === 1 && c === 1) line += '#';
      else if (taken.has([r, c].join(','))) line += '.';
      else line += ' ';
    }
  }

  let pool = new Queue([{ r: 1, c: 1 }]);
  while (!pool.isEmpty()) {
    const { r, c } = pool.shift()!;
    const neigbours = [
      { r: r + 1, c: c + 0 },
      { r: r - 1, c: c + 0 },
      { r: r + 0, c: c + 1 },
      { r: r + 0, c: c - 1 },
    ];
    neigbours.forEach((n) => {
      if (taken.has(`${n.c},${n.r}`)) return;
      taken.add(`${n.c},${n.r}`);
      pool.push(n);
    });
  }

  for (let r = minY; r <= maxY; r++) {
    let line = '';
    for (let c = minX; c <= maxX; c++) {
      if (r === 1 && c === 1) line += '#';
      else if (taken.has([r, c].join(','))) line += '.';
      else line += ' ';
    }
    log(line);
  }

  return taken.size;
};

export const correctOutput = 36807;
