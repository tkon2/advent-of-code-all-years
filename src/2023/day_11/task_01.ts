import { PositionSet } from '../../lib/PositionSet';
import { rotate } from '../../lib/grids';
import { Position } from '../../lib/position';
import { Solution } from '../../types/solution';

export const solve: Solution = async (rows, { log }) => {
  let total = 0;
  const spaced = rows.flatMap((r) => (r.includes('#') ? [r] : [r, r]));

  // Flip array to check other side
  const flipped = rotate(spaced.map((r) => r.split('')));
  const starmap = flipped.flatMap((r) => (r.includes('#') ? [r] : [r, r]));

  const pos = new PositionSet<Position>();

  // Add stars
  for (let r = 0; r < starmap.length; r++) {
    for (let c = 0; c < starmap[r].length; c++) {
      if (starmap[r][c] === '#') pos.add([r, c]);
    }
  }

  const positions = pos.toArray();

  // Get combinations
  for (let i = 0; i < positions.length; i++) {
    for (let j = i + 1; j < positions.length; j++) {
      const distance =
        Math.abs(positions[i][0] - positions[j][0]) +
        Math.abs(positions[i][1] - positions[j][1]);
      log({ ii: i + 1, i: positions[i], ji: j + 1, j: positions[j], distance });
      total += distance;
    }
  }

  return total;
};

export const correctOutput = 9639160;
