import { PositionSet } from '../../lib/PositionSet';
import { rotate } from '../../lib/grids';
import { Position } from '../../lib/position';
import { Solution } from '../../types/solution';

const expansion = 1000000;

export const solve: Solution = async (rows, { log }) => {
  let total = 0;
  const spaceRows: number[] = [];
  const spaceCols: number[] = [];

  rows.forEach((l, i) => {
    if (l.includes('#')) return;
    spaceRows.push(i);
  });

  // Flip array to check other side
  const flipped = rotate(rows.map((r) => r.split('')));

  flipped.forEach((l, i) => {
    if (l.includes('#')) return;
    spaceCols.push(i);
  });

  log({ spaceCols: spaceRows, spaceRows: spaceCols });

  const pos = new PositionSet<Position>();

  // Add stars
  for (let r = 0; r < rows.length; r++) {
    for (let c = 0; c < rows[r].length; c++) {
      if (rows[r][c] === '#') pos.add([r, c]);
    }
  }

  const positions = pos.toArray();

  // Get combinations
  for (let i = 0; i < positions.length; i++) {
    for (let j = i + 1; j < positions.length; j++) {
      const x1 = positions[i][0];
      const x2 = positions[j][0];
      const y1 = positions[i][1];
      const y2 = positions[j][1];
      const startX = Math.min(x1, x2);
      const startY = Math.min(y1, y2);
      const endX = Math.max(x1, x2);
      const endY = Math.max(y1, y2);

      const rowCount = spaceRows.filter((s) => s < endX && s > startX).length;
      const colCount = spaceCols.filter((s) => s < endY && s > startY).length;

      const distance =
        Math.abs(x1 - x2) +
        Math.abs(y1 - y2) +
        (rowCount * expansion - rowCount) +
        (colCount * expansion - colCount);
      log({ ii: i + 1, i: positions[i], ji: j + 1, j: positions[j], distance });
      total += distance;
    }
  }

  return total;
};

export const correctOutput = 752936133304;
