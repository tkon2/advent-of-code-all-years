import { PriorityQueue } from 'data-structure-typed';
import { Solution } from '../../types/solution';

export const solve: Solution = async (rows, { log }) => {
  let total = 0;

  const sR = 0;
  const sC = rows[sR].split('').indexOf('.');

  const gR = rows.length - 1;
  const gC = rows[gR].split('').indexOf('.');

  const pool = new PriorityQueue(
    [{ r: sR, c: sC, steps: 0, visited: new Set<string>() }],
    { comparator: (a, b) => b.steps - a.steps },
  );

  while (!pool.isEmpty()) {
    const current = pool.poll()!;
    if (current.c === gC && current.r === gR)
      total = Math.max(current.steps, total);
    const visited = current.visited;
    for (let d of [
      [0, 1],
      [0, -1],
      [1, 0],
      [-1, 0],
    ] as const) {
      let r = current.r + d[0];
      let c = current.c + d[1];
      let tile = rows[r]?.[c];
      if (tile === undefined) continue;
      if (tile === '#') continue;
      let key = [r, c].join(',');
      if (visited.has(key)) continue;
      visited.add(key);
      if (tile === '.') {
        pool.add({
          r,
          c,
          steps: current.steps + 1,
          visited: new Set([...visited]),
        });
        continue;
      }
      let newD: [number, number] = [0, 0];
      if (tile === '>') newD = [0, 1];
      else if (tile === 'v') newD = [1, 0];
      else if (tile === '<') newD = [0, -1];
      else if (tile === '^') newD = [-1, 0];
      if (newD[0] === 0 && newD[1] === 0) continue;

      r = r + newD[0];
      c = c + newD[1];
      key = [r, c].join(',');
      tile = rows[r]?.[c];
      if (tile === undefined) continue;
      if (tile === '#') continue;
      if (visited.has(key)) continue;
      visited.add(key);
      pool.add({
        r,
        c,
        steps: current.steps + 2,
        visited: new Set([...visited]),
      });
    }
  }

  log([sR, sC], [gR, gC]);

  return total;
};

export const correctOutput = 2306;
