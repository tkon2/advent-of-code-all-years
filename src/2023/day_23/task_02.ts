import { UndirectedGraph } from 'data-structure-typed';
import { Solution } from '../../types/solution';

const makeId = (i: number, j: number) => `${i},${j}`;

export const solve: Solution = async (lines, { log }) => {
  let longest = 0;
  let start: string = '0,0';
  let end: string = '0,0';
  let allPoints: string[] = [];

  const pathGraph = new UndirectedGraph<string>();

  lines.forEach((l, i) => {
    l.split('').forEach((c, j) => {
      const id = makeId(i, j);
      if (c === '#') return;
      if (i === 0) start = id;
      if (i === lines.length - 1) end = id;
      pathGraph.addVertex(id);
      allPoints.push(id);
    });
  });

  lines.forEach((l, i) => {
    if (i === 0) return;
    l.split('').forEach((_, j) => {
      if (j === 0) return;
      if (lines[i - 1][j] !== '#') {
        pathGraph.addEdge(makeId(i - 1, j), makeId(i, j), 1);
      }
      if (lines[i][j - 1] !== '#') {
        pathGraph.addEdge(makeId(i, j - 1), makeId(i, j), 1);
      }
    });
  });

  for (const point of allPoints) {
    const links = pathGraph.edgesOf(point);
    if (links.length !== 2) continue;
    const first = links[0].endpoints.find((e) => e !== point);
    const second = links[1].endpoints.find((e) => e !== point);
    if (!first || !second) throw new Error();
    const weight = links[0].weight + links[1].weight;
    pathGraph.deleteVertex(point);
    pathGraph.addEdge(first, second, weight);
  }

  const possiblePaths = pathGraph.getAllPathsBetween(start, end, 2000000);

  possiblePaths.forEach((path) => {
    const pathLength = pathGraph.getPathSumWeight(path);
    longest = Math.max(longest, pathLength);
  });

  return longest;
};

export const correctOutput = 6718;
