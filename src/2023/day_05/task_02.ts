import { Solution } from '../../types/solution';
import { tn } from '../../lib/functions';

export const solve: Solution = async (data, { log }) => {
  const conversions: [number, number, number][][] = [];
  let arr: [number, number, number][] = [];

  for (let i = 3; i < data.length; i++) {
    const line = data[i];
    if (line.includes(':')) {
      conversions.unshift(arr);
      arr = [];
    } else if (line.trim() !== '') {
      const [f, t, s] = data[i].split(' ').map((v) => tn(v));
      arr.push([f, t, s]);
    }
  }
  conversions.unshift(arr);

  /** Convert a position into a soil number */
  const convert = (pos: number): number => {
    let val = pos;
    conversions.forEach((c) => {
      for (const [from, to, steps] of c) {
        if (val >= from && val <= from + steps) {
          val = to + (val - from);
          break;
        }
      }
    });
    return val;
  };

  const rawseeds = data[0].split(': ')[1].split(' ');
  const seeds = rawseeds.map((s) => tn(s));

  const isSeed = (num: number) => {
    for (let s = 0; s < seeds.length - 1; s += 2) {
      if (seeds[s] < num && seeds[s] + seeds[s + 1] >= num) return true;
    }
    return false;
  };

  let lowest: number = 0;

  const ballpark = 10000;

  // Find the ballpark
  for (let i = 0; true; i += ballpark) {
    const seedOrigin = convert(i);
    if (isSeed(seedOrigin)) {
      lowest = i;
      break;
    }
  }

  // Find exact
  for (let i = lowest - ballpark - 1; i <= lowest; i++) {
    const seedOrigin = convert(i);
    if (isSeed(seedOrigin)) {
      return i;
    }
  }

  return null;
};

export const correctOutput = 20358599;
