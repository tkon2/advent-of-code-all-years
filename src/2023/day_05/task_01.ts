import { Solution } from '../../types/solution';
import { tn } from '../../lib/functions';

export const solve: Solution = async (data, { log }) => {
  const conversions: [number, number, number][][] = [];
  let arr: [number, number, number][] = [];

  for (let i = 3; i < data.length; i++) {
    const line = data[i];
    if (line.includes(':')) {
      conversions.push(arr);
      arr = [];
    } else if (line.trim() !== '') {
      arr.push(
        data[i].split(' ').map((v) => tn(v)) as [number, number, number],
      );
    }
  }
  conversions.push(arr);

  /** Convert a soil number into a position */
  const convert = (pos: number): number => {
    let val = pos;
    log('# Calculating for', val);
    conversions.forEach((c) => {
      for (const [to, from, steps] of c) {
        if (val >= from && val <= from + steps) {
          val = to + (val - from);
          break;
        }
      }
      log('Value:', val);
    });
    return val;
  };

  let minPos = Infinity;

  const rawseeds = data[0].split(': ')[1].split(' ');
  const seeds = rawseeds.map((s) => tn(s));

  for (let s = 0; s < seeds.length; s++) {
    const pos = convert(seeds[s]);
    if (pos < minPos) {
      minPos = pos;
    }
  }

  log('Correct:');
  log(31599214);
  log('Output:');
  return minPos;
};

export const correctOutput = 31599214;
