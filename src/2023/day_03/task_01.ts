import { Solution } from '../../types/solution';
import { tn } from '../../lib/functions';

const isNum = (n: string) => {
  return /\d/.exec(n) !== null;
};

export const solve: Solution = async (data) => {
  let total = 0;
  for (let i = 0; i < data.length; i++) {
    for (let j = 0; j < data[i].length; j++) {
      if (isNum(data[i][j])) {
        let valid = false;
        let num = '';
        // Check left
        if (j > 0) {
          if (data[i][j - 1] !== '.') valid = true;
        }
        // check diagonal left
        if (i > 0 && j > 0) {
          if (data[i - 1][j - 1] !== '.') valid = true;
        }
        if (i < data.length - 1 && j > 0) {
          if (data[i + 1][j - 1] !== '.') valid = true;
        }
        // Check current
        if (i > 0) {
          if (data[i - 1][j] !== '.') valid = true;
        }
        if (i < data.length - 1) {
          if (data[i + 1][j] !== '.') valid = true;
        }
        while (isNum(data[i][j])) {
          num += data[i][j];
          // Check next for each number in sequence
          if (i > 0 && j < data[i].length - 1) {
            if (data[i - 1][j + 1] !== '.') valid = true;
          }
          if (i < data.length - 1 && j < data[i].length - 1) {
            if (data[i + 1][j + 1] !== '.') valid = true;
          }
          j += 1;
        }
        // Check right (we've already moved one right)
        if (j < data[i].length - 1) {
          if (data[i][j] !== '.') valid = true;
        }
        if (valid) total += tn(num);
      }
    }
  }
  return total;
};

export const correctOutput = 546563;
