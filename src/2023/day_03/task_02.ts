import { Solution } from '../../types/solution';
import { tn } from '../../lib/functions';

const isNum = (n: string) => {
  if (!n) return false;
  return '0123456789'.includes(n);
};

type Found = {
  coords: [number, number];
  value: number;
};

export const solve: Solution = async (data) => {
  let total = 0;

  const find = (x: number, y: number): Found | null => {
    if (!isNum(data[x][y])) return null;

    // find start
    while (y > 0 && isNum(data[x][y - 1])) y -= 1;
    const startCoords = [x, y] as [number, number];

    let num = '0';
    while (isNum(data[x][y])) {
      num += data[x][y];
      if (y >= data[x].length - 1) break;
      y += 1;
    }
    return { coords: startCoords, value: tn(num) };
  };

  for (let i = 0; i < data.length; i++) {
    for (let j = 0; j < data[i].length; j++) {
      if (data[i][j] === '*') {
        const values: (Found | null)[] = [];
        // Left
        if (j > 0 && isNum(data[i][j - 1])) values.push(find(i, j - 1));
        // Right
        if (j < data[i].length - 1 && isNum(data[i][j + 1]))
          values.push(find(i, j + 1));
        // Up left
        if (i > 0 && j > 0 && isNum(data[i - 1][j - 1]))
          values.push(find(i - 1, j - 1));
        // Up middle
        if (i > 0 && isNum(data[i - 1][j])) values.push(find(i - 1, j));
        // Up right
        if (i > 0 && j < data[i].length && isNum(data[i - 1][j + 1]))
          values.push(find(i - 1, j + 1));
        // Down left
        if (i < data.length - 1 && j > 0 && isNum(data[i + 1][j - 1]))
          values.push(find(i + 1, j - 1));
        // Down middle
        if (i < data.length - 1 && isNum(data[i + 1][j]))
          values.push(find(i + 1, j));
        // Down right
        if (
          i < data.length - 1 &&
          j < data[i].length &&
          isNum(data[i + 1][j + 1])
        )
          values.push(find(i + 1, j + 1));
        const actualVals: Found[] = [];
        values.forEach((v) => {
          if (v === null) return;
          if (
            actualVals.some(
              (av) =>
                av.coords[0] === v.coords[0] && av.coords[1] === v.coords[1],
            )
          )
            return;
          actualVals.push(v);
        });
        if (actualVals.length > 1) {
          total += actualVals[0].value * actualVals[1].value;
        }
      }
    }
  }
  return total;
};

export const correctOutput = 91031374;
