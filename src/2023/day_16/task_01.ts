import { sleep } from 'bun';
import { Solution } from '../../types/solution';

export const solve: Solution = async (rows, { log, logging }) => {
  let rays: { x: number; y: number; h: number }[] = [{ x: 0, y: 0, h: 0 }];
  const visited = new Set<string>();
  const done = new Set<string>();
  const pickups = new Set<string>();

  while (rays.length) {
    const ray = rays.shift();
    if (!ray) continue;
    visited.add(`${ray.x}-${ray.y}`);

    const nextPos = {
      x: ray.x + (ray.h === 0 ? 1 : 0) + (ray.h === 2 ? -1 : 0),
      y: ray.y + (ray.h === 1 ? 1 : 0) + (ray.h === 3 ? -1 : 0),
      h: ray.h,
    };

    if (nextPos.x >= rows[0].length) continue;
    if (nextPos.y >= rows.length) continue;
    if (nextPos.x < 0) continue;
    if (nextPos.y < 0) continue;

    if (logging) {
      await sleep(70);
      log();
      for (let y = 0; y < rows.length; y++) {
        let line = '';
        for (let x = 0; x < rows[y].length; x++) {
          line += rows[y][x];
          if (x === nextPos.x && y === nextPos.y && nextPos.h === 0)
            line += '>';
          else if (x === nextPos.x && y === nextPos.y && nextPos.h === 1)
            line += 'V';
          else if (x === nextPos.x && y === nextPos.y && nextPos.h === 2)
            line += '<';
          else if (x === nextPos.x && y === nextPos.y && nextPos.h === 3)
            line += '^';
          else if (visited.has(`${x}-${y}`)) line += '#';
          else if (pickups.has(`${x}-${y}`)) line += '?';
          else line += ' ';
        }
        log(line);
      }
    }

    // handle loops etc
    const key = JSON.stringify(ray);
    if (done.has(key)) {
      log("We've already been here...");
      continue;
    }
    done.add(key);

    let nextChar = rows[nextPos.y][nextPos.x];
    const lc = rows[nextPos.y][nextPos.x - 1];
    const rc = rows[nextPos.y][nextPos.x + 1];

    if (nextChar === '/') {
      log('Hit a mirror /');
      if (nextPos.h === 0) rays.unshift({ ...nextPos, h: 3 });
      if (nextPos.h === 1) rays.unshift({ ...nextPos, h: 2 });
      if (nextPos.h === 2) rays.unshift({ ...nextPos, h: 1 });
      if (nextPos.h === 3) rays.unshift({ ...nextPos, h: 0 });
      continue;
    }

    if (nextChar === '\\') {
      log('Hit a mirror \\');
      if (nextPos.h === 0) rays.unshift({ ...nextPos, h: 1 });
      if (nextPos.h === 1) rays.unshift({ ...nextPos, h: 0 });
      if (nextPos.h === 2) rays.unshift({ ...nextPos, h: 3 });
      if (nextPos.h === 3) rays.unshift({ ...nextPos, h: 2 });
      continue;
    }

    if (nextChar === '-') {
      if (nextPos.h === 1 || nextPos.h === 3) {
        log('Hit a splitter -');
        rays.unshift({ ...nextPos, h: 2 });
        rays.push({ ...nextPos, h: 0 });
        if (logging) pickups.add(`${nextPos.x + 1}-${nextPos.y}`);
        continue;
      }
    }

    if (nextChar === '|') {
      if (nextPos.h === 0 || nextPos.h === 2) {
        log('Hit a splitter |');
        rays.unshift({ ...nextPos, h: 3 });
        rays.push({ ...nextPos, h: 1 });
        if (logging) pickups.add(`${nextPos.x}-${nextPos.y + 1}`);
        continue;
      }
    }
    log('Hit a dot');
    rays.unshift(nextPos);
  }

  return visited.size;
};

export const correctOutput = 7434;
