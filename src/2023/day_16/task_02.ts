import { sleep } from 'bun';
import { Solution } from '../../types/solution';

export const solve: Solution = async (rows, { log, logging }) => {
  let total = 0;

  const getSolution = async (x: number, y: number, h: number) => {
    let rays: { x: number; y: number; h: number }[] = [{ x, y, h }];
    const visited = new Set<string>();
    const done = new Set<string>();

    while (rays.length) {
      const ray = rays.shift();
      if (!ray) continue;
      visited.add(`${ray.x}-${ray.y}`);
      const key = `${ray.x},${ray.y},${ray.h}`;
      if (done.has(key)) continue;
      done.add(key);

      const nextPos = {
        x: ray.x + (ray.h === 0 ? 1 : 0) + (ray.h === 2 ? -1 : 0),
        y: ray.y + (ray.h === 1 ? 1 : 0) + (ray.h === 3 ? -1 : 0),
        h: ray.h,
      };

      if (nextPos.x >= rows[0].length) continue;
      if (nextPos.y >= rows.length) continue;
      if (nextPos.x < 0) continue;
      if (nextPos.y < 0) continue;

      let nextChar = rows[nextPos.y][nextPos.x];

      if (nextChar === '/') {
        if (nextPos.h === 0) rays.push({ ...nextPos, h: 3 });
        if (nextPos.h === 1) rays.push({ ...nextPos, h: 2 });
        if (nextPos.h === 2) rays.push({ ...nextPos, h: 1 });
        if (nextPos.h === 3) rays.push({ ...nextPos, h: 0 });
        continue;
      }

      if (nextChar === '\\') {
        if (nextPos.h === 0) rays.push({ ...nextPos, h: 1 });
        if (nextPos.h === 1) rays.push({ ...nextPos, h: 0 });
        if (nextPos.h === 2) rays.push({ ...nextPos, h: 3 });
        if (nextPos.h === 3) rays.push({ ...nextPos, h: 2 });
        continue;
      }

      if (nextChar === '-') {
        if (nextPos.h === 1 || nextPos.h === 3) {
          rays.push({ ...nextPos, h: 0 });
          rays.push({ ...nextPos, h: 2 });
          continue;
        }
      }

      if (nextChar === '|') {
        if (nextPos.h === 0 || nextPos.h === 2) {
          rays.push({ ...nextPos, h: 1 });
          rays.push({ ...nextPos, h: 3 });
          continue;
        }
      }
      rays.push(nextPos);
    }

    if (logging) {
      await sleep(500);
      log();
      for (let j = 0; j < rows.length; j++) {
        let line = '';
        for (let i = 0; i < rows[j].length; i++) {
          line += rows[j][i];
          if (i === x && j === y && h === 0) line += '>';
          else if (i === x && j === y && h === 1) line += 'V';
          else if (i === x && j === y && h === 2) line += '<';
          else if (i === x && j === y && h === 3) line += '^';
          else line += visited.has(`${i}-${j}`) ? '#' : ' ';
        }
        log(line);
      }
    }

    return visited.size;
  };

  if (logging) {
    // Top
    for (let j = 0; j < rows[0].length; j++) {
      total = Math.max(total, await getSolution(j, 0, 1));
    }

    // Right
    for (let i = 0; i < rows.length; i++) {
      total = Math.max(total, await getSolution(rows[0].length - 1, i, 2));
    }

    // Bottom rev
    for (let j = rows[0].length - 1; j >= 0; j--) {
      total = Math.max(total, await getSolution(j, rows.length - 1, 3));
    }

    // Left rev
    for (let i = rows.length - 1; i >= 0; i--) {
      total = Math.max(total, await getSolution(0, i, 0));
    }
  } else {
    for (let i = 0; i < Math.max(rows.length, rows[0].length); i++) {
      total = Math.max(total, await getSolution(i, 0, 1));
      total = Math.max(total, await getSolution(rows[0].length - 1, i, 2));
      total = Math.max(total, await getSolution(i, rows.length - 1, 3));
      total = Math.max(total, await getSolution(0, i, 0));
    }
  }

  return total;
};

export const correctOutput = 8183;
