import { Solution } from '../../types/solution';

export const solve: Solution = async (rawRows, { log }) => {
  let total = 0;

  log(rawRows.join('\n'), '\n');

  const getNext = (rows: string[]): string[] => {
    let flipped: string[] = [];
    for (let x = 0; x < rows[0].length; x++) {
      for (let y = 0; y < rows.length; y++) {
        flipped[x] = (flipped[x] ?? '') + rows[y][x];
      }
      flipped[x] = flipped[x].split('').toReversed().join('');
    }

    let shifted: string[] = [];
    for (let r = 0; r < flipped.length; r++) {
      let row = flipped[r].split('');

      for (let c = row.length - 1; c >= 0; c--) {
        const char = row[c];
        if (char !== 'O') continue;
        if (c >= row.length - 1 || row[c + 1] !== '.') {
          continue;
        }
        for (let i = c; i < row.length; i++) {
          if ((i >= row.length - 1 || row[i + 1] !== '.') && row[i] === '.') {
            row[c] = '.';
            row[i] = 'O';
            break;
          }
        }
      }
      shifted.push(row.join(''));
    }

    return shifted;
  };

  let r = rawRows;
  let count = 0;
  const desired = 1000000000 * 4;

  const known = new Map<string, string[]>();
  let skipped = false;

  let cycle: string[] = [];
  while (count++ < desired) {
    if (count === 5) log(1, 'Cycle:\n' + r.join('\n'), '\n');
    if (count === 9) log(2, 'Cycles:\n' + r.join('\n'), '\n');
    if (count === 13) log(3, 'Cycles:\n' + r.join('\n'), '\n');

    if (!skipped && cycle.some((c) => c === r.join('\n'))) {
      const skipBy = cycle.length - cycle.findIndex((c) => c === r.join('\n'));
      skipped = true;
      while (count < desired - skipBy) count += skipBy;
    }
    cycle.push(r.join('\n'));
    const k = known.get(r.join('\n'));
    if (k) {
      r = k;
      continue;
    }
    const key = r.join('\n');
    r = getNext(r);
    known.set(key, r);
  }

  log(desired / 4, 'Cycles:');
  for (let ri = 0; ri < r.length; ri++) {
    const row = r[ri];
    log(row);
    for (let c = 0; c < row.length; c++) {
      if (row[c] !== 'O') continue;
      total += r.length - ri;
    }
  }

  return total;
};

export const correctOutput = 87700;
