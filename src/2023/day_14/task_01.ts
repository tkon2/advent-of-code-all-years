import { Solution } from '../../types/solution';

export const solve: Solution = async (rows, { log }) => {
  let total = 0;

  log(rows.join('\n'), '\n');

  let flipped: string[] = [];
  for (let x = 0; x < rows[0].length; x++) {
    for (let y = 0; y < rows.length; y++) {
      flipped[x] = (flipped[x] ?? '') + rows[y][x];
    }
    flipped[x] = flipped[x].split('').toReversed().join('');
  }

  log(flipped.join('\n'), '\n');

  let shifted: string[] = [];
  for (let r = 0; r < flipped.length; r++) {
    let row = flipped[r].split('');

    for (let c = row.length - 1; c >= 0; c--) {
      const char = row[c];
      if (char !== 'O') continue;
      if (c >= row.length - 1 || row[c + 1] !== '.') {
        total += c + 1;
        continue;
      }
      for (let i = c; i < row.length; i++) {
        if ((i >= row.length - 1 || row[i + 1] !== '.') && row[i] === '.') {
          row[c] = '.';
          row[i] = 'O';
          total += i + 1;
          break;
        }
      }
    }
    shifted.push(row.join(''));
  }

  log(shifted.join('\n'));

  return total;
};

export const correctOutput = 106648;
