import { Solution } from '../../types/solution';
import { PositionSet } from '../../lib/PositionSet';
import { Position } from '../../lib/position';

export const solve: Solution = async (rows, { log, logging }) => {
  /** Row */
  const startingPosR = rows.findIndex((l) => l.includes('S'));
  log('Starting Row', startingPosR);
  /** Column */
  const startingPosC = rows[startingPosR].indexOf('S');
  log('Starting Col', startingPosC);

  const findNext = (prev: Position, pos: Position): Position | null => {
    if (pos[0] < 0 || pos[0] >= rows.length) return null;
    if (pos[1] < 0 || pos[1] >= rows[0].length) return null;
    const c = rows[pos[0]][pos[1]];
    // Up
    if (prev[0] === pos[0] + 1 && c === 'F') return [pos[0], pos[1] + 1];
    if (prev[0] === pos[0] + 1 && c === '|') return [pos[0] - 1, pos[1]];
    if (prev[0] === pos[0] + 1 && c === '7') return [pos[0], pos[1] - 1];

    // Down
    if (prev[0] === pos[0] - 1 && c === 'L') return [pos[0], pos[1] + 1];
    if (prev[0] === pos[0] - 1 && c === '|') return [pos[0] + 1, pos[1]];
    if (prev[0] === pos[0] - 1 && c === 'J') return [pos[0], pos[1] - 1];

    // Right
    if (prev[1] === pos[1] - 1 && c === 'J') return [pos[0] - 1, pos[1]];
    if (prev[1] === pos[1] - 1 && c === '-') return [pos[0], pos[1] + 1];
    if (prev[1] === pos[1] - 1 && c === '7') return [pos[0] + 1, pos[1]];

    // Left
    if (prev[1] === pos[1] + 1 && c === 'F') return [pos[0] + 1, pos[1]];
    if (prev[1] === pos[1] + 1 && c === '-') return [pos[0], pos[1] - 1];
    if (prev[1] === pos[1] + 1 && c === 'L') return [pos[0] - 1, pos[1]];

    return null;
  };

  // Need to keep track of up to four paths
  let currents = [
    findNext([startingPosR, startingPosC], [startingPosR + 1, startingPosC]),
    findNext([startingPosR, startingPosC], [startingPosR - 1, startingPosC]),
    findNext([startingPosR, startingPosC], [startingPosR, startingPosC + 1]),
    findNext([startingPosR, startingPosC], [startingPosR, startingPosC - 1]),
  ] as (Position | null)[];
  let prevs: (Position | null)[] = [
    [startingPosR + 1, startingPosC],
    [startingPosR - 1, startingPosC],
    [startingPosR, startingPosC + 1],
    [startingPosR, startingPosC - 1],
  ];

  let correct: number | null = null;

  while (!correct) {
    currents.forEach((current, i) => {
      if (current === null) return;
      if (rows[current[0]][current[1]] === 'S') {
        correct = i;
      }
      const prev = prevs[i];
      if (prev === null) return;
      const next = findNext(prev, current);
      currents[i] = next;
      prevs[i] = current;
    });

    if (currents.every((c) => c === null)) break;
  }

  if (!correct) throw new Error('Could not find path');

  let current = [
    findNext([startingPosR, startingPosC], [startingPosR + 1, startingPosC]),
    findNext([startingPosR, startingPosC], [startingPosR - 1, startingPosC]),
    findNext([startingPosR, startingPosC], [startingPosR, startingPosC + 1]),
    findNext([startingPosR, startingPosC], [startingPosR, startingPosC - 1]),
  ][correct];

  let previous: Position | null = [
    [startingPosR + 1, startingPosC] as Position,
    [startingPosR - 1, startingPosC] as Position,
    [startingPosR, startingPosC + 1] as Position,
    [startingPosR, startingPosC - 1] as Position,
  ][correct];

  let steps = 1;
  let poses: Position[] = [previous as Position];
  do {
    poses.push(current as Position);
    const next = findNext(previous, current as Position);
    previous = current as Position;
    current = next;
    steps += 1;
  } while (current !== null);

  const findSolution = (handedness: 1 | -1) => {
    let inside = new PositionSet<Position>();
    // Assuming the loop's handedness
    for (let i = 1; i < poses.length; i++) {
      const c = poses[i];
      const p = poses[i - 1];
      // Going left
      if (c[1] === p[1] + 1) {
        inside.add([c[0] + handedness, c[1] - 1]);
        inside.add([c[0] + handedness, c[1]]);
      }
      // Going right
      if (c[1] === p[1] - 1) {
        inside.add([c[0] + -handedness, c[1]]);
        inside.add([c[0] + -handedness, c[1] + 1]);
      }
      // Going up
      if (c[0] === p[0] + 1) {
        inside.add([c[0], c[1] + -handedness]);
        inside.add([c[0] - 1, c[1] + -handedness]);
      }
      // Going down
      if (c[0] === p[0] - 1) {
        inside.add([c[0] + 1, c[1] + handedness]);
        inside.add([c[0], c[1] + handedness]);
      }
    }

    const pathPoses = new PositionSet(poses);
    inside = new PositionSet(inside.toArray().filter((i) => !pathPoses.has(i)));

    // Flood fill found pos
    const potentials = inside.toArray();
    while (potentials.length) {
      const c = potentials.shift();
      if (!c) throw new Error('Ran out of potentials');
      for (let y = -1; y < 2; y++) {
        for (let x = -1; x < 2; x++) {
          const n = [c[0] + y, c[1] + x] as Position;
          // If the position runs out of bounds it is not the correct handedness
          if (n[0] < 0 || n[0] > rows.length) return null;
          if (n[1] < 0 || n[1] > rows[0].length) return null;
          if (pathPoses.has(n)) continue;
          if (!inside.has(n)) {
            potentials.push(n);
          }
          inside.add(n);
        }
      }
    }

    if (logging) {
      for (let r = 0; r < rows.length; r++) {
        let line = '';
        for (let c = 0; c < rows[r].length; c++) {
          if (poses.some((p) => p[0] === r && p[1] === c)) line += rows[r][c];
          else if (inside.has([r, c])) {
            line += '#';
          } else line += ' ';
        }
        log(line);
      }
    }

    inside = new PositionSet(inside.toArray().filter((i) => !pathPoses.has(i)));
    return inside.size;
  };

  // Test both handednesses
  return findSolution(-1) || findSolution(1);
};

export const correctOutput = 527;
