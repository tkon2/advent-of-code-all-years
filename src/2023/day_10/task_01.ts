import { Solution } from '../../types/solution';
import { Position } from '../../lib/position';

export const solve: Solution = async (rows, { log, logging }) => {
  /** Row */
  const startingPosR = rows.findIndex((l) => l.includes('S'));
  log('Starting Row', startingPosR);
  /** Column */
  const startingPosC = rows[startingPosR].indexOf('S');
  log('Starting Col', startingPosC);

  const findNext = (prev: Position, pos: Position): Position | null => {
    if (pos[0] < 0 || pos[0] >= rows.length) return null;
    if (pos[1] < 0 || pos[1] >= rows[0].length) return null;
    const c = rows[pos[0]][pos[1]];
    log('Prev', prev, 'Current:', pos, 'Current Char:', c);
    // Up
    if (prev[0] === pos[0] + 1 && c === 'F') return [pos[0], pos[1] + 1];
    if (prev[0] === pos[0] + 1 && c === '|') return [pos[0] - 1, pos[1]];
    if (prev[0] === pos[0] + 1 && c === '7') return [pos[0], pos[1] - 1];

    // Down
    if (prev[0] === pos[0] - 1 && c === 'L') return [pos[0], pos[1] + 1];
    if (prev[0] === pos[0] - 1 && c === '|') return [pos[0] + 1, pos[1]];
    if (prev[0] === pos[0] - 1 && c === 'J') return [pos[0], pos[1] - 1];

    // Right
    if (prev[1] === pos[1] - 1 && c === 'J') return [pos[0] - 1, pos[1]];
    if (prev[1] === pos[1] - 1 && c === '-') return [pos[0], pos[1] + 1];
    if (prev[1] === pos[1] - 1 && c === '7') return [pos[0] + 1, pos[1]];

    // Left
    if (prev[1] === pos[1] + 1 && c === 'F') return [pos[0] + 1, pos[1]];
    if (prev[1] === pos[1] + 1 && c === '-') return [pos[0], pos[1] - 1];
    if (prev[1] === pos[1] + 1 && c === 'L') return [pos[0] - 1, pos[1]];

    return null;
  };

  // Need to keep track of up to four paths
  let currents = [
    findNext([startingPosR, startingPosC], [startingPosR + 1, startingPosC]),
    findNext([startingPosR, startingPosC], [startingPosR - 1, startingPosC]),
    findNext([startingPosR, startingPosC], [startingPosR, startingPosC + 1]),
    findNext([startingPosR, startingPosC], [startingPosR, startingPosC - 1]),
  ] as (Position | null)[];
  let prevs: (Position | null)[] = [
    [startingPosR + 1, startingPosC],
    [startingPosR - 1, startingPosC],
    [startingPosR, startingPosC + 1],
    [startingPosR, startingPosC - 1],
  ];

  let correct: number | null = null;

  while (!correct) {
    currents.forEach((current, i) => {
      if (current === null) return;
      if (rows[current[0]][current[1]] === 'S') {
        correct = i;
      }
      const prev = prevs[i];
      if (prev === null) return;
      const next = findNext(prev, current);
      currents[i] = next;
      prevs[i] = current;
    });

    if (currents.every((c) => c === null)) break;
  }
  if (!correct) throw new Error('Could not find path');

  let current = [
    findNext([startingPosR, startingPosC], [startingPosR + 1, startingPosC]),
    findNext([startingPosR, startingPosC], [startingPosR - 1, startingPosC]),
    findNext([startingPosR, startingPosC], [startingPosR, startingPosC + 1]),
    findNext([startingPosR, startingPosC], [startingPosR, startingPosC - 1]),
  ][correct];
  // let previous = [startingPosR, startingPosC] as Position;

  let previous: Position | null = [
    [startingPosR + 1, startingPosC] as Position,
    [startingPosR - 1, startingPosC] as Position,
    [startingPosR, startingPosC + 1] as Position,
    [startingPosR, startingPosC - 1] as Position,
  ][correct];

  const dist = (p: Position) => {
    return Math.abs(startingPosR - p[0]) + Math.abs(startingPosC - p[1]);
  };

  let steps = 1;
  let poses: Position[] = [];
  do {
    log('Doing step');
    if (logging) poses.push(current as Position);
    const next = findNext(previous, current as Position);
    previous = current as Position;
    current = next;
    steps += 1;
  } while (current !== null);

  for (let r = 0; r < rows.length; r++) {
    let line = '';
    for (let c = 0; c < rows[r].length; c++) {
      if (poses.some((p) => p[0] === r && p[1] === c)) line += dist([r, c]);
      else line += ' ';
    }
    log(line);
  }

  return steps / 2;
};

export const correctOutput = 6812;
