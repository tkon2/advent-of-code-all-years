import { Queue } from 'data-structure-typed';
import { Solution } from '../../types/solution';

export const solve: Solution = async (rows, { log }) => {
  let total = 0;

  let stepGoal = 64;

  const map = rows.map((r) => r.split(''));

  const startingPosC = map.findIndex((m) => m.includes('S'));
  const startingPosR = map[startingPosC].indexOf('S');

  const pool = new Queue<[number, number, number]>([
    [startingPosC, startingPosR, 0],
  ]);
  const visited = new Set<string>();
  while (!pool.isEmpty()) {
    const [r, c, steps] = pool.shift()!;
    if (map[r]?.[c] !== '.' && map[r]?.[c] !== 'S') continue;
    if (steps > stepGoal) continue;
    const key = [r, c, steps].join(',');
    if (visited.has(key)) continue;
    visited.add(key);
    log(r, c, steps, map[r]?.[c]);
    if (steps === stepGoal) total++;
    pool.push([r + 1, c, steps + 1]);
    pool.push([r - 1, c, steps + 1]);
    pool.push([r + 0, c + 1, steps + 1]);
    pool.push([r + 0, c - 1, steps + 1]);
  }

  for (let i = 0; i < rows.length; i++) {
    let line = '';
    for (let j = 0; j < rows[i].length; j++) {
      if (visited.has([i, j].join(','))) line += 'O';
      else line += rows[i][j];
    }
    log(line);
  }

  log('Not 46');
  return total;
};

export const correctOutput = 3682;
