import { Queue } from 'data-structure-typed';
import { Solution } from '../../types/solution';

const totalSteps = 26501365;

export const solve: Solution = async (rows, { log }) => {
  const H = rows.length;
  const W = rows[0].length;
  const map = rows.map((r) => r.split(''));
  const startingPosC = map.findIndex((m) => m.includes('S'));
  const startingPosR = map[startingPosC].indexOf('S');
  map[startingPosC][startingPosR] = '.';

  const getValueAt = (r: number, c: number) => {
    let mr = r % rows.length;
    let mc = c % rows[0].length;
    while (mr < 0) mr += rows.length;
    while (mc < 0) mc += rows[0].length;
    return map[mr][mc];
  };

  ////////////////////////////

  const solveTo = (stepGoal: number) => {
    const pool = new Queue<[number, number, number]>([
      [startingPosC, startingPosR, 0],
    ]);
    const visited = new Set<string>();
    const counts = new Set<string>();
    let count = 0;
    while (!pool.isEmpty()) {
      const [r, c, steps] = pool.shift()!;
      if (steps > stepGoal) continue;
      const key = [r, c, steps].join(',');
      if (visited.has(key)) continue;
      visited.add(key);
      // log(r, c, steps, mapValue);
      if (steps === stepGoal) {
        count++;
        // counts.add([r, c].join(','));
      }
      for (let d of [
        [1, 0],
        [-1, 0],
        [0, 1],
        [0, -1],
      ]) {
        let nr = r + d[0];
        let nc = c + d[1];
        if (getValueAt(nr, nc) !== '.') continue;
        // if (nr < 0) nr = rows.length - 1;
        // if (nc < 0) nc = rows[0].length - 1;
        pool.push([nr, nc, steps + 1]);
      }
      // pool.push([(r + 1) % rows.length, c, steps + 1]);
      // pool.push([(r + rows.length - 1) % rows.length, c + 0, steps + 1]);
      // pool.push([r + 0, (c + 1) % rows[0].length, steps + 1]);
      // pool.push([r + 0, (c + rows[0].length - 1) % rows[0].length, steps + 1]);
    }
    return count;
  };

  const x1 = 0;
  const y1 = solveTo((totalSteps % W) + W * 0);
  const x2 = 1;
  const y2 = solveTo((totalSteps % W) + W * 1);
  const x3 = 2;
  const y3 = solveTo((totalSteps % W) + W * 2);

  const solveCubic = () => {
    const a =
      (x1 * (y3 - y2) + x2 * (y1 - y3) + x3 * (y2 - y1)) /
      ((x1 - x2) * (x1 - x3) * (x2 - x3));
    const b = (y2 - y1) / (x2 - x1) - a * (x1 + x2);
    const c = y1 - a * x1 ** 2 - b * x1;
    const x = Math.floor(totalSteps / W);
    const ans = a * x ** 2 + b * x + c;
    return ans;
  };

  // const knownGridVals = new Map<string, bigint>();
  // const getGridValue = (r: number, c: number, remaining: number) => {
  //   const sg = Math.min(rows.length * 2, remaining);
  //   const key = [r, c, sg].join(',');
  //   const known = knownGridVals.get(key);
  //   if (known) return known;
  //   let count = 0n;
  //   const pool = new Queue<[number, number, number]>([[r, c, 0]])
  //   const visited = new Set<string>();
  //   while (!pool.isEmpty()) {
  //     const [r, c, steps] = pool.shift()!;
  //     if (map[r]?.[c] !== '.' && map[r]?.[c] !== 'S') continue;
  //     if (steps > sg) continue;
  //     const key = [r, c, steps].join(',');
  //     if (visited.has(key)) continue;
  //     visited.add(key);
  //     // log(r, c, steps, map[r]?.[c]);
  //     if (steps === sg) count++;
  //     pool.push([r + 1, c, steps + 1]);
  //     pool.push([r - 1, c, steps + 1]);
  //     pool.push([r + 0, c + 1, steps + 1]);
  //     pool.push([r + 0, c - 1, steps + 1]);
  //   }
  //   knownGridVals.set(key, count);
  //   return count;
  // }

  // let total = 0n;

  // const pool = new Queue([{ r: startingPosR, c: startingPosC, gx: 0, gy: 0, remaining: 26501365 }]);
  // const visited = new Set<string>();
  // let loopCount = 0;
  // while (!pool.isEmpty()) {
  //   const { r, c, gx, gy, remaining } = pool.shift()!;
  //   if (Math.abs(gx) > 202300 || Math.abs(gy) > 202300) {
  //     log({ r, c, gx, gy, remaining });
  //     throw new Error("OOB");
  //   }
  //   if (loopCount < 10 || loopCount % 10000000 === 0) log({ r, c, gx, gy, remaining, loopCount });
  //   loopCount++;
  //   if (remaining <= 0) continue;
  //   const key = [gx, gy].join(',');
  //   if (visited.has(key)) continue;
  //   visited.add(key);
  //   total += getGridValue(r, c, remaining);

  //   const goTo = (nx: number, ny: number) => {
  //     const key = [nx, ny].join(',');
  //     if (visited.has(key)) return;
  //     let nr = r;
  //     let nc = c;
  //     let nRemaining = remaining;

  //     if (nx < gx) {
  //       if (nc === W - 1) nRemaining -= W;
  //       nc = W - 1;
  //     }
  //     if (nx > gx) {
  //       if (nc === 0) nRemaining -= W;
  //       nc = 0;
  //     }

  //     if (ny < gy) {
  //       if (nr === H - 1) nRemaining -= H;
  //       nr = H - 1;
  //     }
  //     if (ny > gy) {
  //       if (nr === 0) nRemaining -= H;
  //       nr = 0;
  //     }

  //     nRemaining -= Math.abs(nc - c);
  //     nRemaining -= Math.abs(nr - r);

  //     pool.push({
  //       r: nr,
  //       c: nc,
  //       gx: nx,
  //       gy: ny,
  //       remaining: nRemaining,
  //     })
  //   }

  //   // When going left, the start pos is moved to the right etc
  //   goTo(gx + 1, gy + 0);
  //   goTo(gx - 1, gy + 0);
  //   goTo(gx + 0, gy + 1);
  //   goTo(gx + 0, gy - 1);
  // }

  // const startingPosC = map.findIndex(m => m.includes('S'));
  // const startingPosR = map[startingPosC].indexOf('S');
  // map[startingPosC][startingPosR] = '.'

  // const pool = new Queue<[number, number, number]>([[startingPosC, startingPosR, 0]])
  // const visited = new Set<string>();
  // const counts = new Set<string>();
  // while (!pool.isEmpty()) {
  //   const [r, c, steps] = pool.shift()!;
  //   if (steps > stepGoal) continue;
  //   const key = [r, c, steps].join(',');
  //   if (visited.has(key)) continue;
  //   visited.add(key);
  //   // log(r, c, steps, mapValue);
  //   if (steps === stepGoal) {
  //     total++;
  //     // counts.add([r, c].join(','));
  //   }
  //   for (let d of [[1, 0], [-1, 0], [0, 1], [0, -1]]) {
  //     let nr = (r + d[0]);
  //     let nc = (c + d[1]);
  //     let mr = nr % rows.length;
  //     let mc = nc % rows[0].length;
  //     while (mr < 0) mr += rows.length;
  //     while (mc < 0) mc += rows[0].length;
  //     if (map[mr][mc] !== '.') continue;
  //     // if (nr < 0) nr = rows.length - 1;
  //     // if (nc < 0) nc = rows[0].length - 1;
  //     pool.push([nr, nc, steps + 1]);

  //   }
  //   // pool.push([(r + 1) % rows.length, c, steps + 1]);
  //   // pool.push([(r + rows.length - 1) % rows.length, c + 0, steps + 1]);
  //   // pool.push([r + 0, (c + 1) % rows[0].length, steps + 1]);
  //   // pool.push([r + 0, (c + rows[0].length - 1) % rows[0].length, steps + 1]);
  // }

  // for (let i = 0; i < rows.length; i++) {
  //   let line = '';
  //   for (let j = 0; j < rows[i].length; j++) {
  //     if (counts.has([i, j].join(','))) line += 'O';
  //     else line += rows[i][j];
  //   }
  //   // log(line);
  // }

  // log("Too high", 1906051824540742);
  //                  609012263058042
  log(609012263058042);
  return solveCubic();
};

export const correctOutput = 609012263058042;
