# Advent of Code - all years

Baseline typescript project with useful utils specifically crafted for advent
of code.

This project requires bun to run. Bun is easy to [install](https://bun.sh/).

Run `bun install` to install dependencies, then `bun solve` to solve all years.

You can run `bun solve 2015` to only solve 2015,
or `bun solve 2015 2` to only solve day 2 in 2015.
You can even run `bun solve 2015 2 1` to solve part one of day 2 in 2015.

Add the `-c` flag anywhere to copy the data from the clipboard. Useful while
working with example data.
